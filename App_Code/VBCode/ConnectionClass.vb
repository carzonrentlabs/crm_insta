Imports Microsoft.VisualBasic

Public Class ConnectionClass

    Public Function GetConfigConnection(ByVal KeyName As String) As String
        Dim Strconn As String = ConfigurationManager.ConnectionStrings("EasyCabsConnString").ToString()
        'Con = new SqlConnection(Strconn); 
        Dim conn As String() = Strconn.Split(";"c)
        Dim KeyName1 As String = KeyName
        If conn.Length <> 0 Then
            Dim Databasename As String = conn(1).ToString()
            Dim arrDatabasename As String() = Databasename.Split("="c)
            Databasename = arrDatabasename(1).ToString()

            Dim UserID As String = conn(2).ToString()
            Dim arrUserID As String() = UserID.Split("="c)
            UserID = arrUserID(1).ToString()

            Dim Password As String = conn(3).ToString()
            Dim arrPassword As String() = Password.Split("="c)
            Password = arrPassword(1).ToString()

            Dim ServerName As String = conn(0).ToString()
            Dim arrServerName As String() = ServerName.Split("="c)
            ServerName = arrServerName(1).ToString()

            If KeyName1 = "d" Then
                Return Databasename

            ElseIf KeyName1 = "p" Then
                Return Password

            ElseIf KeyName1 = "u" Then
                Return UserID

            ElseIf KeyName1 = "s" Then
                Return ServerName
            Else
                Return 0
            End If
        Else
            Return 0
            End If

    End Function
End Class
