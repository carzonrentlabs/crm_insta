<%@ Page Language="C#" MasterPageFile="~/CRDMasterpage.master" AutoEventWireup="true" CodeFile="CRMReport.aspx.cs" Inherits="CRM_CRMReport" Title="Insta : CRMReport" Theme="TMSTheme" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Page" Runat="Server">
    <table width="100%">
        <tr>
            <td align="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="CRM Report" SkinID="HeadingTop"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <strong>Select Month : </strong>
                <asp:DropDownList ID="ddlMName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlMName_SelectedIndexChanged">
                    <asp:ListItem Value="1">Jan</asp:ListItem>
                    <asp:ListItem Value="2">Feb</asp:ListItem>
                    <asp:ListItem Value="3">Mar</asp:ListItem>
                    <asp:ListItem Value="4">Apr</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">Jun</asp:ListItem>
                    <asp:ListItem Value="7">Jul</asp:ListItem>
                    <asp:ListItem Value="8">Aug</asp:ListItem>
                    <asp:ListItem Value="9">Sept</asp:ListItem>
                    <asp:ListItem Value="10">Oct</asp:ListItem>
                    <asp:ListItem Value="11">Nov</asp:ListItem>
                    <asp:ListItem Value="12">Dec</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlMName"
                    ErrorMessage="Please Select Month" InitialValue="0">*</asp:RequiredFieldValidator>
                &nbsp;&nbsp; <strong>Select Year :</strong>
                <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" DataSourceID="sdcsYear" DataTextField="Year" DataValueField="Fyear">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                </asp:DropDownList>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                    runat="server" ControlToValidate="ddlYear" ErrorMessage="Please Select Year"
                    InitialValue="0">*</asp:RequiredFieldValidator>
                <asp:Button ID="btnexport" runat="server" OnClick="btnexport_Click"
                    Text="Export to Excel" /></td>
        </tr>
        <tr>
            <td>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
            </td>
        </tr>
        <tr>
            <td >
                <asp:GridView ID="GvcrmDetails" SkinID="SandAndSkywithoutwidth" runat="server" AutoGenerateColumns="False" Caption="<B>CRM Details</b>" DataSourceID="odsCRM" Width="70%">
                <Columns>
                <asp:TemplateField HeaderText="Branch Name">
                <ItemTemplate>
                    <asp:Label ID="lblbranch" runat="server" Text='<%#Bind("BranchName")%>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="left" />
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="Total Complaints Registered">
                <ItemTemplate>
                    <asp:Label ID="lblttl" runat="server" Text='<%#Bind("totalinmonth")%>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="Closed Complaints within 48 hrs">
                <ItemTemplate>
                    <asp:Label ID="lblcom" runat="server" Text='<%#Bind("totalcompleted")%>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="Achieved %Age">
                <ItemTemplate>
                    <asp:Label ID="lblachieved" runat="server" Text='<%#Bind("achieved")%>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderText=" Average TAT">
                <ItemTemplate>
                    <asp:Label ID="lblTAT" runat="server" Text='<%#Bind("TAT")%>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="width: 100px; height: 21px;">
                <asp:ObjectDataSource ID="odsCRM" runat="server" SelectMethod="GetCRMData" TypeName="insta.CRM_Insta">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlMName" Name="Month" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="ddlYear" Name="Year" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:SqlDataSource ID="sdcsYear" runat="server" ConnectionString="<%$ ConnectionStrings:EasyCabsConnString %>"
                    ProviderName="<%$ ConnectionStrings:EasyCabsConnString.ProviderName %>" SelectCommand="BL_SP_GetYear"
                    SelectCommandType="StoredProcedure"></asp:SqlDataSource>
            </td>
        </tr>
       
     
    </table>
</asp:Content>

