﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;
using System;

/// <summary>
/// Summary description for CorDrive
/// </summary>
namespace Insta
{
    public class CorDrive
    {
        public CorDrive()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public DataSet GetLocations_UserAccessWise_Unit(int UserID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = UserID;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise_Unit", ObjParam);
        }

        public DataSet GetActiveCarModel()
        {
            SqlParameter[] ObjParam = new SqlParameter[0];

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetActiveCarModel", ObjParam);
        }
        public DataSet GetCarCategory()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCarCategory");
        }
        public int SubmitSalesPlan(int CusId, string MeetingDesc, string ActionItem, int User
           , string MeetingPersonName, string MeetingPersonMobile, string ClientCoName, string CustomerType
           , string ModeofConnect, int ProspectID, int Month, int Year, string MeetingSchedule)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[13];

            ObjparamUser[0] = new SqlParameter("@CusId", CusId);
            ObjparamUser[1] = new SqlParameter("@MeetingDesc", MeetingDesc);
            ObjparamUser[2] = new SqlParameter("@ActionItem", ActionItem);
            ObjparamUser[3] = new SqlParameter("@User", User);
            ObjparamUser[4] = new SqlParameter("@MeetingPersonName", MeetingPersonName);
            ObjparamUser[5] = new SqlParameter("@MeetingPersonMobile", MeetingPersonMobile);
            ObjparamUser[6] = new SqlParameter("@ClientCoName", ClientCoName);
            ObjparamUser[7] = new SqlParameter("@CustomerType", CustomerType);
            ObjparamUser[8] = new SqlParameter("@ModeOfConnect", ModeofConnect);
            ObjparamUser[9] = new SqlParameter("@ProspectID", ProspectID);
            ObjparamUser[10] = new SqlParameter("@Month", Month);
            ObjparamUser[11] = new SqlParameter("@Year", Year);
            ObjparamUser[12] = new SqlParameter("@MeetingSchedule", MeetingSchedule);
            int status = 0;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Sp_SavePlan", ObjparamUser);
            if (ds.Tables[0].Rows.Count > 0)
            {
                status = Convert.ToInt32(ds.Tables[0].Rows[0]["ReturnID"]);
            }
            else
            {
                status = -1;
            }

            return status;
        }
        public DataSet GetSalesPlanReport(DateTime FromDate, DateTime ToDate, int UserID
           , string CustomerType, string ModeOfConnect, int ClientCoID, int CityID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[7];
            ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
            ObjparamUser[2] = new SqlParameter("@UserID", UserID);
            ObjparamUser[3] = new SqlParameter("@CustomerType", CustomerType);
            ObjparamUser[4] = new SqlParameter("@ModeOfConnect", ModeOfConnect);
            ObjparamUser[5] = new SqlParameter("@ClientCoId", ClientCoID);
            ObjparamUser[6] = new SqlParameter("@CityID", CityID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_SalesPlan", ObjparamUser);
        }

        public DataSet GetLoginHoursDetail(DateTime _FromDate, DateTime _ToDate, int _CityId, int LoginStatus)
        {
            SqlParameter[] ObjParam = new SqlParameter[4];
            ObjParam[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = _FromDate;

            ObjParam[1] = new SqlParameter("@Todate", SqlDbType.DateTime);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = _ToDate;

            ObjParam[2] = new SqlParameter("@CityId", SqlDbType.Int);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = _CityId;

            ObjParam[3] = new SqlParameter("@logStatus", SqlDbType.Int);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = LoginStatus;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_ChauffuerLoginDetail_NewReport", ObjParam);
        }

        public DataSet GetVenderUsgaeReport(int _BranchId, DateTime _FromDate, DateTime _ToDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[3];

            ObjParam[0] = new SqlParameter("@BranchId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = _BranchId;

            ObjParam[1] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = _FromDate;

            ObjParam[2] = new SqlParameter("@Todate", SqlDbType.DateTime);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = _ToDate;


            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_OnCallVendorUsageReport", ObjParam);
        }

        public DataSet GetPKPMData(int _CityId, DateTime _FromDate, DateTime _ToDate, int _VenDorId, int _ModelId, int VdpYN)
        {
            SqlParameter[] ObjParam = new SqlParameter[6];

            ObjParam[0] = new SqlParameter("@CityId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = _CityId;

            ObjParam[1] = new SqlParameter("@PickupDateFrom", SqlDbType.DateTime);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = _FromDate;

            ObjParam[2] = new SqlParameter("@PickupDateTo", SqlDbType.DateTime);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = _ToDate;

            ObjParam[3] = new SqlParameter("@ModelId", SqlDbType.Int);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = _ModelId;

            ObjParam[4] = new SqlParameter("@VendorID", SqlDbType.Int);
            ObjParam[4].Direction = ParameterDirection.Input;
            ObjParam[4].Value = _VenDorId;

            ObjParam[5] = new SqlParameter("@VDPYN", SqlDbType.Int);
            ObjParam[5].Direction = ParameterDirection.Input;
            ObjParam[5].Value = VdpYN;
            //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_PKPMReport_New", ObjParam);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_PKPMReport_New_13_May", ObjParam);
        }

        public DataSet BrachChauffuerDetails(int cityId, int status, int modelId, int vDPYN, int chauffuerSource, int carVendorId)
        {
            SqlParameter[] ObjParam = new SqlParameter[6];

            ObjParam[0] = new SqlParameter("@CityId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = cityId;

            ObjParam[1] = new SqlParameter("@Status", SqlDbType.Int);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = status;

            ObjParam[2] = new SqlParameter("@ModelId", SqlDbType.Int);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = modelId;

            ObjParam[3] = new SqlParameter("@VDPYN", SqlDbType.Int);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = vDPYN;

            ObjParam[4] = new SqlParameter("@ChauffuerSource", SqlDbType.Int);
            ObjParam[4].Direction = ParameterDirection.Input;
            ObjParam[4].Value = chauffuerSource;

            ObjParam[5] = new SqlParameter("@CarVendorId", SqlDbType.Int);
            ObjParam[5].Direction = ParameterDirection.Input;
            ObjParam[5].Value = carVendorId;


            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_BranchChauffuerList", ObjParam);
        }

        public DataSet GetCorDriveTrackingReport(int _BranchId, DateTime _FromDate, DateTime _ToDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[3];

            ObjParam[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = _BranchId;

            ObjParam[1] = new SqlParameter("@Pickupdate", SqlDbType.DateTime);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = _FromDate;

            ObjParam[2] = new SqlParameter("@DropoffDate", SqlDbType.DateTime);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = _ToDate;


            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CORDriveTracking", ObjParam);
        }

        public DataSet GetNextDutySlip(int _BookingId, int _CarId, DateTime _FromDate, DateTime _ToDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[4];

            ObjParam[0] = new SqlParameter("@CurrentBookingId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = _BookingId;

            ObjParam[1] = new SqlParameter("@CarId", SqlDbType.Int);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = _CarId;

            ObjParam[2] = new SqlParameter("@PickupFromdate", SqlDbType.DateTime);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = _FromDate;

            ObjParam[3] = new SqlParameter("@PickupTodate", SqlDbType.DateTime);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = _ToDate;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetNextDuty", ObjParam);
        }
        public DataSet GetCarInInventryList(int CityId, int Status, int ModelId, int VDPYN, int CarAge)
        {
            SqlParameter[] ObjParam = new SqlParameter[5];

            ObjParam[0] = new SqlParameter("@cityID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = CityId;

            ObjParam[1] = new SqlParameter("@Status", SqlDbType.Int);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = Status;

            ObjParam[2] = new SqlParameter("@ModelId", SqlDbType.Int);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = ModelId;

            ObjParam[3] = new SqlParameter("@VDPYN", SqlDbType.Int);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = VDPYN;

            ObjParam[4] = new SqlParameter("@AgeOfCar", SqlDbType.Int);
            ObjParam[4].Direction = ParameterDirection.Input;
            ObjParam[4].Value = CarAge;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CarsinInventoryList", ObjParam);
        }

        public DataSet GetCorDrivInvoiceReport(int CityId, DateTime PickupDate, DateTime DropOffDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[3];

            ObjParam[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = CityId;

            ObjParam[1] = new SqlParameter("@Pickupdate", SqlDbType.DateTime);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = PickupDate;

            ObjParam[2] = new SqlParameter("@DropoffDate", SqlDbType.DateTime);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = DropOffDate;


            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CORDriveInvoiceReport", ObjParam);
        }
        public DataSet DriverEndedReport(int CityId, DateTime PickupDate, DateTime DropOffDate, string Status)
        {
            SqlParameter[] ObjParam = new SqlParameter[4];

            ObjParam[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = CityId;

            ObjParam[1] = new SqlParameter("@Pickupdate", SqlDbType.DateTime);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = PickupDate;

            ObjParam[2] = new SqlParameter("@DropoffDate", SqlDbType.DateTime);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = DropOffDate;

            ObjParam[3] = new SqlParameter("@Status", SqlDbType.VarChar);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = Status;


            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_DriverEndedReport", ObjParam);
        }
        public DataSet GetCorDriveBranchInventryReport(int CityId, int CarCatID, int AttachmentPackge, int CarID, DateTime FromDate, DateTime ToDate, int Status)
        {
            SqlParameter[] ObjParam = new SqlParameter[7];

            ObjParam[0] = new SqlParameter("@CityId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = CityId;

            ObjParam[1] = new SqlParameter("@CarCarId", SqlDbType.Int);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = CarCatID;

            ObjParam[2] = new SqlParameter("@AttachmentPackage", SqlDbType.Int);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = AttachmentPackge;

            ObjParam[3] = new SqlParameter("@CarSource", SqlDbType.Int);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = CarID;

            ObjParam[4] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            ObjParam[4].Direction = ParameterDirection.Input;
            ObjParam[4].Value = FromDate;

            ObjParam[5] = new SqlParameter("@Todate", SqlDbType.DateTime);
            ObjParam[5].Direction = ParameterDirection.Input;
            ObjParam[5].Value = ToDate;

            ObjParam[6] = new SqlParameter("@Status", SqlDbType.Int);
            ObjParam[6].Direction = ParameterDirection.Input;
            ObjParam[6].Value = Status;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_BranchInventory", ObjParam);
        }
        public DataSet GetOwnedCarReportData(int UnitId, DateTime FromDate, DateTime ToDate, int ProductId)
        {
            SqlParameter[] ObjParam = new SqlParameter[4];

            ObjParam[0] = new SqlParameter("@BranchId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = UnitId;

            ObjParam[1] = new SqlParameter("@PickupDateFrom", SqlDbType.DateTime);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = FromDate;

            ObjParam[2] = new SqlParameter("@PickupDateTo", SqlDbType.DateTime);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = ToDate;

            ObjParam[3] = new SqlParameter("@ProductId", SqlDbType.Int);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = ProductId;


            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_Prc_OwnedCarReport_08_May_2015", ObjParam);
        }
        public DataSet GetLoginHoursSummary(int cityId, DateTime fromDate, DateTime toDate, int modelId, int vdpYN)
        {
            SqlParameter[] ObjParam = new SqlParameter[6];

            ObjParam[0] = new SqlParameter("@CityId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = cityId;

            ObjParam[1] = new SqlParameter("@Fromdate", SqlDbType.DateTime);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = fromDate;

            ObjParam[2] = new SqlParameter("@Todate", SqlDbType.DateTime);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = toDate;

            ObjParam[3] = new SqlParameter("@ModelId", SqlDbType.Int);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = modelId;

            ObjParam[4] = new SqlParameter("@VDPYN", SqlDbType.Int);
            ObjParam[4].Direction = ParameterDirection.Input;
            ObjParam[4].Value = vdpYN;

            ObjParam[5] = new SqlParameter("@VendorCarYN", SqlDbType.Int);
            ObjParam[5].Direction = ParameterDirection.Input;
            ObjParam[5].Value = 0;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CORDriveLoginHoursSummary", ObjParam);

        }

        public DataSet GetDayName(DateTime fromDate, DateTime todate)
        {

            SqlParameter[] ObjParam = new SqlParameter[2];

            ObjParam[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = fromDate;

            ObjParam[1] = new SqlParameter("@Todate", SqlDbType.DateTime);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = todate;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Fn_GetWEEKDAY", ObjParam);
        }
        public DataSet GetMixBooking(int branchId, DateTime fromDate, DateTime todate)
        {

            SqlParameter[] ObjParam = new SqlParameter[3];

            ObjParam[0] = new SqlParameter("@UnitId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = branchId;

            ObjParam[1] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = fromDate;

            ObjParam[2] = new SqlParameter("@Todate", SqlDbType.DateTime);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = todate;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_BookingMix", ObjParam);

        }

        public DataSet GetCategoryWiseRevenue(int branchId, DateTime fromDate, DateTime todate)
        {

            SqlParameter[] ObjParam = new SqlParameter[3];

            ObjParam[0] = new SqlParameter("@BrachId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = branchId;

            ObjParam[1] = new SqlParameter("@PickupdateFrom", SqlDbType.DateTime);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = fromDate;

            ObjParam[2] = new SqlParameter("@Pickupdateto", SqlDbType.DateTime);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = todate;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CategoryWiseRevenueReport", ObjParam);

        }

        public DataSet GetSoldOutReport(int branchId, DateTime fromDate, DateTime todate)
        {

            SqlParameter[] ObjParam = new SqlParameter[3];

            ObjParam[0] = new SqlParameter("@UnitId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = branchId;

            ObjParam[1] = new SqlParameter("@DateFrom", SqlDbType.DateTime);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = fromDate;

            ObjParam[2] = new SqlParameter("@DateTo", SqlDbType.DateTime);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = todate;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_SoldOutReport", ObjParam);

        }

        public DataSet GetCurrentDateLoginCar(int cityId)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@cityId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = cityId;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetAllCurrentdateLogin", ObjParam);

        }

        public DataSet GetAutAllocationReports(string fromdate, string todate)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = fromdate;

            ObjParam[1] = new SqlParameter("@Todate", SqlDbType.VarChar);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = todate;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prec_CorDriveBranchWiseAllocationDeallocationSummary", ObjParam);

        }
        public DataSet GetLoginSummaryReport(DateTime PickupDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@Pickupdate", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = PickupDate;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_Loginummary", ObjParam);
        }
        public DataSet GetAllocationReport(DateTime PickupDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@Pickupdate", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = PickupDate;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_AutoAllocationSummary", ObjParam);
        }

        public DataSet GetLoginSummaryReportCityWise(string CityName)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@CityName", SqlDbType.VarChar);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = CityName;
            // return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_LoginSummaryCityWiseNew", ObjParam);
            // return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_LoginSummaryCityWiseLatest", ObjParam); // commented by bk sharma on 27 nov 2015
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_LoginSummaryCityWiseLatest1", ObjParam);
        }
        public DataSet GetLoginSummaryExportToExcel(string CityName)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@CityName", SqlDbType.VarChar);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = CityName;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_LoginSummaryExportToExcel", ObjParam);
        }

        public DataSet GetAreaWiseHome(string Latitude, string Longitude, int Radius, int Catid)
        {
            SqlParameter[] ObjParam = new SqlParameter[4];
            ObjParam[0] = new SqlParameter("@Latitude", SqlDbType.Float);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = Latitude;

            ObjParam[1] = new SqlParameter("@Longitude", SqlDbType.Float);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = Longitude;

            ObjParam[2] = new SqlParameter("@Radius", SqlDbType.Int);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = Radius;

            ObjParam[3] = new SqlParameter("@Catid", SqlDbType.Int);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = Catid;
            //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_ReportAreaWiseHomeNew", ObjParam); //commented by bk sharma on 27 nov 2015
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_ReportAreaWiseHomeNew1", ObjParam);
        }
        public DataSet GetAreaWiseCurrent(string Latitude, string Longitude, int Radius, int Catid)
        {
            SqlParameter[] ObjParam = new SqlParameter[4];
            ObjParam[0] = new SqlParameter("@Latitude", SqlDbType.Float);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = Latitude;

            ObjParam[1] = new SqlParameter("@Longitude", SqlDbType.Float);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = Longitude;

            ObjParam[2] = new SqlParameter("@Radius", SqlDbType.Int);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = Radius;

            ObjParam[3] = new SqlParameter("@Catid", SqlDbType.Int);
            ObjParam[3].Direction = ParameterDirection.Input;
            ObjParam[3].Value = Catid;
            //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_ReportAreaWiseCurrentNew", ObjParam); //Commened By Bk on date 27 Nov 2015
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_ReportAreaWiseCurrentNew1", ObjParam);
        }

        public DataSet GetAreaVersion()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "sp_ReportAreaVersionWise");
        }
        public DataSet GetSummaryReportCityWise()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "sp_ReportSummaryCurrent");
        }
        public DataSet GetDispatchSummaryReport()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_RptDispatch");
        }
        public DataSet VendorCarActivityReport(int CityId)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@CityId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = CityId;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCarWorkingDetails_VDP_PKPM_Revenue_Report", ObjParam);
        }

        public DataSet GetLocations_UserAccessWise_SummaryCityWise(int UserID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = UserID;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise_SummaryCityWise", ObjParam);
        }
        public int SubmitCarRemarks(int CabId, string ReasonType, DateTime FromDate, DateTime ToDate, string Remarks)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];
            ObjparamUser[0] = new SqlParameter("@CabId", CabId);
            ObjparamUser[1] = new SqlParameter("@ReasonType", ReasonType);
            ObjparamUser[2] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[3] = new SqlParameter("@ToDate", ToDate);
            ObjparamUser[4] = new SqlParameter("@Remarks", Remarks);
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveNotAvailableReason", ObjparamUser);
        }
        public DataSet VendorCarActivityReportDateWise(int CityId, DateTime FromDate, DateTime ToDate)
        {

            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@CityId", CityId);
            ObjparamUser[1] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[2] = new SqlParameter("@ToDate", ToDate);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCarWorkingDetails_VDP_PKPM_RevenueNew", ObjparamUser);
        }
        public DataSet VendorCarActivityReportDateWise(int CityId, DateTime FromDate, DateTime ToDate, string Reason)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[4];
            ObjparamUser[0] = new SqlParameter("@CityId", CityId);
            ObjparamUser[1] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[2] = new SqlParameter("@ToDate", ToDate);
            ObjparamUser[3] = new SqlParameter("@Reason", Reason);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCarWorkingDetails_VDP_PKPM_RevenueReasonWise", ObjparamUser);
        }
        public DataSet SubmitPauseRideBookingid(int BookingId, int UserId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@BookingId", BookingId);
            ObjparamUser[1] = new SqlParameter("@UserId", UserId);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Sp_UpdatePauseRideBookingId", ObjparamUser);
        }

        public DataSet GetNotAvailableDetails(int CabId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@CabId", CabId);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_getNotAvailableDetails", ObjparamUser);
        }
        public int SubmitCarRemarks(int CabId, string ReasonType, DateTime FromDate, DateTime ToDate, string Remarks, int User)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];
            ObjparamUser[0] = new SqlParameter("@CabId", CabId);
            ObjparamUser[1] = new SqlParameter("@ReasonType", ReasonType);
            ObjparamUser[2] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[3] = new SqlParameter("@ToDate", ToDate);
            ObjparamUser[4] = new SqlParameter("@Remarks", Remarks);
            ObjparamUser[5] = new SqlParameter("@User", User);
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveNotAvailableReason", ObjparamUser);
        }
        public int UpdateCarRemarks(int CabId, string ReasonType, DateTime FromDate, DateTime ToDate, string Remarks, int User)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];
            ObjparamUser[0] = new SqlParameter("@CabId", CabId);
            ObjparamUser[1] = new SqlParameter("@ReasonType", ReasonType);
            ObjparamUser[2] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[3] = new SqlParameter("@ToDate", ToDate);
            ObjparamUser[4] = new SqlParameter("@Remarks", Remarks);
            ObjparamUser[5] = new SqlParameter("@User", User);
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_UpdateNotAvailableReason", ObjparamUser);
        }

        public DataSet GetRemarksLog(int CabId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@CabId", CabId);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetAllRemarksLog", ObjparamUser);
        }
        public bool IsUserAuthorized_VendorCarActivityRpt(int Userid)
        {
            bool answer = false;
            int[] UserList = { 1911, 2760, 1471, 444, 49, 920, 2419, 3016, 1139, 2902, 292, 2416, 2591, 164 };
            for (int i = 0; i < UserList.Length; i++)
            {
                if (UserList[i] == Userid)
                {
                    answer = true;
                    break;
                }
            }
            return answer;
        }

        public DataSet GetCustName()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_SalesFollowUpClient");
        }

        public DataSet GetProspectName()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_ProspectsList");
        }

        public DataSet GetCityName()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CityDetails");
        }

        public DataSet GetActionManagerName()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_SalesFollowUpUser");
        }
        public int SubmitSalesFollowUp(DateTime DateOfMeeting, int CusId, string MeetingDesc, string ActionItem
            , DateTime ActionByDate, int ActionMgrId, DateTime NextMeetingSchedule, int User
            , string MeetingPersonName, string MeetingPersonMobile, string ClientCoName, string CustomerType
            , string OutcomeofMetting, string ModeofConnect, int ProspectID, string ProspectMovement)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[16];
            ObjparamUser[0] = new SqlParameter("@DateOfMeeting", DateOfMeeting);
            ObjparamUser[1] = new SqlParameter("@CusId", CusId);
            ObjparamUser[2] = new SqlParameter("@MeetingDesc", MeetingDesc);
            ObjparamUser[3] = new SqlParameter("@ActionItem", ActionItem);
            ObjparamUser[4] = new SqlParameter("@ActionByDate", ActionByDate);
            ObjparamUser[5] = new SqlParameter("@ActionMgrId", ActionMgrId);
            ObjparamUser[6] = new SqlParameter("@NextMeetingSchedule", NextMeetingSchedule);
            ObjparamUser[7] = new SqlParameter("@User", User);
            ObjparamUser[8] = new SqlParameter("@MeetingPersonName", MeetingPersonName);
            ObjparamUser[9] = new SqlParameter("@MeetingPersonMobile", MeetingPersonMobile);
            ObjparamUser[10] = new SqlParameter("@ClientCoName", ClientCoName);
            ObjparamUser[11] = new SqlParameter("@CustomerType", CustomerType);
            ObjparamUser[12] = new SqlParameter("@OutcomeofMetting", OutcomeofMetting);
            ObjparamUser[13] = new SqlParameter("@ModeOfConnect", ModeofConnect);
            ObjparamUser[14] = new SqlParameter("@ProspectID", ProspectID);
            ObjparamUser[15] = new SqlParameter("@ProspectMovement", ProspectMovement);
            int status = 0;
            //return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveSalesFollowUp", ObjparamUser);
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Sp_SaveSalesFollowUp_New", ObjparamUser);
            if (ds.Tables[0].Rows.Count > 0)
            {
                status = Convert.ToInt32(ds.Tables[0].Rows[0]["ReturnID"]);
            }
            else
            {
                status = -1;
            }
            
            return status;
        }

        public int SubmitSalesTracker(int CusId, string MeetingDesc, string ActionItem, int User
            , string MeetingPersonName, string MeetingPersonMobile, string ClientCoName, string CustomerType
            , string ModeofConnect, int ProspectID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[10];
            
            ObjparamUser[0] = new SqlParameter("@CusId", CusId);
            ObjparamUser[1] = new SqlParameter("@MeetingDesc", MeetingDesc);
            ObjparamUser[2] = new SqlParameter("@ActionItem", ActionItem);
            ObjparamUser[3] = new SqlParameter("@User", User);
            ObjparamUser[4] = new SqlParameter("@MeetingPersonName", MeetingPersonName);
            ObjparamUser[5] = new SqlParameter("@MeetingPersonMobile", MeetingPersonMobile);
            ObjparamUser[6] = new SqlParameter("@ClientCoName", ClientCoName);
            ObjparamUser[7] = new SqlParameter("@CustomerType", CustomerType);
            ObjparamUser[8] = new SqlParameter("@ModeOfConnect", ModeofConnect);
            ObjparamUser[9] = new SqlParameter("@ProspectID", ProspectID);

            int status = 0;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Sp_SaveTracker", ObjparamUser);
            if (ds.Tables[0].Rows.Count > 0)
            {
                status = Convert.ToInt32(ds.Tables[0].Rows[0]["ReturnID"]);
            }
            else
            {
                status = -1;
            }

            return status;
        }

        public DataSet GetSalesFollowUpReport(DateTime FromDate, DateTime ToDate, int UserID
            , string CustomerType, string ModeOfConnect, int ClientCoID, int CityID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[7];
            ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
            ObjparamUser[2] = new SqlParameter("@UserID", UserID);
            ObjparamUser[3] = new SqlParameter("@CustomerType", CustomerType);
            ObjparamUser[4] = new SqlParameter("@ModeOfConnect", ModeOfConnect);
            ObjparamUser[5] = new SqlParameter("@ClientCoId", ClientCoID);
            ObjparamUser[6] = new SqlParameter("@CityID", CityID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_SalesFollowUp", ObjparamUser);
            //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_SalesFollowUp_New", ObjparamUser);
        }

        public DataSet GetSalesTrackerReport(DateTime FromDate, DateTime ToDate, int UserID
            , string CustomerType, string ModeOfConnect, int ClientCoID, int CityID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[7];
            ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
            ObjparamUser[2] = new SqlParameter("@UserID", UserID);
            ObjparamUser[3] = new SqlParameter("@CustomerType", CustomerType);
            ObjparamUser[4] = new SqlParameter("@ModeOfConnect", ModeOfConnect);
            ObjparamUser[5] = new SqlParameter("@ClientCoId", ClientCoID);
            ObjparamUser[6] = new SqlParameter("@CityID", CityID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_SalesTracker", ObjparamUser);
        }

        public int SaveProspect(string ClientCoName, int CityID, double PotentialAmout, string ContactPersonName, string EmailID, string Phone
            , int UserID, int UpdateYN, int ID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[10];
            ObjparamUser[0] = new SqlParameter("@ClientCoName", ClientCoName);
            ObjparamUser[1] = new SqlParameter("@CityID", CityID);
            ObjparamUser[2] = new SqlParameter("@PotentialAmout", PotentialAmout);
            ObjparamUser[3] = new SqlParameter("@ContactPersonName", ContactPersonName);
            ObjparamUser[4] = new SqlParameter("@EmailID", EmailID);
            ObjparamUser[5] = new SqlParameter("@Phone", Phone);
            ObjparamUser[6] = new SqlParameter("@CreatedBy", UserID);
            ObjparamUser[7] = new SqlParameter("@UpdateYN", UpdateYN);
            ObjparamUser[8] = new SqlParameter("@ID", ID);
            ObjparamUser[9] = new SqlParameter("@ReturnID", SqlDbType.Int);
            ObjparamUser[9].Direction = ParameterDirection.Output;

            int status = 0;
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveProspects", ObjparamUser);
            //return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveProspects_New", ObjparamUser);
            status = Convert.ToInt32(ObjparamUser[9].Value);
            return status;
        }

        public DataSet GetProspectsReport(DateTime FromDate, DateTime ToDate)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_Prospects", ObjparamUser);
        }

        public DataSet GetProspectsDataByID(int ID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@ID", ID);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_ProspectsByID", ObjparamUser);
        }

        public int DeactivateProspects(int ProspectID, int UserID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@ProspectID", ProspectID);
            ObjparamUser[1] = new SqlParameter("@UserID", UserID);
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_DeactivateProspects", ObjparamUser);
        }

        public DataSet GetUserName(int UserID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@UserID", UserID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetUserName", ObjparamUser);
        }

        public DataSet GetLocations_UserAccessWise_VendorActivity(int UserID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = UserID;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise_VendorActivity", ObjParam);
        }
        public DataSet CheckForcedoffRoad(int VendorCarId)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@VendorCarId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = VendorCarId;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CheckForceOffRoad", ObjParam);
        }
        public DataSet GetVendorPerformence(int CityId, DateTime FromDate, DateTime ToDate)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@CityId", CityId);
            ObjparamUser[1] = new SqlParameter("@DateFrom", FromDate);
            ObjparamUser[2] = new SqlParameter("@DateTo", ToDate);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "RelationShipManagerReprotDetails_BKSharma", ObjparamUser);
        }
        public DataSet GetDeallocationReportExportToExcel(DateTime FromDate, DateTime ToDate)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_DeallocationReport", ObjparamUser);
        }
        public DataSet GetBidSummaryReport(DateTime FromDate, DateTime ToDate)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_Call_AutoAllocationBidSummary", ObjparamUser);
        }
        public DataSet GetBidDetailsReport(string CityName, DateTime FromDate, DateTime ToDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[3];
            ObjParam[0] = new SqlParameter("@CityName", CityName);
            ObjParam[1] = new SqlParameter("@FromDate", FromDate);
            ObjParam[2] = new SqlParameter("@ToDate", ToDate);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_AutoAllocationCarBidDetails", ObjParam);
        }
        public DataSet GetCarWiseBidDetailsExportToExcel(DateTime FromDate, DateTime ToDate, int CarId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
            ObjparamUser[2] = new SqlParameter("@CarId", CarId);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_ExportBidDetailsCarWise", ObjparamUser);
        }
        public DataSet GetYatraMIS(DateTime FromDate, DateTime ToDate, string OriginCode)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
            ObjparamUser[2] = new SqlParameter("@OriginCode", OriginCode);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_YatraBookingMIS", ObjparamUser);
        }
        public DataSet GetBangaloreAirportVendorReport(int BranchId, DateTime FromDate, DateTime ToDate)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@BranchId", BranchId);
            ObjparamUser[1] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[2] = new SqlParameter("@ToDate", ToDate);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_AirportRecommendedVendorReport_ClosedDuties", ObjparamUser);
        }
    }
}