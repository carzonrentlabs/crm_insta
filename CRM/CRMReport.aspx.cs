using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ExcelUtil;

public partial class CRM_CRMReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ddlMName.SelectedValue = DateTime.Now.Month.ToString();
            ddlYear.SelectedValue = DateTime.Now.Year.ToString();
        }
    }
    protected void ddlMName_SelectedIndexChanged(object sender, EventArgs e)
    {
        GvcrmDetails.DataBind();
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        GvcrmDetails.DataBind();
    }
    protected void btnexport_Click(object sender, EventArgs e)
    {
       
        DataSet ds = new DataSet() ;
        ds = Insta.CRM_Insta.GetCRMData(Convert.ToInt32(ddlMName.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));        
        WorkbookEngine.ExportDataSetToExcel(ds, "CRMReport" + ".xls");
    }
}
