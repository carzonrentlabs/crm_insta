<%@ Page Language="C#" MasterPageFile="~/Salespage.master" AutoEventWireup="true"
    CodeFile="SalesPlan.aspx.cs" ValidateRequest="false" Inherits="SalesPlan"
    Title="Sales Plan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder_Page" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

            <script src="../JQuery/moment.js" type="text/javascript"></script>

            <script src="../JQuery/ui.core.js" type="text/javascript"></script>

            <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>

            <script src="../ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>

            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0" border="1" style="width: 75%">
                            <tr>
                                <td colspan="6" align="center" style="background-color: #ccffff; height: 30px;">
                                    <b>Sales Plan</b>&nbsp;
                                    <asp:Label ID="lblloginBy" Text="" runat="server"></asp:Label>&nbsp;
                                    Month:<asp:DropDownList ID="ddlMonth" runat="server"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rvmonth" InitialValue="0" SetFocusOnError="True" ValidationGroup="FS"
                                runat="server" ErrorMessage="Please Select Month" ControlToValidate="ddlMonth"
                                Display="None"></asp:RequiredFieldValidator>
                                    &nbsp;
                                    Year:<asp:DropDownList ID="ddlYear" runat="server"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rvYear" InitialValue="0" SetFocusOnError="True" ValidationGroup="FS"
                                runat="server" ErrorMessage="Please Select Year" ControlToValidate="ddlYear"
                                Display="None"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                            </tr>
                            <tr>
                                <td colspan="6" align="center">
                                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                                    <b>Client Type</b>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" colspan="1">
                                    <asp:DropDownList ID="ddlClientType" runat="server" ValidationGroup="FS" OnSelectedIndexChanged="ddlClientType_SelectedIndexChanged"
                                        AutoPostBack="true">
                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Prospect" Value="Prospect"></asp:ListItem>
                                        <asp:ListItem Text="Existing" Value="Existing"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="ddlclientvalidation" InitialValue="0" SetFocusOnError="True"
                                        ValidationGroup="FS" runat="server" ErrorMessage="Please Select Client Type"
                                        ControlToValidate="ddlClientType" Display="None"></asp:RequiredFieldValidator>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                                    <b>Customer Name</b>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" colspan="3">
                                    <asp:DropDownList ID="ddlCustName" runat="server" ValidationGroup="FS" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlCustName_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtcustname" runat="server" Visible="false" Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rvddlCustName" InitialValue="0" SetFocusOnError="True"
                                        ValidationGroup="FS" runat="server" ErrorMessage="Please Select Cust Name" ControlToValidate="ddlCustName"
                                        Display="None"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                                    <b>Agenda Meeting</b>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                                    <asp:DropDownList ID="txtMeetingDesc" ValidationGroup="FS" runat="server">
                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rvtxtMeetingDesc" InitialValue="0" SetFocusOnError="True"
                                        ValidationGroup="FS" runat="server" ErrorMessage="Please Enter Meeting Desc"
                                        ControlToValidate="txtMeetingDesc" Display="None"></asp:RequiredFieldValidator>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                                    <b>Metting Schedule</b>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                                    <asp:DropDownList ID="ddlmeetingschedule" ValidationGroup="FS" runat="server">
                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="First Week" Value="First Week"></asp:ListItem>
                                        <asp:ListItem Text="Second Week" Value="Second Week"></asp:ListItem>
                                        <asp:ListItem Text="Third Week" Value="Third Week"></asp:ListItem>
                                        <asp:ListItem Text="Fourth Week" Value="Fourth Week"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rvmeetingschedule" InitialValue="0" SetFocusOnError="True"
                                        ValidationGroup="FS" runat="server" ErrorMessage="Please Select Meeting Schedule"
                                        ControlToValidate="ddlmeetingschedule" Display="None"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                                    <b>Contact Person Name</b>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                                    <asp:TextBox ID="txtPersonName" runat="server" ValidationGroup="FS"></asp:TextBox>&nbsp;&nbsp;
                                    <asp:RequiredFieldValidator ID="rvtxtPersonName" SetFocusOnError="True" ValidationGroup="FS"
                                        runat="server" ErrorMessage="Please Enter Meeting Person Name" ControlToValidate="txtPersonName"
                                        Display="None"></asp:RequiredFieldValidator>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                                    <b>Mobile No</b>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" colspan="3">
                                    <asp:TextBox ID="txtMobileNo" MaxLength="10" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rvtxtMobileNo" SetFocusOnError="True" ValidationGroup="FS"
                                        runat="server" ErrorMessage="Please Enter Metting Person Mobile" ControlToValidate="txtMobileNo"
                                        Display="None"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                                    <b>Mode of Connect</b>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 2px 2px 2px 4px;">
                                    <asp:DropDownList ID="ddlmodeconnect" runat="server" ValidationGroup="FS">
                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Email" Value="Email"></asp:ListItem>
                                        <asp:ListItem Text="Phone" Value="Phone"></asp:ListItem>
                                        <asp:ListItem Text="Personal Meeting" Value="Personal Meeting"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="Requiredmodeconnect" InitialValue="0" SetFocusOnError="True"
                                        ValidationGroup="FS" runat="server" ErrorMessage="Please Select Mode of Connect"
                                        ControlToValidate="ddlmodeconnect" Display="None"></asp:RequiredFieldValidator>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                                    <b>Purpose of Meeting</b>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 2px 2px 2px 4px;" colspan="3">
                                    <asp:TextBox ID="txtActionItem" TextMode="MultiLine" Columns="25" Rows="5" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rvActionItem" SetFocusOnError="True" ValidationGroup="FS"
                                        runat="server" ErrorMessage="Please Enter Action Item" ControlToValidate="txtActionItem"
                                        Display="None"></asp:RequiredFieldValidator>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center" style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                                    <asp:ValidationSummary ID="vs" runat="server" ShowMessageBox="True" ShowSummary="False"
                                        ValidationGroup="FS" />
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                        ValidationGroup="FS" />&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" align="center" style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                                    <br />
                                    From Meeting Date &nbsp;&nbsp;<asp:TextBox ID="FromDate" runat="server"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="FromDate"
                                        Format="MMM/dd/yyyy" PopupPosition="TopLeft">
                                    </cc1:CalendarExtender>
                                    &nbsp;&nbsp;
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" SetFocusOnError="True" ValidationGroup="FSReport"
                                        runat="server" ErrorMessage="Please Select From Date" ControlToValidate="FromDate"
                                        Display="None"></asp:RequiredFieldValidator>
                                    To Meeting Date &nbsp;&nbsp;<asp:TextBox ID="ToDate" runat="server"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="ToDate"
                                        Format="MMM/dd/yyyy" PopupPosition="TopLeft">
                                    </cc1:CalendarExtender>
                                    &nbsp;&nbsp;
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" SetFocusOnError="True" ValidationGroup="FSReport"
                                        runat="server" ErrorMessage="Please Select To Meeting" ControlToValidate="ToDate"
                                        Display="None"></asp:RequiredFieldValidator>
                                    &nbsp;&nbsp; User Name&nbsp;&nbsp;
                                    <asp:DropDownList ID="ddlActionMgrName1" runat="server" ValidationGroup="FS">
                                    </asp:DropDownList>
                                    <br />
                                    <br />
                                    &nbsp;&nbsp; Client Type&nbsp;&nbsp;<asp:DropDownList ID="ddlClientType1" runat="server"
                                        ValidationGroup="FS" OnSelectedIndexChanged="ddlClientType1_SelectedIndexChanged"
                                        AutoPostBack="true">
                                        <asp:ListItem Text="All" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Prospect" Value="Prospect"></asp:ListItem>
                                        <asp:ListItem Text="Existing" Value="Existing"></asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;&nbsp; Mode Of Connect&nbsp;&nbsp;<asp:DropDownList ID="ddlmodeconnect1" runat="server"
                                        ValidationGroup="FS">
                                        <asp:ListItem Text="All" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Email" Value="Email"></asp:ListItem>
                                        <asp:ListItem Text="Phone" Value="Phone"></asp:ListItem>
                                        <asp:ListItem Text="Personal Meeting" Value="Personal Meeting"></asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;&nbsp; CustomerName&nbsp;&nbsp;<asp:DropDownList ID="ddlCustName1" runat="server"
                                        ValidationGroup="FS">
                                    </asp:DropDownList>
                                    &nbsp;&nbsp;City Name&nbsp;&nbsp;<asp:DropDownList ID="ddlcityName" runat="server">
                                    </asp:DropDownList>
                                    &nbsp;&nbsp;<asp:Button ID="btnGo" runat="server" Text="View" ValidationGroup="FSReport"
                                        OnClick="btnGo_Click" />
                                    &nbsp;&nbsp;<asp:Button ID="bntExprot" runat="server" Text="Export To Excel" OnClick="bntExprot_Click"
                                        ValidationGroup="FSReport" />
                                    <br />
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td colspan="6">
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" style="width: 90%">
                            <tr>
                                <td colspan="6" align="center">
                                    <asp:GridView runat="server" ID="gdsalesTracking" EmptyDataText="No Record Found"
                                        AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="gdsalesTracking_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Client Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblClientconame" Text='<% #Bind("Clientconame") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Meeting Desc">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMeetingDesc" Text='<% #Bind("MeetingDesc") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action Item">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblActionItem" Text='<% #Bind("ActionItem") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Meeting Person Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMeetingPersonName" Text='<% #Bind("MeetingPersonName") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Meeting Person Mobile">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMeetingPersonMobile" Text='<% #Bind("MeetingPersonMobile") %>'
                                                        runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Created By">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCreatedByName" Text='<% #Bind("CreatedByName") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Create Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCreateDate" Text='<% #Bind("CreateDate") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Customer Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCustomerType" Text='<% #Bind("CustomerType") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mode Of Connect">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblModeOfConnect" Text='<% #Bind("ModeOfConnect") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="City Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCityName" Text='<% #Bind("CityName") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Meeting Schedule">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCityName" Text='<% #Bind("meetingschedule") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
