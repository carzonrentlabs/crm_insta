﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for DRMBAL
/// </summary>
public class ImplantBAL
{
    public ImplantBAL()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public ArrayList LoadHour()
    {
        ArrayList AllHour = new ArrayList();
        int i = 0;
        for (i = 0; i <= 23; i++)
        {
            AllHour.Add(i);
        }
        return AllHour;
    }
    public ArrayList LoadMinute()
    {
        ArrayList AllMinute = new ArrayList();
        int i = 0;
        for (i = 0; i <= 59; i++)
        {
            AllMinute.Add(i);
        }
        return AllMinute;
    }
    public string CreateDateTime(string date, int Hr, int Min)
    {
        string newDateTime = "";
        if (date.Trim() == "")
        {
            date = System.DateTime.Now.ToString("MM-dd-yyyy");
        }
        newDateTime = date + " " + Hr + ":" + Min + ":00";
        return newDateTime;
    }
    public DataSet GetLocations(int UserID)
    {
        SqlParameter[] myParams = new SqlParameter[1];
        myParams[0] = new SqlParameter("@UserID", SqlDbType.Int);
        myParams[0].Value = UserID;
        //return ImplantDAL.GetDataSet("SP_Call_getLocations_UserAccessWise_Unit", myParams);
        return ImplantDAL.GetDataSet("getLocations_UserAccessWise_Unit", myParams);
                    
    }
    public DataSet GetClientImplantCompetitor()
    {
        return ImplantDAL.GetDataSet("SP_CallClientImplantCompetitor");
    }
    public DataSet GetBookingByClient(int ClientCoID,int CityId,DateTime PickUpDate,int ImplantId)
    {
        SqlParameter[] myParams = new SqlParameter[4];
        myParams[0] = new SqlParameter("@ClientCoID", SqlDbType.Int);
        myParams[0].Value = ClientCoID;
        myParams[1] = new SqlParameter("@CityId", SqlDbType.Int);
        myParams[1].Value = CityId;
        myParams[2] = new SqlParameter("@PickUpDate", SqlDbType.Date);
        myParams[2].Value = PickUpDate;
        myParams[3] = new SqlParameter("@ImplantId", SqlDbType.Int);
        myParams[3].Value = ImplantId;
        return ImplantDAL.GetDataSet("SP_GetBookingByClient", myParams);
       
    }
    public DataSet GetBookingByClient(int ClientCoID, int CityId, DateTime PickUpDate)
    {
        SqlParameter[] myParams = new SqlParameter[3];
        myParams[0] = new SqlParameter("@ClientCoID", SqlDbType.Int);
        myParams[0].Value = ClientCoID;
        myParams[1] = new SqlParameter("@CityId", SqlDbType.Int);
        myParams[1].Value = CityId;
        myParams[2] = new SqlParameter("@PickUpDate", SqlDbType.Date);
        myParams[2].Value = PickUpDate;
        return ImplantDAL.GetDataSet("SP_GetComplaintCountByClient", myParams);
    }
    public DataSet GetFeedBackCountByClient(int ClientCoID, int CityId, DateTime PickUpDate)
    {

        SqlParameter[] myParams = new SqlParameter[3];
        myParams[0] = new SqlParameter("@ClientCoID", SqlDbType.Int);
        myParams[0].Value = ClientCoID;
        myParams[1] = new SqlParameter("@CityId", SqlDbType.Int);
        myParams[1].Value = CityId;
        myParams[2] = new SqlParameter("@PickUpDate", SqlDbType.Date);
        myParams[2].Value = PickUpDate;
        return ImplantDAL.GetDataSet("SP_GetFeedBackCountByClient", myParams);
    }

    public DataSet GetFeedbackTypeByClient(int ClientCoID, int CityId, DateTime PickUpDate)
    {
        SqlParameter[] myParams = new SqlParameter[3];
        myParams[0] = new SqlParameter("@ClientCoID", SqlDbType.Int);
        myParams[0].Value = ClientCoID;
        myParams[1] = new SqlParameter("@CityId", SqlDbType.Int);
        myParams[1].Value = CityId;
        myParams[2] = new SqlParameter("@PickUpDate", SqlDbType.Date);
        myParams[2].Value = PickUpDate;
        return ImplantDAL.GetDataSet("SP_GetFeedTypeByClientNew", myParams);
    }
    public DataSet GetComplaintTypeByClient(int ClientCoID, int CityId, DateTime PickUpDate)
    {

        SqlParameter[] myParams = new SqlParameter[3];
        myParams[0] = new SqlParameter("@ClientCoID", SqlDbType.Int);
        myParams[0].Value = ClientCoID;
        myParams[1] = new SqlParameter("@CityId", SqlDbType.Int);
        myParams[1].Value = CityId;
        myParams[2] = new SqlParameter("@PickUpDate", SqlDbType.Date);
        myParams[2].Value = PickUpDate;
        return ImplantDAL.GetDataSet("SP_GetComplaintTypeByClient", myParams);
    }

    public DataSet GetBookingByClientForResolution(int ClientCoID,DateTime PickUpDate)
    {
        SqlParameter[] myParams = new SqlParameter[2];
        myParams[0] = new SqlParameter("@ClientCoID", SqlDbType.Int);
        myParams[0].Value = ClientCoID;
        myParams[1] = new SqlParameter("@PickUpDate", SqlDbType.Date);
        myParams[1].Value = PickUpDate;
        return ImplantDAL.GetDataSet("SP_GetBookingByClientForResolution", myParams);
    }
    	
    public string SaveImplantPerformance(int ClientCoId, int ImplantId, int CityId, DateTime SearchDate, int CorBooking, int CompetitorId, string CompetitorName, string CompetitorBooking, int BookingTakenByImplant, int BookingMDT, int ComplaintCount, int SMSFeedbackCount, int Performance,int ClientPerformance, int ComplaintCountIntsaCrm, string FeedbackType, string ActionTakenToimprove,int QualityOfCab,int ChauffeurComplaint,int ServiceFailure,int SOSComplaint,int Invoice_Billining,string BookingIdComplaintResolution,string CityWiseStatus,int CreatedBy)
    {
        string ReturnMsg=string.Empty;
        try
        {
            SqlParameter[] sqlParam = new SqlParameter[25];
            sqlParam[0] = new SqlParameter("@ClientCoId", SqlDbType.Int);
            sqlParam[0].Value = ClientCoId;
            sqlParam[1] = new SqlParameter("@ImplantId", SqlDbType.Int);
            sqlParam[1].Value = ImplantId;
            sqlParam[2] = new SqlParameter("@CityId", SqlDbType.Int);
            sqlParam[2].Value = CityId;
            sqlParam[3] = new SqlParameter("@SearchDate", SqlDbType.Date);
            sqlParam[3].Value = SearchDate;
            sqlParam[4] = new SqlParameter("@CorBooking", SqlDbType.Int);
            sqlParam[4].Value = CorBooking;
            sqlParam[5] = new SqlParameter("@CompetitorId", SqlDbType.Int);
            sqlParam[5].Value =CompetitorId;
            sqlParam[6] = new SqlParameter("@CompetitorName", SqlDbType.VarChar);
            sqlParam[6].Value = CompetitorName;
            sqlParam[7] = new SqlParameter("@CompetitorBooking", SqlDbType.VarChar);
            sqlParam[7].Value = CompetitorBooking;
            sqlParam[8] = new SqlParameter("@BookingTakenByImplant", SqlDbType.Int);
            sqlParam[8].Value = BookingTakenByImplant;
            sqlParam[9] = new SqlParameter("@BookingMDT", SqlDbType.Int);
            sqlParam[9].Value = BookingMDT;
            sqlParam[10] = new SqlParameter("@ComplaintCount", SqlDbType.Int);
            sqlParam[10].Value = ComplaintCount;
            sqlParam[11] = new SqlParameter("@SMSFeedbackCount", SqlDbType.Int);
            sqlParam[11].Value = SMSFeedbackCount;
            sqlParam[12] = new SqlParameter("@Performance", SqlDbType.Int);
            sqlParam[12].Value = Performance;
            sqlParam[13] = new SqlParameter("@ClientPerformance", SqlDbType.Int);
            sqlParam[13].Value = ClientPerformance;

            sqlParam[14] = new SqlParameter("@ComplaintCountIntsaCrm", SqlDbType.Int);
            sqlParam[14].Value = ComplaintCountIntsaCrm;
            sqlParam[15] = new SqlParameter("@FeedbackType", SqlDbType.VarChar);
            sqlParam[15].Value = FeedbackType;
            sqlParam[16] = new SqlParameter("@ActionTakenToimprove", SqlDbType.VarChar);
            sqlParam[16].Value = ActionTakenToimprove;
            
            sqlParam[17] = new SqlParameter("@QualityOfCab", SqlDbType.Int);
            sqlParam[17].Value = QualityOfCab;
            sqlParam[18] = new SqlParameter("@ChauffeurComplaint", SqlDbType.Int);
            sqlParam[18].Value = ChauffeurComplaint;
            sqlParam[19] = new SqlParameter("@ServiceFailure", SqlDbType.Int);
            sqlParam[19].Value = ServiceFailure;
            sqlParam[20] = new SqlParameter("@SOSComplaint", SqlDbType.Int);
            sqlParam[20].Value = SOSComplaint;
            sqlParam[21] = new SqlParameter("@Invoice_Billining", SqlDbType.Int);
            sqlParam[21].Value = Invoice_Billining;

            sqlParam[22] = new SqlParameter("@BookingIdComplaintResolution", SqlDbType.VarChar);
            sqlParam[22].Value = BookingIdComplaintResolution;
            sqlParam[23] = new SqlParameter("@CityWiseStatus", SqlDbType.VarChar);
            sqlParam[23].Value = CityWiseStatus;
            sqlParam[24] = new SqlParameter("@CreatedBy", SqlDbType.Int);
            sqlParam[24].Value = CreatedBy;
            int i = ImplantDAL.ExecuteNonQuery("SP_SaveImplantPerformance", sqlParam);
           if (i > 0)
           {
               ReturnMsg = "Save Successfully";
           }
           else
           {
               ReturnMsg = "Not Save Successfully";
           }
           
        }
        catch (Exception ex)
        {

            ReturnMsg = ex.Message.ToString();

        }
        return ReturnMsg;

    }
   public string SaveImplantResolution(int ClientCoId, int ImplantId, int ImplantCityId, DateTime SearchDate,int CompetitorId, string CompetitorName, int @BookingId,string ResolutionProvided)
    {
        string ReturnMsg = string.Empty;
        try
        {
            SqlParameter[] sqlParam = new SqlParameter[8];
            sqlParam[0] = new SqlParameter("@ClientCoId", SqlDbType.Int);
            sqlParam[0].Value = ClientCoId;
            sqlParam[1] = new SqlParameter("@ImplantId", SqlDbType.Int);
            sqlParam[1].Value = ImplantId;
            sqlParam[2] = new SqlParameter("@ImplantCityId", SqlDbType.Int);
            sqlParam[2].Value = ImplantCityId;
            sqlParam[3] = new SqlParameter("@SearchDate", SqlDbType.Date);
            sqlParam[3].Value = SearchDate;

            sqlParam[4] = new SqlParameter("@CompetitorId", SqlDbType.Int);
            sqlParam[4].Value = CompetitorId;
            sqlParam[5] = new SqlParameter("@CompetitorName", SqlDbType.VarChar);
            sqlParam[5].Value = CompetitorName;
            sqlParam[6] = new SqlParameter("@BookingId", SqlDbType.Int);
            sqlParam[6].Value = BookingId;
            sqlParam[7] = new SqlParameter("@ResolutionProvided", SqlDbType.VarChar);
            sqlParam[7].Value = ResolutionProvided;
            
            int i = ImplantDAL.ExecuteNonQuery("SP_SaveImplantResolution", sqlParam);
            if (i > 0)
            {
                ReturnMsg = "Save Successfully";
            }
            else
            {
                ReturnMsg = "Not Save Successfully";
            }

        }
        catch (Exception ex)
        {

            ReturnMsg = ex.Message.ToString();

        }
        return ReturnMsg;

    }

   public DataSet GetImplantDetails(int ClientCoID)
   {
       SqlParameter[] myParams = new SqlParameter[1];
       myParams[0] = new SqlParameter("@ClientCoID", SqlDbType.Int);
       myParams[0].Value = ClientCoID;
       return ImplantDAL.GetDataSet("SP_GetImplant", myParams);
   }
   public DataSet GetImplantDetails(int ClientCoID,int CityId,int ImplantId,DateTime FromDate,DateTime ToDate)
   {
       SqlParameter[] myParams = new SqlParameter[5];
       myParams[0] = new SqlParameter("@ClientCoId", SqlDbType.Int);
       myParams[0].Value = ClientCoID;
       myParams[1] = new SqlParameter("@ImplantId", SqlDbType.Int);
       myParams[1].Value = ImplantId;
       myParams[2] = new SqlParameter("@CityId", SqlDbType.Int);
       myParams[2].Value = @CityId;
       myParams[3] = new SqlParameter("@FromDate", SqlDbType.Date);
       myParams[3].Value = @FromDate;
       myParams[4] = new SqlParameter("@ToDate", SqlDbType.Date);
       myParams[4].Value = @ToDate;
       return ImplantDAL.GetDataSet("SP_ImplantPerformanceExportToExcel", myParams);
   }
   public DataSet GetImplantCity(int ImplantID)
   {
       SqlParameter[] myParams = new SqlParameter[1];
       myParams[0] = new SqlParameter("@ImplantID", SqlDbType.Int);
       myParams[0].Value = @ImplantID;
       return ImplantDAL.GetDataSet("SP_GetImplantCity", myParams);
   }

   public SqlDataReader SaveCompetitor(int ClientCoId,string ClientName,string CompetitorName,int CreatedBy)
   {
        SqlParameter[] sqlParam = new SqlParameter[4];
        sqlParam[0] = new SqlParameter("@ClientCoId", SqlDbType.Int);
        sqlParam[0].Value = ClientCoId;
        sqlParam[1] = new SqlParameter("@ClientCoName", SqlDbType.VarChar);
        sqlParam[1].Value = ClientName;
        sqlParam[2] = new SqlParameter("@CompetitorName", SqlDbType.VarChar);
        sqlParam[2].Value = CompetitorName;
        sqlParam[3] = new SqlParameter("@CreatedBy", SqlDbType.Int);
        sqlParam[3].Value = CreatedBy;
        return ImplantDAL.ExecuteReader("SP_SaveCompetitor", sqlParam);
      
     }
}