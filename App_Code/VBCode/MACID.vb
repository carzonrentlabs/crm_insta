Imports Microsoft.VisualBasic

Public Class MACID
    Public Function getMAC() As String
        Dim strMac As String

        Dim txtMAC = String.Empty
        Dim mc As System.Management.ManagementClass
        Dim mo As System.Management.ManagementObject
        mc = New System.Management.ManagementClass("Win32_NetworkAdapterConfiguration")
        Dim moc As System.Management.ManagementObjectCollection = mc.GetInstances()
        For Each mo In moc
            If mo.Item("IPEnabled") = True Then
                strMac = mo.Item("MacAddress").ToString()
                Return strMac
            End If
        Next
        Return strMac
    End Function
End Class
