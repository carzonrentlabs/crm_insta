using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace Insta
{

    /// <summary>
    /// Summary description for CRM_Insta
    /// </summary>
    public class CRM_Insta
    {
        public CRM_Insta()
        {
            //
            // TODO: Add constructor logic here
            //
        }        
        
        public static object GetUserName(int SysuserID)
        {
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["CRDMenu"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);          
            SqlCommand Cmd = new SqlCommand();

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@SysuserID", SysuserID);
            Cmd.CommandText="GetUserNameById";
            Cmd.CommandType= CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(param);
            Cmd.Connection = myconn;
            myconn.Open();
            return Cmd.ExecuteScalar();
            myconn.Close();

        }

      public static  string RegisterComplaint
      (
        int BranchID
      , int JobLocationID
      , string JobID
      , DateTime JobDate
      , string Job_From
      , string Job_To
      , string CustomerName
      , string CustomerContact
      , string ContactDetails
      , string Remarks
      , string CabID
      , string ChauffeurName
      , string ChauffeurContact
      , int ComplaintSourceID
      , DateTime ComplaintDateTime
      , string ComplaintDescription
      , string RegisteredBy
      , DateTime Reg_DateTime
      , int ResolutionAt
      , int PriorityID
      , int ComplaintStatus
      , string CarCategory
      , int SubCategoryId
      , string guestEmailId
      , string CarNo
      )
        {
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            SqlDataReader Myreader;
            string Message = "";
            try
            {
                if (Remarks == "")
                {
                    Remarks = "0";
                }



                if (ComplaintDescription == "")
                {
                    ComplaintDescription = "0";
                }

               

                SqlCommand Insertbulktransaction = new SqlCommand("BL_CRM_RegisterComplaints", myconn);
                Insertbulktransaction.Connection = myconn;
                Insertbulktransaction.CommandType = CommandType.StoredProcedure;


                Insertbulktransaction.Parameters.AddWithValue("@BranchID", SqlDbType.Int).Value = BranchID;
                Insertbulktransaction.Parameters.AddWithValue("@JobLocationID", SqlDbType.Int).Value = JobLocationID;
                Insertbulktransaction.Parameters.AddWithValue("@JobID", SqlDbType.VarChar).Value = JobID;
                Insertbulktransaction.Parameters.AddWithValue("@JobDate", SqlDbType.DateTime).Value = JobDate;
                Insertbulktransaction.Parameters.AddWithValue("@Job_From", SqlDbType.VarChar).Value = Job_From;
                Insertbulktransaction.Parameters.AddWithValue("@Job_To", SqlDbType.VarChar).Value = Job_To;
                Insertbulktransaction.Parameters.AddWithValue("@CustomerName", SqlDbType.VarChar).Value = CustomerName;
                Insertbulktransaction.Parameters.AddWithValue("@CustomerContact", SqlDbType.VarChar).Value = CustomerContact;
                Insertbulktransaction.Parameters.AddWithValue("@ContactDetails", SqlDbType.VarChar).Value = ContactDetails;
                Insertbulktransaction.Parameters.AddWithValue("@Remarks", SqlDbType.VarChar).Value = Remarks;
                Insertbulktransaction.Parameters.AddWithValue("@CabID", SqlDbType.VarChar).Value = CabID;
                Insertbulktransaction.Parameters.AddWithValue("@ChauffeurName", SqlDbType.VarChar).Value = ChauffeurName;
                Insertbulktransaction.Parameters.AddWithValue("@ChauffeurContact", SqlDbType.Decimal).Value = ChauffeurContact;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintSourceID", SqlDbType.Int).Value = ComplaintSourceID;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintDateTime", SqlDbType.DateTime).Value = ComplaintDateTime;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintDescription", SqlDbType.VarChar).Value = ComplaintDescription;
                Insertbulktransaction.Parameters.AddWithValue("@RegisteredBy", SqlDbType.VarChar).Value = RegisteredBy;
                Insertbulktransaction.Parameters.AddWithValue("@Reg_DateTime", SqlDbType.DateTime).Value = Reg_DateTime;
                Insertbulktransaction.Parameters.AddWithValue("@ResolutionAt", SqlDbType.Int).Value = ResolutionAt;
                Insertbulktransaction.Parameters.AddWithValue("@PriorityID", SqlDbType.Int).Value = PriorityID;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintStatus", SqlDbType.Int).Value = ComplaintStatus;
                Insertbulktransaction.Parameters.AddWithValue("@CarCategory", SqlDbType.VarChar).Value = CarCategory;
                Insertbulktransaction.Parameters.AddWithValue("@SubCategoryId", SqlDbType.Int).Value = SubCategoryId;
                Insertbulktransaction.Parameters.AddWithValue("@guestEmailId", SqlDbType.VarChar).Value = guestEmailId;
                Insertbulktransaction.Parameters.AddWithValue("@CarNo", SqlDbType.VarChar).Value = CarNo;

                myconn.Open();

                Myreader = Insertbulktransaction.ExecuteReader();

                if (Myreader.Read())
                {
                    Message = Myreader[0].ToString();
                }
                else
                {
                    Message = "Retry";
                }
            }
            catch (Exception ex)
            {
                Message = "CRM under Maintenance";
            }
            myconn.Close();
            return Message;

        }

        public static DataSet ListCrmMonthlyReoport(int BranchID, DateTime FromDate, DateTime ToDate, int IsExport)
        {
            string SProc = "[BL_CRM_ListCrmMonthlyReoport]";

            SqlParameter[] myParams = new SqlParameter[4];

            myParams[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[0].Value = BranchID;

            myParams[1] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            myParams[1].Value = FromDate.ToShortDateString();

            myParams[2] = new SqlParameter("@ToDate", SqlDbType.DateTime);
            myParams[2].Value = ToDate.ToShortDateString();
            myParams[3] = new SqlParameter("@IsExport", SqlDbType.Int);
            myParams[3].Value = IsExport;

            //string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            //SqlConnection myconn = new SqlConnection(ConString);
            //myconn.Open();
            //SqlCommand Cmd = new SqlCommand(SProc, myconn);
            //Cmd.CommandType = CommandType.StoredProcedure;
            //Cmd.Parameters.AddRange(myParams);
            //SqlDataAdapter da = new SqlDataAdapter(Cmd); 
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteEasyCabsDataset(SProc, myParams);
            //da.Fill(ds);
            //myconn.Close();
            return ds;
        }


        public static DataSet CSAT_CSATSuggestionReport(int BranchID, DateTime FromDate, DateTime ToDate)
        {
            string SProc = "[BL_CRM_CSAT_CSATSuggestionReport]";            
            SqlParameter[] myParams = new SqlParameter[3];
            myParams[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            myParams[0].Value = FromDate;

            myParams[1] = new SqlParameter("@ToDate", SqlDbType.DateTime);
            myParams[1].Value = ToDate;

            myParams[2] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[2].Value = BranchID;          
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
        }

        public static DataSet CSAT_LimoSuggestionReport(int BranchID, DateTime FromDate, DateTime ToDate)
        {
            string SProc = "[BL_CRM_CSAT_LimoSuggestionReport]";
            SqlParameter[] myParams = new SqlParameter[3];
            myParams[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            myParams[0].Value = FromDate;

            myParams[1] = new SqlParameter("@ToDate", SqlDbType.DateTime);
            myParams[1].Value = ToDate;

            myParams[2] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[2].Value = BranchID;
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
        }

        public static DataSet CSAT_TripsSuggestionReport(int BranchID, DateTime FromDate, DateTime ToDate)
        {
            string SProc = "[BL_CRM_CSAT_TripsSuggestionReport]";
            SqlParameter[] myParams = new SqlParameter[3];
            myParams[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            myParams[0].Value = FromDate;

            myParams[1] = new SqlParameter("@ToDate", SqlDbType.DateTime);
            myParams[1].Value = ToDate;

            myParams[2] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[2].Value = BranchID;
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
        }

        public static  DataSet ListLIMO_CSAT_Report(int BranchID, DateTime StartDate, DateTime EndDate)
        {
            string SProc = "[BL_CRM_LIMO_CSAT_CSAT_Report]";

            SqlParameter[] myParams = new SqlParameter[3];

            myParams[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[0].Value = BranchID;

            myParams[1] = new SqlParameter("@StartDate", SqlDbType.DateTime);
            myParams[1].Value = StartDate;

            myParams[2] = new SqlParameter("@EndDate", SqlDbType.DateTime);
            myParams[2].Value = EndDate;

            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
            
        }

        public static DataSet ListTrips_CSAT_Report(int BranchID, DateTime StartDate, DateTime EndDate)
        {
            string SProc = "[BL_CRM_Trips_CSAT_CSAT_Report]";

            SqlParameter[] myParams = new SqlParameter[3];

            myParams[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[0].Value = BranchID;

            myParams[1] = new SqlParameter("@StartDate", SqlDbType.DateTime);
            myParams[1].Value = StartDate;

            myParams[2] = new SqlParameter("@EndDate", SqlDbType.DateTime);
            myParams[2].Value = EndDate;

            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;

        }

        public static DataSet GetCRMData(int Month, int Year)
        {
            string SProc = "[BL_SP_GetCRMData]";

            SqlParameter[] myParams = new SqlParameter[2];

            myParams[0] = new SqlParameter("@Month", SqlDbType.Int);
            myParams[0].Value = Month;

            myParams[1] = new SqlParameter("@year", SqlDbType.Int);
            myParams[1].Value = Year;
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
        }

        public DataSet Limo_ListQuestionnaire()
        {
            string SProc = "[BL_CRM_CSAT_Limo_ListQuestionnaire]";
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;           
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
        }

        public static SqlDataReader LIMO_CsatSummary <I>(I BranchID)
        {
            string SProc = "[BL_CRM_LIMO_CSAT_Summary]";

            SqlParameter[] myParams = new SqlParameter[1];

            myParams[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[0].Value = BranchID;
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataReader dr = Cmd.ExecuteReader();
            return dr;
            
        }

        public static DataSet LimoReadRandomNextRecord<I>(I BranchID, I Division)
        {
            string SProc = "[BL_CRM_Limo_CSAT_ReadRandomRecord]";            
            SqlParameter[] myParams = new SqlParameter[2];

            myParams[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[0].Value = BranchID;

            myParams[1] = new SqlParameter("@Division", SqlDbType.Int);
            myParams[1].Value = Division;

            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
            
        }

        public static SqlDataReader TRIPS_CsatSummary<I>(I BranchID)
        {
            string SProc = "[BL_CRM_TRIPS_CSAT_Summary]";

            SqlParameter[] myParams = new SqlParameter[1];

            myParams[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[0].Value = BranchID;
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataReader dr = Cmd.ExecuteReader();
            return dr;

        }

        public static DataSet TripsReadRandomNextRecord<I>(I BranchID, I Division)
        {
            string SProc = "[BL_CRM_Trips_CSAT_ReadRandomRecord]";
            SqlParameter[] myParams = new SqlParameter[2];

            myParams[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[0].Value = BranchID;

            myParams[1] = new SqlParameter("@Division", SqlDbType.Int);
            myParams[1].Value = Division;

            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;

        }

        public DataSet Trips_ListQuestionnaire()
        {
            string SProc = "[BL_CRM_CSAT_TRIPS_ListQuestionnaire]";
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
        }
        public DataSet GetSubComplaintSubType(int ComplainTypeID)
        {
            DataSet ds = new DataSet();
            try
            {   

                string SProc = "[Prc_GetSubComplaintype]";
                SqlParameter[] myParams = new SqlParameter[1];

                myParams[0] = new SqlParameter("@ComplainTypeId", SqlDbType.Int);
                myParams[0].Value = ComplainTypeID;

                string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
                SqlConnection myconn = new SqlConnection(ConString);
                myconn.Open();
                SqlCommand Cmd = new SqlCommand(SProc, myconn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddRange(myParams);
                SqlDataAdapter da = new SqlDataAdapter(Cmd);
                
                da.Fill(ds);
                myconn.Close();
                return ds;

            }
            catch (Exception Ex)
            {
                ds = null;
                new Exception(Ex.Message);
            }
            return ds;
        }
        public int GetResolutionId(int ComplainTypeID)
        {
            DataSet ds = new DataSet();
            int ResolutionId = 0;
            string SProc = "[Prc_GetResolutionRequired]";
            SqlParameter[] myParams = new SqlParameter[1];

            myParams[0] = new SqlParameter("@ComplainTypeId", SqlDbType.Int);
            myParams[0].Value = ComplainTypeID;

            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            da.Fill(ds);
            myconn.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ResolutionId = Convert.ToInt32(ds.Tables[0].Rows[0]["EntityValue"].ToString());
            }
            return ResolutionId;
        
        }

        public DataSet GetMailDetails(int complaintId)
        {
            DataSet ds = new DataSet();
            try
            {

                string SProc = "Prc_DRMFeedBackSendMail";
                SqlParameter[] myParams = new SqlParameter[1];

                myParams[0] = new SqlParameter("@ComplaintId", SqlDbType.Int);
                myParams[0].Value = complaintId;

                string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
                SqlConnection myconn = new SqlConnection(ConString);
                myconn.Open();
                SqlCommand Cmd = new SqlCommand(SProc, myconn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddRange(myParams);
                SqlDataAdapter da = new SqlDataAdapter(Cmd);
                da.Fill(ds);
                myconn.Close();

            }
            catch (Exception Ex)
            {
                ds = null;
                new Exception(Ex.Message);
            }
            return ds;
        
        }

        public static SqlDataReader SearchComplaint_Job(int JobID)
        {
            string SProc = "PRC_ListcomplaintDetails";

            SqlParameter[] myParams = new SqlParameter[1];

            myParams[0] = new SqlParameter("@JobID", SqlDbType.Int);
            myParams[0].Value = JobID;
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataReader dr = Cmd.ExecuteReader();
            return dr;

        }

        public DataSet GetComplaintSubCat(int ComplainCatId)
        {
            DataSet ds = new DataSet();
            try
            {

                string SProc = "[Prc_GetCompSubCat]";
                SqlParameter[] myParams = new SqlParameter[1];

                myParams[0] = new SqlParameter("@ComplainCatId", SqlDbType.Int);
                myParams[0].Value = ComplainCatId;

                string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
                SqlConnection myconn = new SqlConnection(ConString);
                myconn.Open();
                SqlCommand Cmd = new SqlCommand(SProc, myconn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddRange(myParams);
                SqlDataAdapter da = new SqlDataAdapter(Cmd);

                da.Fill(ds);
                myconn.Close();
                return ds;

            }
            catch (Exception Ex)
            {
                ds = null;
                new Exception(Ex.Message);
            }
            return ds;
        }

        public DataSet GetChauffeurDetails(int BookingId)
        {
            DataSet ds = new DataSet();
            try
            {

                string SProc = "[SP_GetChauffeurDetails]";
                SqlParameter[] myParams = new SqlParameter[1];

                myParams[0] = new SqlParameter("@BookingId", SqlDbType.Int);
                myParams[0].Value = BookingId;

                string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["CRDMenu"].ToString();
                SqlConnection myconn = new SqlConnection(ConString);
                myconn.Open();
                SqlCommand Cmd = new SqlCommand(SProc, myconn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddRange(myParams);
                SqlDataAdapter da = new SqlDataAdapter(Cmd);

                da.Fill(ds);
                myconn.Close();
                return ds;

            }
            catch (Exception Ex)
            {
                ds = null;
                new Exception(Ex.Message);
            }
            return ds;
        }
       //public string DoResolution(int ComplaintID, string Resolution, string @ResolutionBy, int @ComplaintStatus)
        public string DoResolution(int ComplaintID, string Resolution, string ResolutionBy, int ComplaintStatus,char WasChauffeurFault,string ChauffeurAllocated,int ComplaintResolutionCatId,int ComplaintResolutionSubCatId,string ComplaintResolutionRemarks,int ComplaintResolutionCarId,string ComplaintResolutionCarRegNo)
        {
            string Msg = "";
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            try
            {
                SqlCommand Insertbulktransaction = new SqlCommand("[BL_CRM_UpdateResolutionNew]", myconn);
                Insertbulktransaction.Connection = myconn;
                Insertbulktransaction.CommandType = CommandType.StoredProcedure;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintID", SqlDbType.Int).Value = ComplaintID;
                Insertbulktransaction.Parameters.AddWithValue("@Resolution", SqlDbType.VarChar).Value = Resolution;
                Insertbulktransaction.Parameters.AddWithValue("@ResolutionBy", SqlDbType.VarChar).Value = ResolutionBy;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintStatus", SqlDbType.Int).Value = ComplaintStatus;

                Insertbulktransaction.Parameters.AddWithValue("@WasChauffeurFault", SqlDbType.Char).Value = WasChauffeurFault;
                Insertbulktransaction.Parameters.AddWithValue("@ChauffeurAllocated", SqlDbType.VarChar).Value = ChauffeurAllocated;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintResolutionCatId", SqlDbType.Int).Value = ComplaintResolutionCatId;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintResolutionSubCatId", SqlDbType.Int).Value = ComplaintResolutionSubCatId;
               
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintResolutionRemarks", SqlDbType.VarChar).Value = ComplaintResolutionRemarks;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintResolutionCarId", SqlDbType.Int).Value = ComplaintResolutionCarId;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintResolutionCarRegNo", SqlDbType.VarChar).Value = ComplaintResolutionCarRegNo;
                
                myconn.Open();
                Msg = Insertbulktransaction.ExecuteScalar().ToString();

            }
            catch (Exception e)
            {
                Msg = "CRM under maintenance";
            }
            myconn.Close();
            return Msg;

        }
        ///////////////////
        public string RegisterComplaintService(int BranchID,int JobLocationID,string JobID,DateTime JobDate,string Job_From,string Job_To,string CustomerName
      , string CustomerContact
      , string ContactDetails
      , string Remarks
      , string CabID
      , string ChauffeurName
      , string ChauffeurContact
      , int ComplaintSourceID
      , DateTime ComplaintDateTime
      , string ComplaintDescription
      , string RegisteredBy
      , DateTime Reg_DateTime
      , int ResolutionAt
      , int PriorityID
      , int ComplaintStatus
      , string CarCategory
      , int SubCategoryId
      , string guestEmailId
      , string CarNo
      ,string TotalUsageTime 
      ,string TotalUsageKms
      ,string TotalParkingTollExtra
      ,string Concern 
      ,string ConcernText 

            
            
           )
        {
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            SqlDataReader Myreader;
            string Message = "";
            try
            {
                //ComplaintId = string.Empty;
                //SqlParameter[] sqlParam = new SqlParameter[25];
                //sqlParam[0] = new SqlParameter("@BusinessTypeId", SqlDbType.Int);
                //sqlParam[0].Value = BusinessTypeId;
                //sqlParam[1] = new SqlParameter("@BranchId", SqlDbType.Int);
                //sqlParam[1].Value = BranchId;
                //sqlParam[2] = new SqlParameter("@ComplaintCityId", SqlDbType.Int);
                //sqlParam[2].Value = ComplaintCityId;
                //sqlParam[3] = new SqlParameter("@ComplaintCity", SqlDbType.VarChar);
                //sqlParam[3].Value = ComplaintCity;
                //sqlParam[4] = new SqlParameter("@ChauffeurName", SqlDbType.VarChar);
                //sqlParam[4].Value = ChauffeurName;
                //sqlParam[5] = new SqlParameter("@ChauffeurContactNo", SqlDbType.Decimal);
                //sqlParam[5].Value = Convert.ToDecimal(ChauffeurContactNo);
                //sqlParam[6] = new SqlParameter("@CarVendorName", SqlDbType.VarChar);
                //sqlParam[6].Value = CarVendorName;
                //sqlParam[7] = new SqlParameter("@CarVendorContactNo", SqlDbType.Decimal);
                //sqlParam[7].Value = Convert.ToDecimal(CarVendorContactNo);

                //sqlParam[8] = new SqlParameter("@CarCatName", SqlDbType.VarChar);
                //sqlParam[8].Value = CarCatName;
                //sqlParam[9] = new SqlParameter("@CarRegNo", SqlDbType.VarChar);
                //sqlParam[9].Value = CarRegNo;
                //sqlParam[10] = new SqlParameter("@Cabid", SqlDbType.Int);
                //sqlParam[10].Value = Cabid;
                //sqlParam[11] = new SqlParameter("@RMName", SqlDbType.VarChar);
                //sqlParam[11].Value = RMName;
                //sqlParam[12] = new SqlParameter("@RMContactNo", SqlDbType.Decimal);
                //sqlParam[12].Value = Convert.ToDecimal(RMContactNo);
                //sqlParam[13] = new SqlParameter("@ComplaintMode", SqlDbType.VarChar);
                //sqlParam[13].Value = ComplaintMode;
                //sqlParam[14] = new SqlParameter("@ComplaintTypeId", SqlDbType.Int);
                //sqlParam[14].Value = ComplaintTypeId;

                //sqlParam[15] = new SqlParameter("@ComplaintSubTypeId", SqlDbType.Int);
                //sqlParam[15].Value = ComplaintSubTypeId;

                //sqlParam[16] = new SqlParameter("@ComplaintDesc", SqlDbType.VarChar);
                //sqlParam[16].Value = ComplaintDesc;
                //sqlParam[17] = new SqlParameter("@ComplaintDateTime", SqlDbType.DateTime);
                //sqlParam[17].Value = ComplaintDateTime;
                //sqlParam[18] = new SqlParameter("@ComplaintRegisteredBy", SqlDbType.VarChar);
                //sqlParam[18].Value = ComplaintRegisteredBy;

                //sqlParam[19] = new SqlParameter("@ComplaintId", SqlDbType.Int);
                //sqlParam[19].Value = ComplaintId;
                //sqlParam[19].Direction = ParameterDirection.Output;

                //sqlParam[20] = new SqlParameter("@ResolutionStatus", SqlDbType.VarChar);
                //sqlParam[20].Value = ResolutionStatus;

                //sqlParam[21] = new SqlParameter("@Caller", SqlDbType.VarChar);
                //sqlParam[21].Value = calller;

                //sqlParam[22] = new SqlParameter("@Misbehaviour_Abusive", SqlDbType.VarChar);
                //sqlParam[22].Value = misbehaviour;

                //sqlParam[23] = new SqlParameter("@BookingId", SqlDbType.VarChar);
                //sqlParam[23].Value = BookingId;

                //sqlParam[24] = new SqlParameter("@AlternateContactNo", SqlDbType.Decimal);
                //sqlParam[24].Value = Convert.ToDecimal(AlternateContactNo);

                ////DRMDAL.ExecuteNonQuery("BL_DRM_ComplaintsRegister", sqlParam);
                //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "BL_CRM_RegisterComplaints", sqlParam);
                ////return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveNotAvailableReason", ObjparamUser);

               

                //ComplaintId = sqlParam[19].Value.ToString();

                SqlCommand Insertbulktransaction = new SqlCommand("BL_CRM_RegisterComplaintsCorDrive", myconn);
                Insertbulktransaction.Connection = myconn;
                Insertbulktransaction.CommandType = CommandType.StoredProcedure;
                Insertbulktransaction.Parameters.AddWithValue("@BranchID", SqlDbType.Int).Value = BranchID;
                Insertbulktransaction.Parameters.AddWithValue("@JobLocationID", SqlDbType.Int).Value = JobLocationID;
                Insertbulktransaction.Parameters.AddWithValue("@JobID", SqlDbType.VarChar).Value = JobID;
                Insertbulktransaction.Parameters.AddWithValue("@JobDate", SqlDbType.DateTime).Value = JobDate;
                Insertbulktransaction.Parameters.AddWithValue("@Job_From", SqlDbType.VarChar).Value = Job_From;
                Insertbulktransaction.Parameters.AddWithValue("@Job_To", SqlDbType.VarChar).Value = Job_To;
                Insertbulktransaction.Parameters.AddWithValue("@CustomerName", SqlDbType.VarChar).Value = CustomerName;
                Insertbulktransaction.Parameters.AddWithValue("@CustomerContact", SqlDbType.VarChar).Value = CustomerContact;
                Insertbulktransaction.Parameters.AddWithValue("@ContactDetails", SqlDbType.VarChar).Value = ContactDetails;
                Insertbulktransaction.Parameters.AddWithValue("@Remarks", SqlDbType.VarChar).Value = Remarks;
                Insertbulktransaction.Parameters.AddWithValue("@CabID", SqlDbType.VarChar).Value = CabID;
                Insertbulktransaction.Parameters.AddWithValue("@ChauffeurName", SqlDbType.VarChar).Value = ChauffeurName;
                Insertbulktransaction.Parameters.AddWithValue("@ChauffeurContact", SqlDbType.Decimal).Value = ChauffeurContact;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintSourceID", SqlDbType.Int).Value = ComplaintSourceID;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintDateTime", SqlDbType.DateTime).Value = ComplaintDateTime;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintDescription", SqlDbType.VarChar).Value = ComplaintDescription;
                Insertbulktransaction.Parameters.AddWithValue("@RegisteredBy", SqlDbType.VarChar).Value = RegisteredBy;
                Insertbulktransaction.Parameters.AddWithValue("@Reg_DateTime", SqlDbType.DateTime).Value = Reg_DateTime;
                Insertbulktransaction.Parameters.AddWithValue("@ResolutionAt", SqlDbType.Int).Value = ResolutionAt;
                Insertbulktransaction.Parameters.AddWithValue("@PriorityID", SqlDbType.Int).Value = PriorityID;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintStatus", SqlDbType.Int).Value = ComplaintStatus;
                Insertbulktransaction.Parameters.AddWithValue("@CarCategory", SqlDbType.VarChar).Value = CarCategory;
                Insertbulktransaction.Parameters.AddWithValue("@SubCategoryId", SqlDbType.Int).Value = SubCategoryId;
                Insertbulktransaction.Parameters.AddWithValue("@guestEmailId", SqlDbType.VarChar).Value = guestEmailId;
                Insertbulktransaction.Parameters.AddWithValue("@CarNo", SqlDbType.VarChar).Value = CarNo;

                Insertbulktransaction.Parameters.AddWithValue("@TotalUsageTime", SqlDbType.VarChar).Value = TotalUsageTime;
                Insertbulktransaction.Parameters.AddWithValue("@TotalUsageKms", SqlDbType.Decimal).Value = TotalUsageKms;
                Insertbulktransaction.Parameters.AddWithValue("@TotalParkingTollExtra", SqlDbType.Decimal).Value = TotalParkingTollExtra;
                Insertbulktransaction.Parameters.AddWithValue("@Concern", SqlDbType.VarChar).Value = Concern;
                Insertbulktransaction.Parameters.AddWithValue("@ConcernText", SqlDbType.VarChar).Value = ConcernText;

                

                myconn.Open();

                Myreader = Insertbulktransaction.ExecuteReader();

                if (Myreader.Read())
                {
                    Message = Myreader[0].ToString();
                }
                else
                {
                    Message = "Retry";
                }
            }
            catch (Exception ex)
            {
                Message = "CRM under Maintenance";
            }
            myconn.Close();
            return Message;

        }
        public DataSet ListCrmLocations(string UserID)
        {
            //string SProc = "[BL_CRM_ListCrmLocations]";

            //SqlParameter[] myParams = new SqlParameter[1];

            //myParams[0] = new SqlParameter("@UserID", SqlDbType.VarChar);
            //myParams[0].Value = UserID;

            //return DbConnect.GetDataSet(SProc, myParams);
            DataSet ds = new DataSet();
            try
            {

                string SProc = "[BL_CRM_ListCrmLocations]";
                SqlParameter[] myParams = new SqlParameter[1];

                myParams[0] = new SqlParameter("@UserID", SqlDbType.VarChar);
                myParams[0].Value = UserID;

                string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
                SqlConnection myconn = new SqlConnection(ConString);
                myconn.Open();
                SqlCommand Cmd = new SqlCommand(SProc, myconn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddRange(myParams);
                SqlDataAdapter da = new SqlDataAdapter(Cmd);

                da.Fill(ds);
                myconn.Close();
                return ds;

            }
            catch (Exception Ex)
            {
                ds = null;
                new Exception(Ex.Message);
            }
            return ds;
        }

        public DataSet GetPenality()
        {
            DataSet ds = new DataSet();
            try
            {

                string SProc = "[SP_GetPenalityReason]";
                string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
                SqlConnection myconn = new SqlConnection(ConString);
                myconn.Open();
                SqlCommand Cmd = new SqlCommand(SProc, myconn);
                Cmd.CommandType = CommandType.StoredProcedure;
               // Cmd.Parameters.AddRange(myParams);
                SqlDataAdapter da = new SqlDataAdapter(Cmd);
                da.Fill(ds);
                myconn.Close();
                return ds;

            }
            catch (Exception Ex)
            {
                ds = null;
                new Exception(Ex.Message);
            }
            return ds;
        }
        public string AddChauffeurPenality(int ComplaintId, string CarRegNo, int CarNo, int ChauffeurId, string Remarks, int PenalityId,int CreatedBy, bool status,bool VendorCarYN,bool VendorChauffYN)
        {
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            SqlDataReader Myreader;
            string Message = "";
            try
            {
                SqlCommand Insertbulktransaction = new SqlCommand("[SP_InsertChauffeurPenality]", myconn);
                Insertbulktransaction.Connection = myconn;
                Insertbulktransaction.CommandType = CommandType.StoredProcedure;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintId", SqlDbType.Int).Value = ComplaintId;
                Insertbulktransaction.Parameters.AddWithValue("@CarRegNo", SqlDbType.VarChar).Value = CarRegNo;
                Insertbulktransaction.Parameters.AddWithValue("@CarNo", SqlDbType.Int).Value = CarNo;
                Insertbulktransaction.Parameters.AddWithValue("@ChauffeurId", SqlDbType.Int).Value = ChauffeurId;
                Insertbulktransaction.Parameters.AddWithValue("@Remarks", SqlDbType.VarChar).Value = Remarks;
                Insertbulktransaction.Parameters.AddWithValue("@PenalityId", SqlDbType.Int).Value = PenalityId;
                Insertbulktransaction.Parameters.AddWithValue("@CreatedBy", SqlDbType.Int).Value = CreatedBy;
                Insertbulktransaction.Parameters.AddWithValue("@status", SqlDbType.Bit).Value = status;
                Insertbulktransaction.Parameters.AddWithValue("@VendorCarYN", SqlDbType.Bit).Value = VendorCarYN;
                Insertbulktransaction.Parameters.AddWithValue("@VendorChauffYN", SqlDbType.Bit).Value = VendorChauffYN;
                myconn.Open();
                Myreader = Insertbulktransaction.ExecuteReader();
                if (Myreader.Read())
                {
                    Message = Myreader[0].ToString();
                }
                else
                {
                    Message = "Retry";
                }
            }
            catch (Exception ex)
            {
                Message = "CRM under Maintenance";
            }
            myconn.Close();
            return Message;

        }
        public string DeActivateOldPenality(int ComplaintId)
        {
            SqlDataReader Myreader;
            string Message = "";
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            SqlCommand Insertbulktransaction = new SqlCommand("[SP_UpdatePenality]", myconn);
            Insertbulktransaction.Connection = myconn;
            Insertbulktransaction.CommandType = CommandType.StoredProcedure;
            Insertbulktransaction.Parameters.AddWithValue("@ComplaintId", SqlDbType.Int).Value = ComplaintId;
            try
            {
                myconn.Open();
                Myreader = Insertbulktransaction.ExecuteReader();
                if (Myreader.Read())
                {
                    Message = Myreader[0].ToString();
                }
                else
                {
                    Message = "Retry";
                }
            }
            catch (Exception ex)
            {
                Message = "CRM under Maintenance";
            }
            myconn.Close();
            return Message;
                
        }

    }
    
}