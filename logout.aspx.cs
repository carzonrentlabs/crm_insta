﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data;
using Insta;
public partial class logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["UserID"] = null;
        Session["User"] = null;
        Session.Abandon();
        OL_LoginUser objLoginUserOL = null;
        LoginValidate loginLog = new LoginValidate();
        if (Session["UserID"] == null || string.IsNullOrEmpty(Session["UserID"].ToString ()))
        {
            objLoginUserOL = new OL_LoginUser();
            objLoginUserOL.MaxLoginID = Convert.ToString(Session["MaxLoginID"].ToString());
            DataTable dtInsert = loginLog.LogOUt(objLoginUserOL.MaxLoginID);            
            Response.Redirect("~/Login.aspx");
        }
       
    }
}