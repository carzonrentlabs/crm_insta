<%@ Page Language="C#" MasterPageFile="~/CRDMasterpage.master" Theme="TmSTheme" AutoEventWireup="true" CodeFile="UpdateEscalation.aspx.cs" Inherits="CRM_UpdateEscalation" Title="CRM Escalation" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Page" Runat="Server">
<fieldset class="FieldsetClassic">
<legend class="LegendClassic"> Escalation Matrix</legend>
<table width="100%" style="font-family: Tahoma">


    <tr>
        <td align="center" colspan="6">
        <table style="font-weight: bold; font-size: 9pt">
        <tr>
    <td align="right">
        Division</td>
    <td>
        :</td>
    <td align="left">
        <asp:DropDownList ID="ddlDivision" runat="server" AutoPostBack="True" DataSourceID="ODS_For_ddlDivision" DataTextField="DivisionName" DataValueField="DivisionID" OnSelectedIndexChanged="ddlDivision_SelectedIndexChanged">
        </asp:DropDownList>&nbsp;&nbsp;
    </td>
    <td align="right">
        Branch</td>
    <td>
        :</td>
    <td align="left">
        <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" DataSourceID="ODS_For_ddlBranch" DataTextField="BranchName" DataValueField="BranchID" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
        </asp:DropDownList>&nbsp;
    </td>
    
</tr>
        </table>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align="left" colspan="6" style="font-size: 8pt">
            <asp:GridView ID="gvEscMatrix" runat="server" BackColor="LightGoldenrodYellow" BorderColor="Tan"
                BorderWidth="1px" CellPadding="2" DataSourceID="ODS_For_gvEscMatrix" ForeColor="Black"
                GridLines="None" Width="100%" AutoGenerateColumns="False" OnRowUpdating="gvEscMatrix_RowUpdating">
                <FooterStyle BackColor="Tan" />
                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                <HeaderStyle BackColor="Tan" Font-Bold="True" />
                <AlternatingRowStyle BackColor="PaleGoldenrod" />
                <Columns>
                    <asp:BoundField DataField="LevelID" HeaderText="LevelID" />
                    <asp:BoundField DataField="Category" HeaderText="Category" />
                    <asp:BoundField DataField="TimeSpan" HeaderText="TimeSpan" />
                    <asp:BoundField DataField="RegisteredEmails" HeaderText="Emails" />
                    <asp:BoundField DataField="RegisteredPhone" HeaderText="Phone" />
                    <asp:BoundField DataField="CategoryID" HeaderText="CategoryID" InsertVisible="False" />
                    <asp:CommandField ShowEditButton="True" ButtonType="Button" />
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ODS_For_gvEscMatrix" runat="server" SelectMethod="List_EscalationMatrix"
                TypeName="CRM" UpdateMethod="UpdateMatrix" OnUpdating="ODS_For_gvEscMatrix_Updating">
                <UpdateParameters>
                    <asp:ControlParameter ControlID="ddlBranch" Name="BranchID" PropertyName="SelectedValue" Type="Int32" />
                    <asp:Parameter Name="LevelID" Type="Int32" />
                    <asp:Parameter Name="CategoryID" Type="Int32" />
                    <asp:Parameter Name="TimeSpan" Type="Double" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="Phone" Type="String" />
                    <asp:Parameter Name="Category" Type="String" />
                    <asp:Parameter Name="RegisteredEmails" Type="String" />
                    <asp:Parameter Name="RegisteredPhone" Type="String" />
                </UpdateParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddlBranch" Name="BranchID" PropertyName="SelectedValue"
                        Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td colspan="6">
        <div id="divLevel" runat="server">
        <table width="80%" style="font-size: 10pt">
            <tr>
                <td align="left" colspan="10" style="background-color: tan">
                    Enter Details for Level:<asp:Label ID="lblLabel" runat="server" Font-Bold="True"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" colspan="10">
                    <asp:GridView ID="gvMatrix" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow"
                        BorderColor="Tan" BorderWidth="1px" CellPadding="2" DataSourceID="ODS_For_gvMatrix"
                        ForeColor="Black" GridLines="None" Width="100%">
                        <FooterStyle BackColor="Tan" />
                        <Columns>
                            <asp:TemplateField HeaderText="Category">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("CategoryName") %>'></asp:Label>
                                    <asp:Label ID="lblCategoryID" runat="server" Text='<%# Bind("CategoryID") %>' Visible="False"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EmailID">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtEmailID" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mobile">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtMobile" runat="server" Width="113px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TimeSpan">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtTimeSpan" runat="server" Width="113px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                        <HeaderStyle BackColor="Tan" Font-Bold="True" />
                        <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="10">
                    <asp:Literal ID="ltrlMsg" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td align="center" colspan="10">
                    <asp:Button ID="btnSave" runat="server" Text="Save" Width="64px" OnClick="btnSave_Click" /></td>
            </tr>
        </table>
        </div>
        </td>
    </tr>
    <tr>
        <td colspan="6" style="font-size: 9pt">
            &nbsp;</td>
    </tr>
    <tr>
        <td>
        </td>
        <td style="font-weight: bold; width: 1%">
        </td>
        <td>
        </td>
        <td>
        </td>
        <td style="font-weight: bold; width: 1%">
        </td>
        <td>
        </td>
    </tr>

</table>
        <asp:ObjectDataSource ID="ODS_For_ddlDivision" runat="server" SelectMethod="List_CRM_Division"
            TypeName="CRM"></asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ODS_For_ddlBranch" runat="server" SelectMethod="List_BranchBy_DivisionID"
            TypeName="CRM">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlDivision" DefaultValue="0" Name="DivisionID"
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ODS_For_gvMatrix" runat="server" SelectMethod="List_EmptyMatrix" TypeName="CRM"></asp:ObjectDataSource>
</fieldset>
</asp:Content>

