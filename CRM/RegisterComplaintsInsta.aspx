<%@ Page Language="C#" Theme="TMSTheme" AutoEventWireup="true" CodeFile="RegisterComplaintsInsta.aspx.cs"
    Inherits="RegisterComplaintsInsta" Title="" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Insta : CRM</title>
    <link href="../App_Themes/stylesheet/HertzInt.css" rel="stylesheet" type="text/css" />

    <script type="text/jscript" src="../CalendarControl.js" language="javascript"></script>

    <script type="text/jscript" src="../JScript_DisableButton.js"></script>

    <script type="text/javascript" src="../JScript_ModalPopUp.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:Label ID="lblcomplaints" SkinID="HeadingTop" runat="server" Text="Register Complaints">
        </asp:Label>
        <br />
        <div>
            <table class="TmsClassic" width="100%">
                <tr>
                    <td valign="top">
                        <table class="TmsClassic_NoBorder_Back" width="100%">
                            <tr>
                                <td>
                                    <table class="TmsClassic_NoBorder" width="95%" style="text-align: left">
                                        <tr>
                                            <td align="right" colspan="8" style="font-size: 9pt; font-family: Tahoma" valign="top">
                                                <table width="100%">
                                                    <tr>
                                                        <td style="width: 50%">
                                                            <%--<asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="Maroon"
                                                                Text="CRM for :"></asp:Label>--%>
                                                        </td>
                                                        <td align="left" colspan="4" valign="middle" style="width: 50%; text-align: left;">
                                                            <asp:DropDownList Visible="false" ID="ddlLocation" runat="server" AutoPostBack="True"
                                                                DataSourceID="ODS_For_ddlLocation" DataTextField="BranchName" DataValueField="BranchID">
                                                            </asp:DropDownList>
                                                            <asp:ObjectDataSource ID="ODS_For_ddlLocation" runat="server" SelectMethod="ListCrmLocations"
                                                                TypeName="Systems.CRM.CRM_New">
                                                                <SelectParameters>
                                                                    <asp:SessionParameter Name="UserID" SessionField="User" Type="String" />
                                                                </SelectParameters>
                                                            </asp:ObjectDataSource>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="right" colspan="1" style="font-size: 9pt; font-family: Tahoma" valign="top">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8" class="TmsClassic_LeftH" align="left">
                                                Job Details :</td>
                                            <td align="left" class="TmsClassic_LeftH" colspan="1">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%; border-right: silver 1px solid; border-top: silver 1px solid;
                                                font-weight: bold; font-size: 8pt; border-left: silver 1px solid; border-bottom: silver 1px solid;
                                                font-family: Tahoma;" align="center" colspan="8">
                                                <asp:Label ID="lblEscInfo" runat="server" BackColor="Tan"></asp:Label>
                                            </td>
                                            <td align="center" colspan="1" style="border-right: silver 1px solid; border-top: silver 1px solid;
                                                font-weight: bold; font-size: 8pt; border-left: silver 1px solid; width: 100%;
                                                border-bottom: silver 1px solid; font-family: Tahoma">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="border-right: silver 2px solid; border-top: silver 2px solid;
                                                border-left: silver 2px solid; width: 50%; border-bottom: silver 2px solid; text-align: center">
                                            </td>
                                            <td colspan="3" style="font-size: 7pt; font-family: Tahoma">
                                                [CA=Cab,CH=Chauffeur,CC=Call Center,CM=Common]</td>
                                            <td colspan="1" style="font-size: 7pt; font-family: Tahoma">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; width: 24%; height: 31px;">
                                                Job Location</td>
                                            <td align="center" style="text-align: center; width: 1px; height: 31px;">
                                                :</td>
                                            <td style="height: 31px">
                                                <asp:DropDownList ID="ddlJobLocation" runat="server" AutoPostBack="false" Enabled="false"
                                                    OnSelectedIndexChanged="ddlJobLocation_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RFV_ddlJobLocation" runat="server" ControlToValidate="ddlJobLocation"
                                                    Display="Dynamic" ErrorMessage="Job Location " InitialValue="-1" SetFocusOnError="True"
                                                    ValidationGroup="r">*</asp:RequiredFieldValidator>
                                                &nbsp;
                                            </td>
                                            <td style="text-align: left; width: 24%; height: 31px;">
                                                Complaint Type</td>
                                            <td style="text-align: center; width: 1px; height: 31px;">
                                                :</td>
                                            <td colspan="3" style="height: 31px">
                                                <asp:DropDownList ID="ddlPriority" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RFV_ddlPriority" runat="server" ControlToValidate="ddlPriority"
                                                    Display="Dynamic" ErrorMessage=" Priority " InitialValue="0" SetFocusOnError="True"
                                                    ValidationGroup="r">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td colspan="1" style="height: 31px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                Job/Booking ID</td>
                                            <td style="text-align: center; width: 1px;" align="center">
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="txtJobID" runat="server" AutoPostBack="false" OnTextChanged="txtJobID_TextChanged">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RFV_txtJobID" runat="server" ControlToValidate="txtJobID"
                                                    ErrorMessage=" Job ID" ValidationGroup="r">*</asp:RequiredFieldValidator>
                                                &nbsp;&nbsp;
                                            </td>
                                            <td style="text-align: left; width: 20%;">
                                                Contact no.</td>
                                            <td style="text-align: center; width: 1px;">
                                                :</td>
                                            <td style="width: 12%">
                                                <asp:TextBox ID="txtCustContact" runat="server" Width="107px">
                                                </asp:TextBox>
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" ValidationGroup="search"
                                                    Width="57px" OnClick="btnSearch_Click" />
                                            </td>
                                            <td style="font-size: 7pt; font-family: Tahoma" colspan="2" rowspan="8" valign="top">
                                                <asp:GridView ID="gvSearch" runat="server" Width="100%" AutoGenerateColumns="False"
                                                    BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" Caption="search results"
                                                    CellPadding="2" DataKeyNames="JobID" ForeColor="Black" GridLines="None" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged"
                                                    AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gvSearch_PageIndexChanging"
                                                    PageSize="8">
                                                    <Columns>
                                                        <asp:BoundField DataField="JobID" HeaderText="JobID" />
                                                        <asp:BoundField DataField="CabID" HeaderText="CabID" />
                                                        <asp:BoundField DataField="JobDate" HeaderText="JobDate" DataFormatString="{0:dd/MM/yy}" />
                                                        <asp:CommandField SelectText="Details" ShowSelectButton="True" />
                                                    </Columns>
                                                    <FooterStyle BackColor="Tan" />
                                                    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                                                    <SelectedRowStyle BackColor="Tan" ForeColor="GhostWhite" />
                                                    <HeaderStyle BackColor="Tan" Font-Bold="True" />
                                                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                                                </asp:GridView>
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp; &nbsp; &nbsp;
                                            </td>
                                            <td colspan="1" rowspan="8" style="font-size: 7pt; font-family: Tahoma" valign="top">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblGuestH" runat="server" Text="Customer Name"></asp:Label>
                                            </td>
                                            <td align="center" style="text-align: center; width: 1px;">
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="txtCustName" runat="server">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RFV_txtCustName" runat="server" ControlToValidate="txtCustName"
                                                    Display="Dynamic" ErrorMessage="<b>What is  Customer Name ?</b> <div>For registering this complaint you must enter customer Name.</div>"
                                                    SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td style="width: 20%; text-align: left">
                                                Job Date</td>
                                            <td style="text-align: center; width: 1px;">
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="txtJobDate" onfocus="showCalendarControl(this);" runat="server"
                                                    MaxLength="10">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RFV_txtJobDate" runat="server" ControlToValidate="txtJobDate"
                                                    ErrorMessage="Job Date " SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <asp:Label ID="Label3" runat="server" Text="Job Type"></asp:Label>
                                            </td>
                                            <td align="center" style="width: 1px; text-align: center">
                                                :</td>
                                            <td rowspan="1" valign="top">
                                                <asp:Label ID="lblJobType" runat="server" Font-Bold="True"></asp:Label>
                                            </td>
                                            <td style="width: 22%; text-align: left">
                                            </td>
                                            <td style="width: 1px; text-align: center">
                                            </td>
                                            <td style="width: 12%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblFrom" runat="server" Text="Travel From"></asp:Label>
                                                &nbsp;
                                            </td>
                                            <td align="center" style="width: 1px; text-align: center">
                                                :</td>
                                            <td rowspan="3" valign="top">
                                                <asp:TextBox ID="txtJobFrom" runat="server" TextMode="MultiLine" Rows="4">
                                                </asp:TextBox>
                                            </td>
                                            <td style="text-align: left; width: 22%;">
                                                <asp:Label ID="lblTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td style="text-align: center; width: 1px;">
                                                :</td>
                                            <td style="width: 12%">
                                                <asp:TextBox ID="txtJobTo" runat="server" TextMode="MultiLine">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                            </td>
                                            <td align="center" style="width: 1px; text-align: center">
                                            </td>
                                            <td style="width: 22%; text-align: left">
                                                <asp:Label ID="lblInvoiceH" runat="server" ForeColor="Maroon"></asp:Label>
                                            </td>
                                            <td style="width: 1px; text-align: center">
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblInvoice" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; height: 22px;">
                                            </td>
                                            <td align="center" style="height: 22px">
                                                &nbsp;
                                            </td>
                                            <td style="text-align: left; width: 20%; height: 22px;">
                                                Resolution Required At</td>
                                            <td style="text-align: center; width: 1px; height: 22px;">
                                                :</td>
                                            <td rowspan="3" valign="top">
                                                <asp:DropDownList ID="ddlResolution" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RFV_ddlResolution" runat="server" ControlToValidate="ddlResolution"
                                                    Display="Dynamic" ErrorMessage="<b>At which level Resolution is Required ?</b> <div> you must select resolution level </div>"
                                                    InitialValue="0" SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 22px; text-align: left">
                                                Car Category
                                            </td>
                                            <td align="center" style="height: 22px; text-align: left">
                                                :</td>
                                            <td style="width: 20%; height: 22px; text-align: left">
                                                <asp:Label ID="lbl_CarCat" runat="server" Font-Bold="True"></asp:Label>
                                            </td>
                                            <td style="width: 1px; height: 22px; text-align: center">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="TmsClassic_LeftH" colspan="8" style="text-align: left">
                                                Cab Details :</td>
                                            <td class="TmsClassic_LeftH" colspan="1" style="text-align: left">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8" style="text-align: left">
                                                <table width="100%">
                                                    <tr>
                                                        <td style="width: 409px" align="left">
                                                            Cab Number :<asp:DropDownList ID="ddlCabNo" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCabNo_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="lblCabID" runat="server"></asp:Label>
                                                        </td>
                                                        <td>
                                                            &nbsp;<asp:Label ID="lblChauffeurNameH" runat="server" Text="Chauffeur Name :"> </asp:Label>
                                                            <asp:TextBox ID="txtChauffName" runat="server">
                                                            </asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblContactH" runat="server" Text="Contact No."></asp:Label>
                                                            <asp:TextBox ID="txtChauffeurContact" runat="server" Width="129px">
                                                            </asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender_Chauff" runat="server" FilterType="Numbers"
                                                    TargetControlID="txtChauffeurContact">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td colspan="1" style="text-align: left">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="TmsClassic_LeftH" colspan="8">
                                                Complaint Details :</td>
                                            <td align="left" class="TmsClassic_LeftH" colspan="1">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Complaint Source</td>
                                            <td style="text-align: center; width: 1px;">
                                                :</td>
                                            <td>
                                                <asp:DropDownList ID="ddlCompSrc" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RFV_ddlCompSrc" runat="server" ControlToValidate="ddlCompSrc"
                                                    ErrorMessage="Complaint Source" InitialValue="0" SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator>
                                                &nbsp;
                                            </td>
                                            <td style="width: 20%">
                                            </td>
                                            <td style="text-align: center; width: 1px;">
                                            </td>
                                            <td colspan="3">
                                                &nbsp;
                                            </td>
                                            <td colspan="1">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Description</td>
                                            <td style="text-align: center; width: 1px;">
                                                :</td>
                                            <td colspan="6" rowspan="2">
                                                <asp:TextBox ID="txtCompDesc" runat="server" Columns="63" TextMode="MultiLine" Rows="4">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RFV_txtCompDesc" runat="server" ControlToValidate="txtCompDesc"
                                                    ErrorMessage="Complaint Detail" SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator>
                                                &nbsp;&nbsp; &nbsp;
                                            </td>
                                            <td colspan="1" rowspan="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td style="text-align: center; width: 1px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8" align="center" class="Messages">
                                                <asp:Literal ID="ltrlMsg" runat="server"></asp:Literal>
                                                <asp:Literal ID="ltrlErr" runat="server"></asp:Literal>
                                            </td>
                                            <td align="center" class="Messages" colspan="1">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%">
                                                Cust. contact details</td>
                                            <td rowspan="2" style="text-align: center; width: 1px;">
                                                :</td>
                                            <td colspan="6" rowspan="2">
                                                <asp:TextBox ID="txtCPDetails" runat="server" Columns="53" TextMode="MultiLine">
                                                </asp:TextBox>
                                                <input type="button" id="btnhide" class="button" style="display: none;" disabled="disabled"
                                                    value="Save" />
                                                <asp:Button ID="btnRegister" runat="server" Text="Register" Width="110px" ValidationGroup="r"
                                                    OnClientClick="return CheckDetails(this);" OnClick="btnRegister_Click" />
                                                <asp:Button ID="btnNew" runat="server" OnClick="btnNew_Click" Text="Register New"
                                                    Width="106px" />
                                            </td>
                                            <td colspan="1" rowspan="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="8" class="TmsClassic_LeftH">
                                                Concerned Person For Resolutions :</td>
                                            <td align="left" class="TmsClassic_LeftH" colspan="1">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Resolution</td>
                                            <td style="text-align: center; width: 1px;">
                                                :</td>
                                            <td colspan="6" rowspan="2">
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Columns="63" ValidationGroup="update"
                                                    Rows="4">
                                                </asp:TextBox>
                                                &nbsp;
                                                <asp:Button ID="btnUpdate" runat="server" Text="Update" Width="80px" OnClick="btnUpdate_Click" />
                                                &nbsp;
                                                <asp:Button ID="btnClose" OnClientClick="showConfirm(this); return false;" runat="server"
                                                    Text="Closure" OnClick="btnClose_Click" Width="80px" />
                                                <asp:Button ID="btnReOpen" runat="server" OnClick="btnReOpen_Click" Text="Re-Open"
                                                    Visible="False" Width="80px" />
                                            </td>
                                            <td colspan="1" rowspan="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td style="text-align: center; width: 1px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8" style="color: #0066cc" align="center">
                                            </td>
                                            <td align="center" colspan="1" style="color: #0066cc">
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <td colspan="8">
                                                <asp:GridView ID="gvResolutions" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow"
                                                    BorderColor="Tan" BorderWidth="1px" CellPadding="2" DataSourceID="DataSource_For_gvResolutions"
                                                    EmptyDataText="No Resolutions For This Complaint" ForeColor="Black" GridLines="None"
                                                    Width="100%">
                                                    <Columns>
                                                        <asp:BoundField DataField="ResolutionBy" HeaderText="Name" />
                                                        <asp:BoundField DataField="Reg_DateTime" HeaderText="Date" />
                                                        <asp:TemplateField HeaderText="Resolution">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtresolution" runat="server" Text='<%# Bind("Resolution") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="TextBox2" runat="server" Columns="80" ReadOnly="True" Text='<%# Bind("Resolution") %>'
                                                                    TextMode="MultiLine"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle BackColor="Tan" />
                                                    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                                                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                                                    <HeaderStyle BackColor="Tan" Font-Bold="True" />
                                                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                                                </asp:GridView>
                                                
                                            </td>
                                            <td colspan="1">
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td colspan="8">
                                                <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BehaviorID="mdlPopup"
                                                    TargetControlID="div" PopupControlID="div" OkControlID="btnOk" OnOkScript="okClick();"
                                                    CancelControlID="btnNo" OnCancelScript="cancelClick();" BackgroundCssClass="modalBackground">
                                                </cc1:ModalPopupExtender>
                                                <div style="display: none" id="div" class="confirm" align="center" runat="server">
                                                    <img src="../App_Images/logo_carzonret.gif" align="Middle" alt="" />
                                                    <br />
                                                    <br />
                                                    <b style="font-family: Tahoma">Are you sure you want to close this complaint ?</b>
                                                    <br />
                                                    <br />
                                                    <asp:Button ID="btnOk" runat="server" Width="50px" Text="Yes"></asp:Button>
                                                    <asp:Button ID="btnNo" runat="server" Width="50px" Text="No"></asp:Button>
                                                </div>
                                            </td>
                                            <td colspan="1">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td style="text-align: left">
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td style="text-align: left">
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
