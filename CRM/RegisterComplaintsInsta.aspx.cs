using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class RegisterComplaintsInsta : System.Web.UI.Page
{
    public LoadAllCabs objLAC = new LoadAllCabs();
    public CRM objCRM = new CRM();
    public MonthYear objMY = new MonthYear();
  
    protected void Page_Load(object sender, EventArgs e)
    {
       // objCRM.CreateComplaintSource
        //if (Request.QueryString["id"] == null || Request.QueryString["BkID"] == null)
        //{
        //    Response.Redirect("http://insta.carzonrent.com");
        //}

        Session["UserID"] = Request.QueryString["id"].ToString();
        //Response.Write("testing: " + Session["UserID"].ToString());
        object UserName = Insta.CRM_Insta.GetUserName(Convert.ToInt32(Session["UserID"]));
        Session["User"] = UserName.ToString();
        //Response.Write("testing: " + UserName.ToString());
        //Response.End();

        if (!IsPostBack)
        {
            if (Session["UserID"] != null && Convert.ToInt32(Session["UserID"]) > 0)
            {
            }
            else
            {
                Response.Redirect("http://insta.carzonrent.com");
            }
            Session["BranchID"] = "1";
            Session["SubBranchID"] = "1";

            ddlLocation.DataBind();
            Bind_ddlCabNo(int.Parse(Session["BranchID"].ToString()), int.Parse(Session["SubBranchID"].ToString()), 1);
            Bind_ddlPriority();
            Bind_ddlJobLocation();
            Bind_ddlCompSrc();
            Bind_ddlResolution();
            EnableDisable_JobID();
            EnableDisable_btnClose();
            EnableDisable_btnSearch();
            VisibleDisable_btnReOpen();
            btnNew.Visible = false;

            fillDetails();
            FillGridView_ForDetails(Convert.ToInt32(Request.QueryString["BkID"]));
        }
        Page.Title = "Insta : CRM";
    }


    #region BindingOfDatacontrols
    protected void Bind_ddlCabNo(int BranchID, int SubBranchID, int IType)
    {
        DataSet dstCabNo;
        dstCabNo = objLAC.LoadActiveAndReturnedCabs(BranchID, SubBranchID, IType);
        if (dstCabNo != null)
        {
            if (dstCabNo.Tables.Count > 0)
            {
                if (dstCabNo.Tables[0].Rows.Count > 0)
                {
                    ddlCabNo.DataSource = dstCabNo;
                    ddlCabNo.DataTextField = "CabNo";
                    ddlCabNo.DataValueField = "CabID";
                    ddlCabNo.DataBind();
                    //ddlCabNo.ClearSelection();
                    ddlCabNo.Items.Insert(1, new ListItem("NA", "-1"));
                }
                else
                {
                    ddlCabNo.DataSource = null;
                    ddlCabNo.DataBind();
                }
            }
        }
    }
    protected void Bind_ddlResolution()
    {
        PopulateDropDownBox(7, ddlResolution);
    }
    protected void Get_ChauffeurInfo(int CabID)
    {
        int status = 0;
        SqlDataReader myreader;

        txtChauffName.Text = "";
        txtChauffeurContact.Text = "";

        myreader = objCRM.ListChauffeurDetails(CabID, out status);
        if (status != -2)
        {
            if (myreader.Read())
            {
                txtChauffName.Text = myreader["CabDriverName"].ToString();
                txtChauffeurContact.Text = myreader["CabDriverContact"].ToString();
            }
        }

    }
    protected void Bind_ddlPriority()
    {
        PopulateDropDownBox(2, ddlPriority);
        //ddlPriority.Items.Insert(0, new ListItem("<--select-->", "0"));
    }
    protected void Bind_ddlJobLocation()
    {
        PopulateDropDownBox_JobLocations(Session["BranchID"] != null ? int.Parse(Session["BranchID"].ToString()) : 0);
        //ddlPriority.Items.Insert(0, new ListItem("<--select-->", "0"));
    }
    protected void PopulateDropDownBox_JobLocations(int BranchID)
    {
        DataSet dstDdlJL;
        dstDdlJL = objCRM.PopulateDropDownBox_JobLocations(BranchID);
        //dstDdlJL = Insta.CRM_Insta.PopulateDropDownBox_JobLocations(Convert.ToInt32(BranchID));
        if (dstDdlJL.Tables.Count >= 0)
        {
            if (dstDdlJL.Tables[0].Rows.Count > 0)
            {
                ddlJobLocation.DataSource = dstDdlJL;
                ddlJobLocation.DataTextField = "SubBranchName";
                ddlJobLocation.DataValueField = "SubBranchID";
                ddlJobLocation.DataBind();
                ddlJobLocation.ClearSelection();
                ddlJobLocation.SelectedIndex = 1;
            }
            else
            {
                ddlJobLocation.DataSource = null;
                ddlJobLocation.DataBind();
            }
        }
        else
        {
            ddlJobLocation.DataSource = null;
            ddlJobLocation.DataBind();
        }
    }
    protected void Bind_ddlCompSrc()
    {
        PopulateDropDownBox(1, ddlCompSrc);
    }
    protected void PopulateDropDownBox(int TableID, DropDownList ddl)
    {
        DataSet dstDDL = null;
        dstDDL = objCRM.PopulateDropDownBox(TableID);
        //dstDDL = Insta.CRM_Insta.PopulateDropDownBox(TableID);
        if (dstDDL.Tables.Count > 0)
        {
            if (dstDDL.Tables[0].Rows.Count > 0)
            {
                ddl.DataSource = dstDDL;
                ddl.DataTextField = "Description";
                ddl.DataValueField = "EntityValue";
                ddl.DataBind();
            }
            else
            {
                ddl.DataSource = null;
                ddl.DataBind();
            }
        }
        else
        {
            ddl.DataSource = null;
            ddl.DataBind();
        }
    }
    #endregion

    #region SearchingAndDisplay
    protected void ddlCabNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Get_ChauffeurInfo(int.Parse(ddlCabNo.SelectedValue.ToString()));
    }
    protected void ddlJobLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        int BranchID;
        if (int.Parse(ddlLocation.SelectedValue) == 0)
            BranchID = int.Parse(Session["BranchID"].ToString());
        else
            BranchID = int.Parse(ddlLocation.SelectedValue);

        EnableDisable_JobID();
        EnableDisable_btnSearch();
        Emty_gvSearch();
        if (txtJobID.Text.Trim() != "")
            SearchJobDetails(int.Parse(ddlJobLocation.SelectedValue.ToString()), txtJobID.Text.Trim() != "" ? txtJobID.Text.Trim() : "0", BranchID);

    }
    protected void txtJobID_TextChanged(object sender, EventArgs e)
    {
        int BranchID;
        if (int.Parse(ddlLocation.SelectedValue) == 0)
            BranchID = int.Parse(Session["BranchID"].ToString());
        else
            BranchID = int.Parse(ddlLocation.SelectedValue);

        if (txtJobID.Text.Trim() != "")
            SearchJobDetails(int.Parse(ddlJobLocation.SelectedValue.ToString()), txtJobID.Text.Trim() != "" ? txtJobID.Text.Trim() : "0", BranchID);
    }

    protected void fillDetails()
    {
        txtJobID.Text = Request.QueryString["BkID"].ToString(); //Session["BookingID"].ToString();
        int BranchID;
        if (int.Parse(ddlLocation.SelectedValue) == 0)
            BranchID = int.Parse(Session["BranchID"].ToString());
        else
            BranchID = int.Parse(ddlLocation.SelectedValue);

        if (txtJobID.Text.Trim() != "")
            SearchJobDetails(int.Parse(ddlJobLocation.SelectedValue.ToString()), txtJobID.Text.Trim() != "" ? txtJobID.Text.Trim() : "0", BranchID);
    }

    protected void EnableDisable_JobID()
    {
        if (ddlJobLocation.SelectedValue.ToString() != "-1")
        {
            txtJobID.Enabled = true;
            btnRegister.Enabled = true;
        }
        else
        {
            txtJobID.Enabled = false;
            btnRegister.Enabled = false;
        }
    }
    protected void EnableDisable_btnClose()
    {
        btnClose.Enabled = true;
    }
    protected void VisibleDisable_btnReOpen()
    {
        btnReOpen.Visible = true;
    }
    protected void EnableDisable_btnUpdate()
    {
        btnUpdate.Enabled = false;
    }
    protected void EnableDisable_btnSearch()
    {
        if (ddlJobLocation.SelectedValue.ToString() != "-1")
        {
            btnSearch.Enabled = true;
        }
        else
        {
            btnSearch.Enabled = false;
        }
    }

    protected void SearchJobDetails(int JobLocationID, string JobID, int BranchID)
    {
        SqlDataReader sdrSRCH;
        sdrSRCH = objCRM.SearchJobDetails(JobLocationID, JobID, BranchID);
        //sdrSRCH = Insta.CRM_Insta.SearchJobDetails(JobLocationID, JobID, BranchID);

        txtJobDate.Text = "";
        txtCustContact.Text = "";
        txtCustName.Text = "";
        txtJobFrom.Text = "";
        txtJobTo.Text = "";
        ddlCabNo.SelectedIndex = 0;
        lblCabID.Text = "";
        lblInvoice.Text = "";
        if (sdrSRCH.Read())
        {

            if (BranchID < 26)
            {
                ddlCabNo.Visible = true;
                lblCabID.Visible = false;
                ddlCabNo.ClearSelection();
                if (sdrSRCH["CabID"].ToString() != "0")
                {
                    if (ddlCabNo.Items.FindByValue(sdrSRCH["CabID"].ToString()) != null)
                    {
                        ddlCabNo.Items.FindByValue(sdrSRCH["CabID"].ToString()).Selected = true;
                    }
                }
            }
            else
            {
                ddlCabNo.Visible = false;
                ddlCabNo.Visible = false;
                lblCabID.Visible = true;
                lblCabID.Text = sdrSRCH["CabID"].ToString();
                lbl_CarCat.Text = sdrSRCH["CarCategory"].ToString();
                txtChauffName.Text = sdrSRCH["chauffeurName"].ToString();
            }

            if (BranchID < 26)
            {
                txtChauffName.Visible = true;
                txtChauffeurContact.Visible = true;
                lblInvoice.Visible = false;
                Get_ChauffeurInfo(int.Parse(ddlCabNo.SelectedValue.ToString()));
            }
            else
            {
                txtChauffName.Visible = true; ;
                txtChauffeurContact.Visible = false;
                ddlCabNo.Visible = false;
                lblInvoice.Visible = true;
                // Insta _Mig //  lblInvoice.Text = sdrSRCH["InvoiceAmount"].ToString();   
            }

            txtJobDate.Text = sdrSRCH["JobDate"].ToString();
            txtCustContact.Text = sdrSRCH["CallerId"].ToString();
            txtCustName.Text = sdrSRCH["CallerName"].ToString();
            txtJobFrom.Text = sdrSRCH["PickupAddress"].ToString();
            txtJobTo.Text = sdrSRCH["DestinationAddress"].ToString();
            lblJobType.Text = sdrSRCH["JobType"].ToString();
        }

    }
    #endregion

    #region ManageGridViewDetails
    protected void FillGridView_ForDetails(int JobID)
    {
        SqlDataReader sdrSRCH;
        sdrSRCH = Insta.CRM_Insta.SearchComplaint_Job(JobID);
        ResetAll();
        if (sdrSRCH.Read())
        {
            ddlJobLocation.DataBind();
            ddlJobLocation.ClearSelection();
            ddlJobLocation.Items.FindByValue(sdrSRCH["JobLocationID"].ToString()).Selected = true;

            ddlPriority.DataBind();
            ddlPriority.ClearSelection();
            ddlPriority.Items.FindByValue(sdrSRCH["PriorityID"].ToString()).Selected = true;

            txtJobID.Text = sdrSRCH["JobID"].ToString();
            txtJobDate.Text = sdrSRCH["JobDate"].ToString();
            txtCustName.Text = sdrSRCH["CustomerName"].ToString();
            txtCustContact.Text = sdrSRCH["CustomerContact"].ToString();
            txtJobFrom.Text = sdrSRCH["Job_From"].ToString();
            txtJobTo.Text = sdrSRCH["Job_To"].ToString();
            lbl_CarCat.Text = sdrSRCH["CarCategory"].ToString();

            if (int.Parse(sdrSRCH["BranchID"].ToString()) < 26)
            {
                ddlCabNo.Visible = true;
                ddlCabNo.DataBind();
                ddlCabNo.ClearSelection();
                if (sdrSRCH["CabID"].ToString() != "0")
                    ddlCabNo.Items.FindByValue(sdrSRCH["CabID"].ToString()).Selected = true;
                lblCabID.Visible = false;
            }
            else
            {
                ddlCabNo.Visible = false;
                lblCabID.Visible = true;
                lblCabID.Text = sdrSRCH["CabID"].ToString();
            }

            try
            {
                ddlResolution.DataBind();
                ddlResolution.ClearSelection();
                ddlResolution.Items.FindByValue(sdrSRCH["ResolutionAt"].ToString()).Selected = true; // sometime gets Error
            }
            catch (Exception)
            {
                ddlResolution.DataBind();
                ddlResolution.ClearSelection();
                ddlResolution.Items.FindByValue("0").Selected = true;
            }

            txtChauffName.Text = sdrSRCH["ChauffeurName"].ToString();
            txtChauffeurContact.Text = sdrSRCH["ChauffeurContact"].ToString();

            ddlCompSrc.DataBind();
            ddlCompSrc.ClearSelection();
            ddlCompSrc.Items.FindByValue(sdrSRCH["ComplaintSourceID"].ToString()).Selected = true;

            txtCompDesc.Text = sdrSRCH["ComplaintDescription"].ToString();
            txtCPDetails.Text = sdrSRCH["ContactDetails"].ToString() == "" ? "NA" : sdrSRCH["ContactDetails"].ToString(); ;
            lblInvoice.Text = sdrSRCH["InvoiceAmount"].ToString();

            ReadOnly(1);
            btnRegister.Visible = false;
            btnNew.Visible = true;
            lblEscInfo.Text = sdrSRCH["Level"].ToString();
            lblJobType.Text = sdrSRCH["JobType"].ToString();
        }

    }

    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {


        int BranchID;
        if (int.Parse(ddlLocation.SelectedValue) == 0)
            BranchID = int.Parse(Session["BranchID"].ToString());
        else
            BranchID = int.Parse(ddlLocation.SelectedValue);

        SearchJobDetails(int.Parse(ddlJobLocation.SelectedValue.ToString()), gvSearch.SelectedDataKey[0].ToString(), BranchID);
        txtJobID.Text = gvSearch.SelectedDataKey[0].ToString();

    }
    protected void ReadOnly(int Mode)
    {
        if (Mode == 1)
        {
            ddlJobLocation.Enabled = false;
            ddlPriority.Enabled = false;
            txtJobID.ReadOnly = true;
            txtJobDate.ReadOnly = true;
            txtCustName.ReadOnly = true;
            txtCustContact.ReadOnly = true;
            txtJobFrom.ReadOnly = true;
            txtJobTo.ReadOnly = true;
            ddlCabNo.Enabled = false;
            ddlResolution.Enabled = false;
            txtChauffName.ReadOnly = true;
            txtChauffeurContact.ReadOnly = true;
            ddlCompSrc.Enabled = false;
            txtCompDesc.ReadOnly = true;
            txtCPDetails.ReadOnly = true;
        }
        if (Mode == 2)
        {
            ddlJobLocation.Enabled = true;
            ddlPriority.Enabled = true;
            txtJobID.ReadOnly = false;
            txtJobDate.ReadOnly = false;
            txtCustName.ReadOnly = false;
            txtCustContact.ReadOnly = false;
            txtJobFrom.ReadOnly = false;
            txtJobTo.ReadOnly = false;
            ddlCabNo.Enabled = true;
            ddlResolution.Enabled = true;
            txtChauffName.ReadOnly = false;
            txtChauffeurContact.ReadOnly = false;
            ddlCompSrc.Enabled = true;
            txtCompDesc.ReadOnly = false;
            txtCPDetails.ReadOnly = false;
        }
    }
    #endregion


    #region Events
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;
        else
        {
            int BranchID;
            if (int.Parse(ddlLocation.SelectedValue) == 0)
                BranchID = int.Parse(Session["BranchID"].ToString());
            else
                BranchID = int.Parse(ddlLocation.SelectedValue);

            string ComplaintDateTime;
            //ComplaintDateTime=txtCompDate.Text + " " + ddlHour.SelectedValue.ToString() + ":" + ddlMinute.SelectedValue.ToString() + ":00";
            ComplaintDateTime = System.DateTime.Now.ToString();
            //New Line as on 18 sept 09 by NCJ
            RegisterComplaint(BranchID, Convert.ToDateTime(ComplaintDateTime), Session["User"].ToString(), System.DateTime.Now);

            ResetAll();
            VisibleDisable_btnReOpen();
        }
    }
    protected void RegisterComplaint(int BranchID, DateTime ComplaintDateTime, string RegisteredBy, DateTime Reg_DateTime)
    {
        ltrlMsg.Text = "";
        ltrlErr.Text = "";
        string ChauffeurName = "";
        string ChauffeurContact = "";
        // Insta  //int CabID;
        string CabID = "";
        if (BranchID < 26)
        {
            ChauffeurName = txtChauffName.Text.Trim();
            ChauffeurContact = txtChauffeurContact.Text.Trim();
            // insta  //  CabID = int.Parse(ddlCabNo.SelectedValue.ToString());
        }
        else
        {
            // ChauffeurName = "NA";
            ChauffeurName = txtChauffName.Text.Trim(); // by nalin 
            //ChauffeurContact = "0"; //commented and added on 07_Oct-2016 by rahul
            ChauffeurContact = txtChauffeurContact.Text.Trim();
            CabID = lblCabID.Text.Trim() != "" ? lblCabID.Text.Trim() : "0";
        }

        String Message = Insta.CRM_Insta.RegisterComplaint
            (BranchID, int.Parse(ddlJobLocation.SelectedValue.ToString()), txtJobID.Text.Trim()
            , Convert.ToDateTime(txtJobDate.Text.Trim()), txtJobFrom.Text.Trim(), txtJobTo.Text.Trim()
            , txtCustName.Text.Trim(), txtCustContact.Text.Trim() != "" ? txtCustContact.Text.Trim() : "0"
            , txtCPDetails.Text.Trim(), txtRemarks.Text.Trim()
            , CabID, ChauffeurName, ChauffeurContact, int.Parse(ddlCompSrc.SelectedValue.ToString())
            , ComplaintDateTime, txtCompDesc.Text.Trim(), RegisteredBy, Reg_DateTime
            , int.Parse(ddlResolution.SelectedValue.ToString()), int.Parse(ddlPriority.SelectedValue.ToString())
            , 8, lbl_CarCat.Text.Trim(), 0, "", CabID);

        ltrlMsg.Text = Message;

    }
    protected void ResetAll()
    {
        ddlJobLocation.SelectedIndex = 1;
        ddlPriority.SelectedIndex = 0;
        txtJobID.Text = "";
        txtJobDate.Text = "";
        txtCustName.Text = "";
        txtCustContact.Text = "";
        txtJobFrom.Text = "";
        txtJobTo.Text = "";
        lbl_CarCat.Text = "";
        ddlCabNo.SelectedIndex = 0;
        ddlResolution.SelectedIndex = 0;
        txtChauffName.Text = "";
        txtChauffeurContact.Text = "";
        ddlCompSrc.SelectedIndex = 0;
        txtCompDesc.Text = "";
        txtCPDetails.Text = "";
        txtRemarks.Text = "";
        EnableDisable_JobID();
        EnableDisable_btnClose();
        EnableDisable_btnUpdate();
        EnableDisable_btnSearch();
        Emty_gvSearch();
        lblCabID.Text = "";
        lblInvoice.Text = "";
        lblEscInfo.Text = "";
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        ltrlErr.Text = "";
        ltrlMsg.Text = "";
        if (txtRemarks.Text.Trim() == "")
        {
            ltrlErr.Text = "Give Resolution";
        }
    }
    protected void DoResolution(int ComplaintID, string Resolution, string ResolutionBy, int ComplaintStatus)
    {
        ltrlMsg.Text = "";
        ltrlErr.Text = "";
        ltrlMsg.Text = objCRM.DoResolution(ComplaintID, Resolution, ResolutionBy, ComplaintStatus);
        //ltrlMsg.Text = Insta.CRM_Insta.DoResolution(ComplaintID, Resolution, ResolutionBy, ComplaintStatus);
    }
    protected void btnNew_Click(object sender, EventArgs e)
    {
        ltrlMsg.Text = "";
        ltrlErr.Text = "";
        btnNew.Visible = false;
        btnRegister.Visible = true;
        ReadOnly(2);
        ResetAll();
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        ltrlMsg.Text = "";
        ltrlErr.Text = "";
        if (txtRemarks.Text.Trim() == "")
        {
            ltrlErr.Text = "Resolution Before Closing this complaint";
        }
        ResetAll();
    }

    protected void BindGridView_gvSearch()
    {

        int BranchID;
        if (int.Parse(ddlLocation.SelectedValue) == 0)
            BranchID = int.Parse(Session["BranchID"].ToString());
        else
            BranchID = int.Parse(ddlLocation.SelectedValue);

        DataSet dst = new DataSet();
        dst = objCRM.SearchContactDetails(int.Parse(ddlJobLocation.SelectedValue.ToString()), txtCustContact.Text.Trim(), BranchID);
        //dst = Insta.CRM_Insta.SearchContactDetails(int.Parse(ddlJobLocation.SelectedValue.ToString()), txtCustContact.Text.Trim(), BranchID);
        if (dst.Tables.Count > 0)
        {
            if (dst.Tables[0].Rows.Count > 0)
            {
                gvSearch.DataSource = dst.Tables[0];
                gvSearch.DataBind();
            }
            else
            {
                gvSearch.DataSource = null;
                gvSearch.DataBind();
            }
        }
        else
        {
            gvSearch.DataSource = null;
            gvSearch.DataBind();
        }
    }
    protected void Emty_gvSearch()
    {
        gvSearch.DataSource = null;
        gvSearch.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (txtCustContact.Text.Trim() != "")
        {
            if (ddlJobLocation.SelectedIndex != 0)
            {
                BindGridView_gvSearch();
            }
        }
    }

    protected void btnReOpen_Click(object sender, EventArgs e)
    {
        ltrlMsg.Text = "";
        ltrlErr.Text = "";
        if (txtRemarks.Text.Trim() == "")
        {
            ltrlErr.Text = "Give Reason For Openining This Complaint";
        }
        ResetAll();
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSearch.PageIndex = e.NewPageIndex;
        BindGridView_gvSearch();
    }

    #endregion Events

    #region NewCodeBlock

    //private void ClearFields()
    //{
    //    ddlJobLocation.SelectedIndex = 1;
    //    ddlPriority.SelectedIndex = 0;
    //    txtJobID.Text = "";
    //    txtJobDate.Text = "";
    //    txtCustName.Text = "";
    //    txtCustContact.Text = "";
    //    txtJobFrom.Text = "";
    //    txtJobTo.Text = "";
    //    ddlCabNo.SelectedIndex = 0;
    //    ddlResolution.SelectedIndex = 0;
    //    txtChauffName.Text = "";
    //    txtChauffeurContact.Text = "";
    //    ddlCompSrc.SelectedIndex = 0;
    //    txtCompDesc.Text = "";
    //    txtCPDetails.Text = "";
    //    txtRemarks.Text = "";

    //    ltrlMsg.Text = "";
    //    ltrlErr.Text = "";

    //    lblCabID.Text = "";
    //    lblInvoice.Text = "";
    //    lblEscInfo.Text = "";
    //    lbl_CarCat.Text = "";
    //}
    //protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    ClearFields();

    //    txtChauffName.Visible = true;
    //    txtChauffeurContact.Visible = false;
    //    ddlCabNo.Visible = false;

    //    lblFrom.Text = "Pickup From";
    //    lblTo.Text = "Company Name";

    //    lblInvoiceH.Visible = true;
    //    lblInvoice.Visible = true;

    //    lblInvoiceH.Text = "Invoice";

    //    lblChauffeurNameH.Visible = true;
    //    lblContactH.Visible = false;
    //    lblCabID.Visible = true;
    //    lblGuestH.Text = "Guest Name";

    //}

    #endregion NewCodeBlock
}