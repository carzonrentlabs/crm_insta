using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CRM_UpdateEscalation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       if (this.Page.IsPostBack == false)
       {
            ddlDivision.DataBind();
            ddlBranch.DataBind();
            OpenLevelForm();
        }
    }

    private void OpenLevelForm()
    {
        GetLevelID();
        if (ddlDivision.SelectedIndex > 0 && ddlBranch.SelectedIndex > 0)
        {
            divLevel.Visible = true;
        }
        else
        {
            divLevel.Visible = false;
        }
    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {

    }
    protected void ddlDivision_SelectedIndexChanged(object sender, EventArgs e)
    {
       
        OpenLevelForm();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        OpenLevelForm();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ltrlMsg.Text = "";
        if (CheckTimeSpan() == true)
        {
            SaveMatrix();
            GetLevelID();
            gvEscMatrix.DataBind();
        }
    }
    protected void GetLevelID()
    {
        lblLabel.Text= Convert.ToString( CRM.GetLevelID<int>(int.Parse(ddlBranch.SelectedValue)));
    }
    protected void SaveMatrix()
    {
        try
        {
            foreach (GridViewRow gvr in gvMatrix.Rows)
            {
                Label lblCatID = (Label)gvr.FindControl("lblCategoryID");
                TextBox txtEmailID = (TextBox)gvr.FindControl("txtEmailID");
                TextBox txtMobile = (TextBox)gvr.FindControl("txtMobile");
                TextBox txtTimeSpan = (TextBox)gvr.FindControl("txtTimeSpan");
                CRM.Save_EscalationMatrix<int, string, decimal, double>
                    (
                      int.Parse(ddlDivision.SelectedValue)
                    , int.Parse(ddlBranch.SelectedValue)
                    , int.Parse(lblLabel.Text)
                    , int.Parse(lblCatID.Text)
                    , txtEmailID.Text.Trim() != "" ? txtEmailID.Text.Trim() : "tms.easycabs.com"
                    , txtMobile.Text.Trim() != "" ? txtMobile.Text.Trim() : "0"
                    , double.Parse(txtTimeSpan.Text.Trim() != "" ? txtTimeSpan.Text.Trim() : "0")
                    );
            }
        }
        catch (Exception err)
        {
            ltrlMsg.Text = err.Message;
        }
    }
    protected bool CheckTimeSpan()
    {
        bool ans = false;
        string status = "0";
        if (int.Parse(lblLabel.Text) == 1)
        {
            ans = true;
        }
        else
        {
            foreach (GridViewRow gvr in gvMatrix.Rows)
            {
                Label lblCatID = (Label)gvr.FindControl("lblCategoryID");
                TextBox txtTimeSpan = (TextBox)gvr.FindControl("txtTimeSpan");
                status= CRM.CheckEscalationTime<int, double>
                (int.Parse(ddlBranch.SelectedValue)
                , int.Parse(lblLabel.Text)
                , int.Parse(lblCatID.Text)
                , double.Parse(txtTimeSpan.Text.Trim() != "" ? txtTimeSpan.Text.Trim() : "0")
                );
                if (status == "1")
                {
                    ltrlMsg.Text = "For Category No: " + lblCatID.Text + "Entered TimeSpan is Less than TimeSpan of Previous Level";

                    break;
                }

            }
        }
        if (status == "0")
            ans = true;
        else
            ans = false;

        return ans;
    }
    protected void gvEscMatrix_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void ODS_For_gvEscMatrix_Updating(object sender, ObjectDataSourceMethodEventArgs e)
    {
       
        //e.InputParameters.Add("BranchID", ddlBranch.SelectedValue);
        //e.InputParameters.Add("LevelID", lblLabel.Text);
    }
}
