using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class CRDMasterpage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] != null && Convert.ToInt32(Session["UserID"]) > 0)
        {
            PopulateMenu();
        }
        else
        {
            Response.Redirect("http://insta.carzonrent.com");
        } 
    }

   private  DataSet MenuItems()
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CRDMenu"].ToString());
        SqlDataAdapter MainMenu = new SqlDataAdapter("select corintModuleMaster.moduleID ,corintModuleMaster.ModuleName from corintModuleMaster INNER JOIN   CorintModuleFunctionMaster on corintModuleMaster.moduleID=CorintModuleFunctionMaster.ModuleID INNER JOIN CorintSysUsersModAccessMaster On CorintModuleFunctionMaster.FunctionID = CorintSysUsersModAccessMaster.ModFuncID where CORIntModuleFunctionMaster.active = 1 and corintModuleMaster.ModuleID=84 and CorintSysUsersModAccessMaster.SysUserID = " + Convert.ToInt32(Session["UserID"]) + " group by corintModuleMaster.ModuleID ,ModuleName", con);
        SqlDataAdapter SubMenu = new SqlDataAdapter("select CorintModuleFunctionMaster.moduleID,CorintModuleFunctionMaster.FunctionName,CorintModuleFunctionMaster.Link from corintModuleMaster INNER JOIN   CorintModuleFunctionMaster on corintModuleMaster.moduleID=CorintModuleFunctionMaster.ModuleID INNER JOIN CorintSysUsersModAccessMaster On CorintModuleFunctionMaster.FunctionID = CorintSysUsersModAccessMaster.ModFuncID where CORIntModuleFunctionMaster.active = 1 and corintModuleMaster.ModuleID=84 and CorintSysUsersModAccessMaster.SysUserID =" + Convert.ToInt32(Session["UserID"]), con);

   
        DataSet ds = new DataSet();
        MainMenu.Fill(ds, "corintModuleMaster");
        SubMenu.Fill(ds, "CorintModuleFunctionMaster");
        ds.Relations.Add("SubMenu", ds.Tables["corintModuleMaster"].Columns["ModuleID"], ds.Tables["CorintModuleFunctionMaster"].Columns["ModuleID"]);
        return ds;
    }

    private void PopulateMenu()
    {
        DataSet ds = MenuItems();

        if (ds.Tables["CorintModuleFunctionMaster"].Rows.Count > 0)
        {

            Menu menu = new Menu();
            menu.Orientation = Orientation.Horizontal;
            menu.StaticTopSeparatorImageUrl = "~/App_Images/separator.gif";
            menu.EnableTheming = true;
            menu.SkipLinkText = " ";
            menu.Width = 543;
            menu.CssClass = "topnav";
            menu.StaticMenuStyle.CssClass = "orngCopy";
            menu.StaticMenuItemStyle.CssClass = "orngCopy";
            foreach (DataRow MainMenu in ds.Tables["corintModuleMaster"].Rows)
            {
                //MenuItem CategoryItem = new MenuItem((string)MainMenu["ModuleName"]);
                //menu.Items.Add(CategoryItem);

                foreach (DataRow subMenu in MainMenu.GetChildRows("SubMenu"))
                {
                    MenuItem SubCategoryItem = new MenuItem((string)subMenu["FunctionName"].ToString());
                    SubCategoryItem.NavigateUrl = subMenu["Link"].ToString();
                    menu.Items.Add(SubCategoryItem);
                    //CategoryItem.ChildItems.Add(SubCategoryItem );
                }
            }

            ContentPlaceHolder1.Controls.Add(menu);
            //Panel1.Controls.Add(menu);
            //Panel1.DataBind();

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('sorry! you are not authorized to CRM  access.');location.href='http://insta.carzonrent.com'", true);
            
        }
    }

}
