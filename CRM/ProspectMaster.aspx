<%@ Page Language="C#" MasterPageFile="~/CRDMasterpage.master" AutoEventWireup="true"
    CodeFile="ProspectMaster.aspx.cs" Inherits="ProspectMaster" Title="Prospect Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder_Page" runat="Server">

    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="../JQuery/moment.js" type="text/javascript"></script>

    <script src="../JQuery/ui.core.js" type="text/javascript"></script>

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script src="../ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>

    <table cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="center" style="width: 1198px">
                <table cellpadding="0" cellspacing="0" border="1" style="width: 75%">
                    <tr>
                        <td colspan="4" align="center" style="background-color: #ccffff; height: 30px;">
                            <b>Prospect Master</b>&nbsp;
                            <asp:Label ID="lblloginBy" Text="" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                            <b>Customer Name</b>
                        </td>
                        <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                            <asp:TextBox ID="txtCustomerName" runat="server"></asp:TextBox>&nbsp;&nbsp;
                            <asp:RequiredFieldValidator ID="rvDateOfMeeting" SetFocusOnError="True" ValidationGroup="FS"
                                runat="server" ErrorMessage="Please Enter Prospect Name" ControlToValidate="txtCustomerName"
                                Display="None"></asp:RequiredFieldValidator>
                        </td>
                        <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                            <b>City Name</b>
                        </td>
                        <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                            <asp:DropDownList ID="ddlcityName" runat="server" ValidationGroup="FS">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="ddlclientvalidation" InitialValue="0" SetFocusOnError="True"
                                ValidationGroup="FS" runat="server" ErrorMessage="Please Select City Name" ControlToValidate="ddlcityName"
                                Display="None"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                            <b>Potential Amount (PA)</b>
                        </td>
                        <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                            <asp:TextBox ID="txtPotentialAmt" runat="server"></asp:TextBox>&nbsp;&nbsp;
                            <%--<asp:RequiredFieldValidator ID="rvtxtMeetingDesc" InitialValue="0" SetFocusOnError="True"
                                ValidationGroup="FS" runat="server" ErrorMessage="Please Enter Potential Amount"
                                ControlToValidate="txtPotentialAmt" Display="None"></asp:RequiredFieldValidator>--%>
                        </td>
                        <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                            <b>Contact Person</b>
                        </td>
                        <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                            <asp:TextBox ID="txtContactperson" runat="server" Text=""></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="rvddlCustName" SetFocusOnError="True" ValidationGroup="FS"
                                runat="server" ErrorMessage="Please Select Cust Name" ControlToValidate="txtContactperson"
                                Display="None"></asp:RequiredFieldValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                            <b>Email Address</b>
                        </td>
                        <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                            <asp:TextBox ID="txtEmailID" runat="server" ValidationGroup="FS"></asp:TextBox>&nbsp;&nbsp;
                            <%--<asp:RequiredFieldValidator ID="rvtxtPersonName" SetFocusOnError="True" ValidationGroup="FS"
                                runat="server" ErrorMessage="Please Enter Emaail ID" ControlToValidate="txtEmailID"
                                Display="None"></asp:RequiredFieldValidator>--%>
                        </td>
                        <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                            <b>Phone</b>
                        </td>
                        <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                            <asp:TextBox ID="txtMobileNo" MaxLength="10" runat="server"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="rvtxtMobileNo" SetFocusOnError="True" ValidationGroup="FS"
                                runat="server" ErrorMessage="Please Enter Metting Person Mobile" ControlToValidate="txtMobileNo"
                                Display="None"></asp:RequiredFieldValidator>--%>
                            <asp:HiddenField ID="hdnIDs" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                            <asp:ValidationSummary ID="vs" runat="server" ShowMessageBox="True" ShowSummary="False"
                                ValidationGroup="FS" />
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                ValidationGroup="FS" />
                            &nbsp;&nbsp;
                            <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click"
                                ValidationGroup="FS" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                            <b>From Create Date</b> &nbsp;&nbsp;<asp:TextBox ID="FromDate" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="FromDate"
                                Format="MMM/dd/yyyy">
                            </cc1:CalendarExtender>
                            &nbsp;&nbsp;
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" SetFocusOnError="True" ValidationGroup="FSReport"
                                runat="server" ErrorMessage="Please Select From Date" ControlToValidate="FromDate"
                                Display="None"></asp:RequiredFieldValidator>
                            <b>To Create Date</b> &nbsp;&nbsp;<asp:TextBox ID="ToDate" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="ToDate"
                                Format="MMM/dd/yyyy">
                            </cc1:CalendarExtender>
                            &nbsp;&nbsp;
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" SetFocusOnError="True" ValidationGroup="FSReport"
                                runat="server" ErrorMessage="Please Select To Meeting" ControlToValidate="ToDate"
                                Display="None"></asp:RequiredFieldValidator>
                            &nbsp;&nbsp;<asp:Button ID="btnGo" runat="server" Text="View" ValidationGroup="FSReport"
                                OnClick="btnGo_Click" />
                            &nbsp;&nbsp;<asp:Button ID="bntExprot" runat="server" Text="Export To Excel" OnClick="bntExprot_Click"
                                ValidationGroup="FSReport" />
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td colspan="4">
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" style="width: 90%">
                    <tr>
                        <td align="center">
                            <asp:GridView runat="server" ID="gdprospect" EmptyDataText="No Record Found" AutoGenerateColumns="False"
                                AllowPaging="True" DataKeyNames="id" OnPageIndexChanging="gdprospect_PageIndexChanging"
                                OnRowCancelingEdit="gdprospect_RowCancelingEdit" OnRowDeleting="gdprospect_RowDeleting"
                                OnRowEditing="gdprospect_RowEditing" OnRowUpdating="gdprospect_RowUpdating">
                                <Columns>
                                    <asp:TemplateField HeaderText="Customer Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblClientconame" Text='<% #Bind("ClientCoName") %>' runat="server"></asp:Label>
                                            <asp:HiddenField ID="hdnid" runat="server" Value='<%#Bind("ID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="City Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeetingDate" Text='<% #Bind("cityName") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Potential Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeetingDesc" Text='<% #Bind("PotentialAmout") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact Person Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActionItem" Text='<% #Bind("ContactPersonName") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email Address">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActionByDate" Text='<% #Bind("EmailID") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phone">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActionManagerName" Text='<% #Bind("Phone") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Button" ShowCancelButton="true" ShowEditButton="true" />
                                    <asp:CommandField ButtonType="Button" ShowDeleteButton="true" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
