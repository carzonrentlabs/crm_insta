﻿<%@ WebHandler Language="C#" Class="LoginUser" %>

using System;
using System.Web;
using System.Data;
using System.Web.SessionState;
using Insta;

public class LoginUser : IHttpHandler, IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) 
    {
        
        context.Response.ContentType = "text/plain";
        string username = context.Request.QueryString["username"].ToString();
        string Password = context.Request.QueryString["Password"].ToString();
        OL_LoginUser objLoginUserOL = new OL_LoginUser();
        BLLoginUser objLoginUser = new BLLoginUser();
        LoginValidate loginLog = new LoginValidate();
        objLoginUserOL.LoginId = username.ToString();
        objLoginUserOL.Password = Password.ToString();

        objLoginUserOL = objLoginUser.ValidateUser(objLoginUserOL);
        
        if (objLoginUserOL.LoginStatus.Equals("valid") && Convert.ToInt32(objLoginUserOL.SysUserId) > 0)
        {
            context.Session["UserID"] = objLoginUserOL.SysUserId.ToString();            
            context.Session["fin_year"] = "2010-2011";
            context.Session["User"] = username;
            DataTable dtInsert = loginLog.InsertLoginDetail(objLoginUserOL.LoginId.ToString().Trim(), context.Request.ServerVariables["remote_addr"]);
            if (dtInsert.Rows.Count > 0)
            {
                context.Session["MaxLoginID"]=dtInsert.Rows[0]["ID"].ToString(); 
            }
            context.Response.Write(objLoginUserOL.LoginStatus.ToString());
            context.Response.End();
        }
        else
        {
            context.Response.Write("Invalid Id or Passowrd.");
            context.Response.End();
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}