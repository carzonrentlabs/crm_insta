using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ExcelUtil;

public partial class Reporting_CRM_MonthlyReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] != null && Convert.ToInt32(Session["UserID"]) > 0)
        {
            if (this.Page.IsPostBack == false)
            {
                Date_Init();
                gvReport.DataBind();
            }
            Page.Title = "Insta : CRM";
        }
        else
        {
            Response.Redirect("http://insta.carzonrent.com");
        } 
    }

    private void Date_Init()
    {
        txtFrom.Text = txtTo.Text = DateTime.Now.ToString("MM-dd-yyyy");
    }
    protected void btnShow_Click(object sender, EventArgs e)
    {
        DataSet DS = Insta.CRM_Insta.ListCrmMonthlyReoport(Convert.ToInt32(ddlBranch.SelectedValue.ToString()), Convert.ToDateTime(txtFrom.Text.Trim()), Convert.ToDateTime(txtTo.Text.Trim()), 1);
        gvReport.DataSource = DS.Tables[0];
        gvReport.DataBind();

    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        DataSet DS = Insta.CRM_Insta.ListCrmMonthlyReoport(Convert.ToInt32(ddlBranch.SelectedValue.ToString()), Convert.ToDateTime(txtFrom.Text.Trim()), Convert.ToDateTime(txtTo.Text.Trim()), 1);
        ExcelUtil.WorkbookEngine.ExportDataSetToExcel(DS,"CRM_Report.xls");
    }
}
