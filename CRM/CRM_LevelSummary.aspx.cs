using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CRM_CRM_LevelSummary : System.Web.UI.Page
{
    static public string title = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if(Request["mid"]!=null && Request["l"]!=null)
        {
            if (Request["mid"].ToString() != "" && Request["l"].ToString() != "")
            {
                ODS_For_gvLevel.SelectParameters[0].DefaultValue = Session["BranchID"].ToString();
                ODS_For_gvLevel.SelectParameters[1].DefaultValue = Request["mid"].ToString();
                ODS_For_gvLevel.SelectParameters[2].DefaultValue = Request["l"].ToString();
                gvLevel.DataBind();
            }
        }
    }
}
