using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Insta;
using Systems;
public partial class CRM_RegisterComplaintsD : System.Web.UI.Page
{
    #region GlobalVar
    public LoadAllCabs objLAC = new LoadAllCabs();
    public CRM objCRM = new CRM();
    public MonthYear objMY = new MonthYear();
    public CRM_Insta objInsta = new CRM_Insta();
    
    #endregion global

    #region BindingOfDatacontrols
    protected void Bind_ddlCabNo(int BranchID, int SubBranchID, int IType)
    {
        DataSet dstCabNo;
        dstCabNo = objLAC.LoadActiveAndReturnedCabs(BranchID, SubBranchID, IType);
        if (dstCabNo != null)
        {
            if (dstCabNo.Tables.Count > 0)
            {
                if (dstCabNo.Tables[0].Rows.Count > 0)
                {
                    ddlCabNo.DataSource = dstCabNo;
                    ddlCabNo.DataTextField = "CabNo";
                    ddlCabNo.DataValueField = "CabID";
                    ddlCabNo.DataBind();
                    //ddlCabNo.ClearSelection();
                    ddlCabNo.Items.Insert(1, new ListItem("NA", "-1"));
                }
                else
                {
                    ddlCabNo.DataSource = null;
                    ddlCabNo.DataBind();
                }
            }
        }
    }
    protected void Bind_ddlResolution()
    {
        PopulateDropDownBox(7, ddlResolution);
    }
    protected void Get_ChauffeurInfo(int CabID)
    {
        int status = 0;
        SqlDataReader myreader;

        //txtChauffName.Text = "";
        //txtChauffeurContact.Text = "";

        myreader = objCRM.ListChauffeurDetails(CabID, out status);
        if (status != -2)
        {
            if (myreader.Read())
            {
                if (string.IsNullOrEmpty(txtChauffName.Text))
                {
                    txtChauffName.Text = myreader["CabDriverName"].ToString();
                }
                txtChauffeurContact.Text = myreader["CabDriverContact"].ToString();
            }
        }

    }
    protected void Bind_ddlPriority()
    {
        PopulateDropDownBox(2, ddlPriority);
        //ddlPriority.Items.Insert(0, new ListItem("<--select-->", "0"));
    }
    protected void Bind_ddlJobLocation()
    {
        PopulateDropDownBox_JobLocations(Session["BranchID"] != null ? int.Parse(Session["BranchID"].ToString()) : 0);
        //ddlPriority.Items.Insert(0, new ListItem("<--select-->", "0"));
    }
    protected void PopulateDropDownBox_JobLocations(int BranchID)
    {
        DataSet dstDdlJL;
        dstDdlJL = objCRM.PopulateDropDownBox_JobLocations(BranchID);
        //dstDdlJL = Insta.CRM_Insta.PopulateDropDownBox_JobLocations(BranchID);
        if (dstDdlJL.Tables.Count >= 0)
        {
            if (dstDdlJL.Tables[0].Rows.Count > 0)
            {
                ddlJobLocation.DataSource = dstDdlJL;
                ddlJobLocation.DataTextField = "SubBranchName";
                ddlJobLocation.DataValueField = "SubBranchID";
                ddlJobLocation.DataBind();
                ddlJobLocation.ClearSelection();
                ddlJobLocation.SelectedIndex = 1;
            }
            else
            {
                ddlJobLocation.DataSource = null;
                ddlJobLocation.DataBind();
            }
        }
        else
        {
            ddlJobLocation.DataSource = null;
            ddlJobLocation.DataBind();
        }
    }
    protected void Bind_ddlCompSrc()
    {
        PopulateDropDownBox(1, ddlCompSrc);
        // ddlCompSrc.Items.Insert(0, new ListItem("<--select-->", "0"));
    }
    protected void PopulateDropDownBox(int TableID, DropDownList ddl)
    {
        DataSet dstDDL = null;
        dstDDL = objCRM.PopulateDropDownBox(TableID);
        if (dstDDL.Tables.Count > 0)
        {
            if (dstDDL.Tables[0].Rows.Count > 0)
            {
                ddl.DataSource = dstDDL;
                ddl.DataTextField = "Description";
                ddl.DataValueField = "EntityValue";
                ddl.DataBind();
            }
            else
            {
                ddl.DataSource = null;
                ddl.DataBind();
            }
        }
        else
        {
            ddl.DataSource = null;
            ddl.DataBind();
        }
    }
    #endregion

    #region SearchingAndDisplay
    protected void ddlCabNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Get_ChauffeurInfo(int.Parse(ddlCabNo.SelectedValue.ToString()));
    }
    protected void ddlJobLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        int BranchID;
        if (int.Parse(ddlLocation.SelectedValue) == 0)
            BranchID = int.Parse(Session["BranchID"].ToString());
        else
            BranchID = int.Parse(ddlLocation.SelectedValue);

        EnableDisable_JobID();
        EnableDisable_btnSearch();
        Emty_gvSearch();
        if (txtJobID.Text.Trim() != "")
            SearchJobDetails(int.Parse(ddlJobLocation.SelectedValue.ToString()), txtJobID.Text.Trim() != "" ? txtJobID.Text.Trim() : "0", BranchID);

    }
    protected void txtJobID_TextChanged(object sender, EventArgs e)
    {
        int BranchID;
        if (int.Parse(ddlLocation.SelectedValue) == 0)
            BranchID = int.Parse(Session["BranchID"].ToString());
        else
            BranchID = int.Parse(ddlLocation.SelectedValue);

        if (txtJobID.Text.Trim() != "")
           SearchJobDetails(int.Parse(ddlJobLocation.SelectedValue.ToString()), txtJobID.Text.Trim() != "" ? txtJobID.Text.Trim() : "0", BranchID);
           
        
    }
    protected void EnableDisable_JobID()
    {
        if (ddlJobLocation.SelectedValue.ToString() != "-1")
        {
            txtJobID.Enabled = true;
            btnRegister.Enabled = true;
        }
        else
        {
            txtJobID.Enabled = false;
            btnRegister.Enabled = false;
        }
    }
    protected void EnableDisable_btnClose()
    {
        if (gvComplaints.SelectedIndex != -1 && ddlcomplaintStatus.SelectedIndex != 1)
        {
            btnClose.Enabled = true;
        }
        else
        {
            btnClose.Enabled = false;
        }
    }
    protected void VisibleDisable_btnReOpen()
    {
        if (gvComplaints.SelectedIndex != -1 && ddlcomplaintStatus.SelectedValue == "11")
        {
            btnReOpen.Visible = true;
        }
        else
        {
            btnReOpen.Visible = false;
        }
    }
    protected void EnableDisable_btnUpdate()
    {
        if (ddlcomplaintStatus.SelectedIndex != 1)
        {
            btnUpdate.Enabled = true;
        }
        else
        {
            btnUpdate.Enabled = false;
        }
    }
    protected void EnableDisable_btnSearch()
    {
        if (ddlJobLocation.SelectedValue.ToString() != "-1")
        {
            btnSearch.Enabled = true;
        }
        else
        {
            btnSearch.Enabled = false;
        }
    }

    protected void SearchJobDetails(int JobLocationID, string JobID, int BranchID)
    {
        SqlDataReader sdrSRCH;
        sdrSRCH = objCRM.SearchJobDetails(JobLocationID, JobID, BranchID);

        txtJobDate.Text = "";
        txtCustContact.Text = "";
        txtCustName.Text = "";
        txtJobFrom.Text = "";
        txtJobTo.Text = "";
        ddlCabNo.SelectedIndex = 0;
        lblCabID.Text = "";
        lblInvoice.Text = "";
        txtEmailId.Text = "";
        txtCabNo.Text = "";
        if (sdrSRCH.Read())
        {

            if (BranchID < 26)
            {
                ddlCabNo.Visible = true;
                lblCabID.Visible = false;
                ddlCabNo.ClearSelection();
                if (sdrSRCH["CabID"].ToString() != "0")
                {
                    if (ddlCabNo.Items.FindByValue(sdrSRCH["CabID"].ToString()) != null)
                    {
                        ddlCabNo.Items.FindByValue(sdrSRCH["CabID"].ToString()).Selected = true;
                    }
                }
            }
            else
            {
                ddlCabNo.Visible = false;
                ddlCabNo.Visible = false;
                lblCabID.Visible = true;
                lblCabID.Text = sdrSRCH["CabID"].ToString();
                lbl_CarCat.Text = sdrSRCH["CarCategory"].ToString();
                txtChauffName.Text = sdrSRCH["chauffeurName"].ToString();
            }

            if (BranchID < 26)
            {
                txtChauffName.Visible = true;
                txtChauffeurContact.Visible = true;
                lblInvoice.Visible = false;
                Get_ChauffeurInfo(int.Parse(ddlCabNo.SelectedValue.ToString()));
            }
            else
            {
                txtChauffName.Visible = true; ;
                txtChauffeurContact.Visible = false;
                ddlCabNo.Visible = false;
                lblInvoice.Visible = true;
                // Insta _Mig //  lblInvoice.Text = sdrSRCH["InvoiceAmount"].ToString();   
            }

            txtJobDate.Text = sdrSRCH["JobDate"].ToString();
            txtCustContact.Text = sdrSRCH["CallerId"].ToString();
            txtCustName.Text = sdrSRCH["CallerName"].ToString();
            txtJobFrom.Text = sdrSRCH["PickupAddress"].ToString();
            txtJobTo.Text = sdrSRCH["DestinationAddress"].ToString();
            lblJobType.Text = sdrSRCH["JobType"].ToString();
            txtEmailId.Text = sdrSRCH["EmailID"].ToString();
            txtChauffName.Text = sdrSRCH["chauffeurName"].ToString();
            txtCabNo.Text = sdrSRCH["CabID"].ToString();
            lbl_CarCat.Text = sdrSRCH["CarCategory"].ToString();
        }

    }
    #endregion

    #region ManageGridViewDetails
    protected void FillGridView_ForDetails(int ComplaintID)
    {
        SqlDataReader sdrSRCH;
        DataSet ds=new DataSet ();
        sdrSRCH = objCRM.SearchComplaint(ComplaintID);
        ResetAll();
        if (sdrSRCH.Read())
        {
            ddlJobLocation.DataBind();
            ddlJobLocation.ClearSelection();
            ddlJobLocation.Items.FindByValue(sdrSRCH["JobLocationID"].ToString()).Selected = true;
            ddlPriority.DataBind();
            ddlPriority.ClearSelection();
            try
            {
                
                ddlPriority.Items.FindByValue(sdrSRCH["PriorityID"].ToString()).Selected = true;
            }
            catch (Exception)
            {
                ddlPriority.Items.FindByValue("0").Selected = true;
            }

            ddlPriority_SelectedIndexChanged(null,null);
            //ddlComplaintSubCategory.DataBind();
            //ddlComplaintSubCategory.ClearSelection();
            try
            {
                ddlComplaintSubCategory.Items.FindByValue(sdrSRCH["ComplaintSubID"].ToString()).Selected = true;
            }
            catch (Exception)
            {
                ddlComplaintSubCategory.Items.FindByValue("0").Selected = true;
            }

            txtJobID.Text = sdrSRCH["JobID"].ToString();
            txtJobDate.Text = sdrSRCH["JobDate"].ToString();
            txtCustName.Text = sdrSRCH["CustomerName"].ToString();
            txtCustContact.Text = sdrSRCH["CustomerContact"].ToString();
            txtJobFrom.Text = sdrSRCH["Job_From"].ToString();
            txtJobTo.Text = sdrSRCH["Job_To"].ToString();
            lbl_CarCat.Text = sdrSRCH["CarCategory"].ToString();
            //txtComplaintRemark.Text =Convert.ToString(sdrSRCH["SubComplaintRemark"]);
            txtEmailId.Text = sdrSRCH["GuestEmailId"].ToString();
            txtChauffName.Text = sdrSRCH["chauffeurName"].ToString();
            txtCabNo.Text = sdrSRCH["CabID"].ToString();
            if (int.Parse(sdrSRCH["BranchID"].ToString()) < 26)
            {
                ddlCabNo.Visible = true;
                ddlCabNo.DataBind();
                ddlCabNo.ClearSelection();
               // if (sdrSRCH["CabID"].ToString() != "0")
                   // ddlCabNo.Items.FindByValue(sdrSRCH["CabID"].ToString()).Selected = true;//Commented By Akhilesh
                lblCabID.Visible = false;
            }
            else
            {
                ddlCabNo.Visible = false;
                lblCabID.Visible = true;
                lblCabID.Text = sdrSRCH["CabID"].ToString();
            }

            try
            { 
                ddlResolution.DataBind();
                ddlResolution.ClearSelection();
                ddlResolution.Items.FindByValue(sdrSRCH["ResolutionAt"].ToString()).Selected = true; // sometime gets Error
            }
            catch (Exception)
            {                
                ddlResolution.DataBind();
                ddlResolution.ClearSelection();
                ddlResolution.Items.FindByValue("0").Selected = true;
            }
                      
            txtChauffName.Text = sdrSRCH["ChauffeurName"].ToString();
            txtChauffeurContact.Text = sdrSRCH["ChauffeurContact"].ToString();

            ddlCompSrc.DataBind();
            ddlCompSrc.ClearSelection();
            ddlCompSrc.Items.FindByValue(sdrSRCH["ComplaintSourceID"].ToString()).Selected = true;
            //txtCompDate.Text = sdrSRCH["ComplaintDateTime"].ToString();

            txtCompDesc.Text = sdrSRCH["ComplaintDescription"].ToString();
           // txtCPDetails.Text = sdrSRCH["ContactDetails"].ToString() == "" ? "NA" : sdrSRCH["ContactDetails"].ToString(); ;
            lblInvoice.Text = sdrSRCH["InvoiceAmount"].ToString();

            ReadOnly(1);
            btnRegister.Visible = false;
            btnNew.Visible = true;
            lblEscInfo.Text = sdrSRCH["Level"].ToString();
            lblJobType.Text = sdrSRCH["JobType"].ToString();
        }

    }
    protected void gvComplaints_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            FillGridView_ForDetails(int.Parse(gvComplaints.SelectedDataKey[0].ToString()));
            VisibleDisable_btnReOpen();
            int JobId = Convert.ToInt32(gvComplaints.SelectedRow.Cells[3].Text);
            GetChauffeurDetails(JobId);
        }
        catch (Exception err)
        {
            ltrlMsg.Text = "[While Opening Complaint]" + err.Message;
        }

    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        int BranchID;
        if(int.Parse(ddlLocation.SelectedValue) == 0)
            BranchID = int.Parse(Session["BranchID"].ToString());
        else
            BranchID = int.Parse(ddlLocation.SelectedValue);

        SearchJobDetails(int.Parse(ddlJobLocation.SelectedValue.ToString()), gvSearch.SelectedDataKey[0].ToString(), BranchID);
        txtJobID.Text = gvSearch.SelectedDataKey[0].ToString();

    }
    protected void ReadOnly(int Mode)
    {
        if (Mode == 1)
        {
            ddlJobLocation.Enabled = false;
            ddlPriority.Enabled = false;
            txtJobID.ReadOnly = true;
            txtJobDate.ReadOnly = true;
            txtCustName.ReadOnly = true;
            txtCustContact.ReadOnly = true;
            txtJobFrom.ReadOnly = true;
            txtJobTo.ReadOnly = true;
            ddlCabNo.Enabled = false;
            ddlResolution.Enabled = false;
            txtChauffName.ReadOnly = true;
            txtChauffeurContact.ReadOnly = true;
            ddlCompSrc.Enabled = false;
            //txtCompDate.ReadOnly = true;
            // ddlHour.Enabled = false;
            // ddlMinute.Enabled = false;
            txtCompDesc.ReadOnly = true;
            //txtCPDetails.ReadOnly = true;
        }
        if (Mode == 2)
        {
            ddlJobLocation.Enabled = true;
            ddlPriority.Enabled = true;
            txtJobID.ReadOnly = false;
            txtJobDate.ReadOnly = false;
            txtCustName.ReadOnly = false;
            txtCustContact.ReadOnly = false;
            txtJobFrom.ReadOnly = false;
            txtJobTo.ReadOnly = false;
            ddlCabNo.Enabled = true;
            ddlResolution.Enabled = true;
            txtChauffName.ReadOnly = false;
            txtChauffeurContact.ReadOnly = false;
            ddlCompSrc.Enabled = true;
            //txtCompDate.ReadOnly = false;
            //ddlHour.Enabled = true;
            //ddlMinute.Enabled = true;
            txtCompDesc.ReadOnly = false;
            //txtCPDetails.ReadOnly = false;
        }

    }
    #endregion

    #region PageCycle
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["UserID"] = "3122";
      //  Session["UserID"] = Request.QueryString["id"].ToString();
        object UserName = Insta.CRM_Insta.GetUserName(Convert.ToInt32(Session["UserID"]));
        Session["User"] = UserName.ToString();
              
       // Session["User"] = "akhilesh.kumar";
        if (!IsPostBack)
        {
            if (Session["UserID"] != null && Convert.ToInt32(Session["UserID"]) > 0)
            {

            }
            else
            {
                Response.Redirect("http://insta.carzonrent.com");
            }
            Session["BranchID"] = "1";
            Session["SubBranchID"] = "1";

          //  DataSet ds = new DataSet();
           
            ddlLocation.DataBind();
            ddlLocation.DataSource=objInsta.ListCrmLocations(Convert.ToString(Session["UserID"]));
            ddlLocation.DataTextField="BranchName";
            ddlLocation.DataValueField="BranchID";
            ddlLocation.DataBind();

          //  ddlLocation.Items.Remove(ddlLocation.Items.FindByValue("0")); // by nalin 
            Bind_ddlCabNo(int.Parse(Session["BranchID"].ToString()), int.Parse(Session["SubBranchID"].ToString()), 1);
            
            
            Bind_ddlPriority();
            Bind_ddlJobLocation();
            Bind_ddlCompSrc();
            Bind_ddlResolution();
            EnableDisable_JobID();
            EnableDisable_btnClose();
            EnableDisable_btnSearch();
            VisibleDisable_btnReOpen();
            btnNew.Visible = false;
            gvComplaints.DataBind();
            ShowComplaints();
            gvComplaints.SelectedIndex = -1;
            gvResolutions.DataBind();

        }
        Page.Title = "Insta : CRM";
    }
    #endregion PageCycle

    #region Events
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;
        else
        {

            int BranchID;
            if (int.Parse(ddlLocation.SelectedValue) == 0)
                BranchID = int.Parse(Session["BranchID"].ToString());
            else
                BranchID = int.Parse(ddlLocation.SelectedValue);

            string ComplaintDateTime;
            //ComplaintDateTime=txtCompDate.Text + " " + ddlHour.SelectedValue.ToString() + ":" + ddlMinute.SelectedValue.ToString() + ":00";
            ComplaintDateTime = System.DateTime.Now.ToString();
            //New Line as on 18 sept 09 by NCJ
            RegisterComplaint(BranchID, Convert.ToDateTime(ComplaintDateTime), Session["User"].ToString(), System.DateTime.Now);
            ShowComplaints();

            //old line by NCJ on same date as above
            //RegisterComplaint(int.Parse(Session["BranchID"].ToString()),Convert.ToDateTime(ComplaintDateTime), Session["User"].ToString(), System.DateTime.Now);
            //gvComplaints.DataBind();


            ResetAll();
            VisibleDisable_btnReOpen();
        }
    }
    protected void RegisterComplaint(int BranchID, DateTime ComplaintDateTime, string RegisteredBy, DateTime Reg_DateTime)
    {
        ltrlMsg.Text = "";
        ltrlErr.Text = "";
        string ChauffeurName = "";
        string ChauffeurContact = "";
        // Insta  //int CabID;
        string CabID = "";
        if (BranchID < 26)
        {
            ChauffeurName = txtChauffName.Text.Trim();
            ChauffeurContact = txtChauffeurContact.Text.Trim();
            // insta  //  CabID = int.Parse(ddlCabNo.SelectedValue.ToString());
        }
        else
        {
            // ChauffeurName = "NA";
            ChauffeurName = txtChauffName.Text.Trim(); // by nalin 
            //ChauffeurContact = "0"; //commented and added on 07_Oct-2016 by rahul
            ChauffeurContact = txtChauffeurContact.Text.Trim();
            CabID = lblCabID.Text.Trim() != "" ? lblCabID.Text.Trim() : "0";
        }

        String Message = Insta.CRM_Insta.RegisterComplaint
            (
             BranchID
            , int.Parse(ddlJobLocation.SelectedValue.ToString())
            , txtJobID.Text.Trim()
            , Convert.ToDateTime(txtJobDate.Text.Trim())
            , txtJobFrom.Text.Trim()
            , txtJobTo.Text.Trim()
            , txtCustName.Text.Trim()
            , txtCustContact.Text.Trim() != "" ? txtCustContact.Text.Trim() : "0"
            , ""
            , txtRemarks.Text.Trim()
            , CabID
            , ChauffeurName
            , ChauffeurContact == "" ? "0" : ChauffeurContact
            , int.Parse(ddlCompSrc.SelectedValue.ToString())
            , ComplaintDateTime
            , txtCompDesc.Text.Trim()
            , RegisteredBy
            , Reg_DateTime
            , int.Parse(ddlResolution.SelectedValue.ToString())
            , int.Parse(ddlPriority.SelectedValue.ToString())
            , 8
            , lbl_CarCat.Text.Trim()
            , int.Parse(ddlComplaintSubCategory.SelectedValue.ToString())
            , txtEmailId.Text.Trim()
            , txtCabNo.Text.Trim()
            );
        ltrlMsg.Text = Message;

    }
    protected void ResetAll()
    {
        ddlJobLocation.SelectedIndex = 1;
        ddlPriority.SelectedIndex = 0;
        txtJobID.Text = "";
        txtJobDate.Text = "";
        txtCustName.Text = "";
        txtCustContact.Text = "";
        txtJobFrom.Text = "";
        txtJobTo.Text = "";
        lbl_CarCat.Text = "";
        ddlCabNo.SelectedIndex = 0;
        ddlResolution.SelectedIndex = 0;
        txtChauffName.Text = "";
        txtChauffeurContact.Text = "";
        ddlCompSrc.SelectedIndex = 0;
        ////  txtCompDate.Text = "";
        // ddlHour.SelectedIndex = 0;
        //  ddlMinute.SelectedIndex = 0;
        txtCompDesc.Text = "";
        //txtCPDetails.Text = "";
        txtRemarks.Text = "";
        EnableDisable_JobID();
        EnableDisable_btnClose();
        EnableDisable_btnUpdate();
        EnableDisable_btnSearch();
        Emty_gvSearch();
        //Emty_gvResolutions();
        lblCabID.Text = "";
        lblInvoice.Text = "";
        lblEscInfo.Text = "";
        txtEmailId.Text = string.Empty;
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        ltrlErr.Text = "";
        ltrlMsg.Text = "";
        if (txtRemarks.Text.Trim() != "")
        {
            if (gvComplaints.SelectedIndex > -1)
            {
   
                 string CarRegNo="";
                 int CarId = 0, ChauffeurId=0;
                //CarRegNo="Kamlesh Mishra(9172984626)-2351 MH48F";
                if(ddlChauffeurAllocated.SelectedItem.Text.ToString()!= "<--Select Chauffeur-->")
                {
                    CarRegNo = ddlChauffeurAllocated.SelectedItem.Text.ToString();
                    CarRegNo = CarRegNo.Remove(0, CarRegNo.IndexOf('-') + 1);
                    string[] ar = CarRegNo.Split('-');
                    CarRegNo = ar[0].ToString();
                    ChauffeurId = Convert.ToInt32(ar[1]);
                    CarId = Convert.ToInt32(ddlChauffeurAllocated.SelectedValue);
                }
                string ChauffeurAllocated = ddlChauffeurAllocated.SelectedItem.Text.ToString() == "<--Select Chauffeur-->" ? "" : ddlChauffeurAllocated.SelectedItem.Text.ToString();
                int CompSubCatId = string.IsNullOrEmpty(ddlComplaintResolutionSubCat.SelectedValue.ToString()) ? 0 : Convert.ToInt32(ddlComplaintResolutionSubCat.SelectedValue);
                           
                

                /////////////////
               // DoResolution(int.Parse(gvComplaints.SelectedDataKey[0].ToString()), txtRemarks.Text.Trim(), Session["User"] != null ? Session["User"].ToString() : "noname", 0, Convert.ToChar(ddlChauffeurFault.SelectedValue), ddlChauffeurAllocated.SelectedItem.Text.ToString(), Convert.ToInt32(ddlComplaintResolutionCat.SelectedValue), Convert.ToInt32(ddlComplaintResolutionSubCat.SelectedValue), txtRemarksIfAny.Text.ToString(), Convert.ToInt32(ddlChauffeurAllocated.SelectedValue), CarRegNo);
                DoResolution(int.Parse(gvComplaints.SelectedDataKey[0].ToString()), txtRemarks.Text.Trim(), Session["User"] != null ? Session["User"].ToString() : "noname", 0, Convert.ToChar(ddlChauffeurFault.SelectedValue), ChauffeurAllocated, Convert.ToInt32(ddlComplaintResolutionCat.SelectedValue), CompSubCatId, txtRemarksIfAny.Text.ToString(), CarId, CarRegNo);
                gvResolutions.DataBind();
                gvComplaints.DataBind();
            }
            else
            {
                ltrlErr.Text = "Select a complaint first";
            }
        }
        else
        {
            ltrlErr.Text = "Give Resolution";
        }
    }
    //protected void DoResolution(int ComplaintID, string Resolution, string ResolutionBy, int ComplaintStatus)
    protected void DoResolution(int ComplaintID, string Resolution, string ResolutionBy, int ComplaintStatus,char WasChauffeurFault,string ChauffeurAllocated,int ComplaintResolutionCatId,int ComplaintResolutionSubCatId,string ComplaintResolutionRemarks,int ComplaintResolutionCarId,string ComplaintResolutionCarRegNo)
    {
        ltrlMsg.Text = "";
        ltrlErr.Text = "";
        objInsta = new CRM_Insta();
        //ltrlMsg.Text = objInsta.DoResolution(ComplaintID, Resolution, ResolutionBy, ComplaintStatus);
       // DoResolution(int ComplaintID, string Resolution, string ResolutionBy, int ComplaintStatus,char WasChauffeurFault,string ChauffeurAllocated,int ComplaintResolutionCatId,int ComplaintResolutionSubCatId,string ComplaintResolutionRemarks,int ComplaintResolutionCarId,string ComplaintResolutionCarRegNo)
        ltrlMsg.Text = objInsta.DoResolution(ComplaintID, Resolution, ResolutionBy, ComplaintStatus, WasChauffeurFault, ChauffeurAllocated, ComplaintResolutionCatId, ComplaintResolutionSubCatId, ComplaintResolutionRemarks, ComplaintResolutionCarId, ComplaintResolutionCarRegNo);
   }
    protected void btnNew_Click(object sender, EventArgs e)
    {
        ltrlMsg.Text = "";
        ltrlErr.Text = "";
        btnNew.Visible = false;
        btnRegister.Visible = true;
        ReadOnly(2);
        gvComplaints.SelectedIndex = -1;
        ResetAll();

    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        ltrlMsg.Text = "";
        ltrlErr.Text = "";
        string CarRegNo = "";
        int CarId=0;
        int ComplaintId = 0, ChauffeurId = 0;
        int VendorCarYN=0, VendorChauffYN=0;
        if (txtRemarks.Text.Trim() != "")
        {
            if (gvComplaints.SelectedIndex > -1)
            {
                if(ddlChauffeurAllocated.SelectedItem.Text.ToString()!= "<--Select Chauffeur-->")
                {
                    CarRegNo=ddlChauffeurAllocated.SelectedItem.Text.ToString();
                    //CarRegNo = CarRegNo.Remove(0, CarRegNo.LastIndexOf('-') + 1);
                    CarRegNo = CarRegNo.Remove(0, CarRegNo.IndexOf('-') + 1);
                    string[] ar = CarRegNo.Split('-');
                    CarRegNo = ar[0].ToString();
                    ChauffeurId = Convert.ToInt32(ar[1]);
                    VendorCarYN = Convert.ToInt32(ar[2]);
                    VendorChauffYN = Convert.ToInt32(ar[3]);
                    CarId=Convert.ToInt32(ddlChauffeurAllocated.SelectedValue);
                    
                }
                // obj.BlockUnblockStatus = dsStatus.Tables[0].Rows[0]["CabStatus"].ToString() == "0" ? obj.BlockUnblockStatus = "1" : obj.BlockUnblockStatus = "0";
                //DoResolution(int.Parse(gvComplaints.SelectedDataKey[0].ToString()), txtRemarks.Text.Trim(), Session["User"] != null ? Session["User"].ToString() : "noname", 1, Convert.ToChar(ddlChauffeurFault.SelectedValue), ddlChauffeurAllocated.SelectedItem.Text.ToString(), Convert.ToInt32(ddlComplaintResolutionCat.SelectedValue), Convert.ToInt32(ddlComplaintResolutionSubCat.SelectedValue), txtRemarksIfAny.Text.ToString(), Convert.ToInt32(ddlChauffeurAllocated.SelectedValue), CarRegNo);
                DoResolution(int.Parse(gvComplaints.SelectedDataKey[0].ToString()), txtRemarks.Text.Trim(), Session["User"] != null ? Session["User"].ToString() : "noname", 1, Convert.ToChar(ddlChauffeurFault.SelectedValue), ddlChauffeurAllocated.SelectedItem.Text.ToString(), Convert.ToInt32(ddlComplaintResolutionCat.SelectedValue), Convert.ToInt32(ddlComplaintResolutionSubCat.SelectedValue), txtRemarksIfAny.Text.ToString(), Convert.ToInt32(ddlChauffeurAllocated.SelectedValue), CarRegNo);
                // gvResolutions.DataBind();
                ComplaintId = int.Parse(gvComplaints.SelectedDataKey[0].ToString());
                gvComplaints.DataBind();
                gvComplaints.SelectedIndex = -1;
                if(lstPenality.Items.Count > 0)
                    {
                        string TotalPenality = string.Empty;
                        for (int i = 0; i < lstPenality.Items.Count; i++)
                        {
                            if (i == 0)
                            { 
                                //code to deactivate all old details for the complaintid
                                string ii= objInsta.DeActivateOldPenality(ComplaintId);
                            }
                            if (lstPenality.Items[i].Selected)
                            {
                                int PenalityId =Convert.ToInt32((lstPenality.Items[i].Value.Trim()));
                                string output = objInsta.AddChauffeurPenality(ComplaintId, CarRegNo, CarId, ChauffeurId, txtRemarksIfAny.Text.ToString(), PenalityId,Convert.ToInt32(Session["UserId"]), true,Convert.ToBoolean(VendorCarYN),Convert.ToBoolean(VendorChauffYN));
                            }

                        }
                       
                    }
               
            }
            else
            {
                ltrlErr.Text = "Select a complaint first";
            }

        }
        else
        {
            ltrlErr.Text = "Resolution Before Closing this complaint";
        }
        ResetAll();
    }
    protected void ddlcomplaintStatus_SelectedIndexChanged(object sender, EventArgs e)
    {

        ResetAll();
    }

    protected void BindGridView_gvSearch()
    {

        int BranchID;
        if (int.Parse(ddlLocation.SelectedValue) == 0)
            BranchID = int.Parse(Session["BranchID"].ToString());
        else
            BranchID = int.Parse(ddlLocation.SelectedValue);

        DataSet dst = new DataSet();
        dst = objCRM.SearchContactDetails(int.Parse(ddlJobLocation.SelectedValue.ToString()), txtCustContact.Text.Trim(), BranchID);
        //dst = Insta.CRM_Insta.SearchContactDetails(int.Parse(ddlJobLocation.SelectedValue.ToString()), txtCustContact.Text.Trim(), BranchID);
        if (dst.Tables.Count > 0)
        {
            if (dst.Tables[0].Rows.Count > 0)
            {
                gvSearch.DataSource = dst.Tables[0];
                gvSearch.DataBind();
            }
            else
            {
                gvSearch.DataSource = null;
                gvSearch.DataBind();
            }
        }
        else
        {
            gvSearch.DataSource = null;
            gvSearch.DataBind();

        }


    }
    protected void Emty_gvSearch()
    {
        gvSearch.DataSource = null;
        gvSearch.DataBind();
    }
    protected void Emty_gvResolutions()
    {
        //gvResolutions.DataSource = null;
        //gvResolutions.DataBind();
        //DataSource_For_gvResolutions.SelectParameters[0].DefaultValue = "0";

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (txtCustContact.Text.Trim() != "")
        {
            if (ddlJobLocation.SelectedIndex != 0)
            {
                BindGridView_gvSearch();
            }
        }
        //if (txtJobID.Text.Trim() != "")
        //{
            
        //    DataSet dsChauffeur = new DataSet();
        //    objInsta = new CRM_Insta();
        //    dsChauffeur = objInsta.GetChauffeurDetails(txtJobID.Text.Trim() != "" ? Convert.ToInt32(txtJobID.Text.Trim()) : 0);
        //    if (dsChauffeur.Tables[0].Rows.Count > 0)
        //    {
        //        ddlChauffeurAllocated.DataSource = dsChauffeur.Tables[0];
        //        ddlChauffeurAllocated.DataTextField = "ChauffeurDetails";
        //        ddlChauffeurAllocated.DataValueField = "CarID";
        //        ddlChauffeurAllocated.DataBind();
        //    }
        //    else
        //    {
        //        ddlChauffeurAllocated.DataSource = null;
        //        ddlChauffeurAllocated.DataBind();
        //    }
        //}
    }

    protected void btnShowComplaints_Click(object sender, EventArgs e)
    {
        ResetAll();
        //gvComplaints.SelectedIndex = -1;
        SearchComplaints(txtCID.Text.Trim() != "" ? txtCID.Text.Trim() : "0");
        VisibleDisable_btnReOpen();


    }
    protected void btnSearchCID_Click(object sender, EventArgs e)
    {

    }
    protected void btnReOpen_Click(object sender, EventArgs e)
    {
        ltrlMsg.Text = "";
        ltrlErr.Text = "";
        string CarRegNo = "";
        int ChauffeurId = 0;
        if (txtRemarks.Text.Trim() != "")
        {
            if (gvComplaints.SelectedIndex > -1)
            {
               // DoResolution(int.Parse(gvComplaints.SelectedDataKey[0].ToString()), txtRemarks.Text.Trim(), Session["User"] != null ? Session["User"].ToString() : "noname", 2);
                //CarRegNo = ddlChauffeurAllocated.SelectedItem.Text.ToString();
                //CarRegNo = CarRegNo.Remove(0, CarRegNo.LastIndexOf('-') + 1);
                CarRegNo = ddlChauffeurAllocated.SelectedItem.Text.ToString();
                CarRegNo = CarRegNo.Remove(0, CarRegNo.IndexOf('-') + 1);
                string[] ar = CarRegNo.Split('-');
                CarRegNo = ar[0].ToString();
                ChauffeurId = Convert.ToInt32(ar[1]);
               // CarId = Convert.ToInt32(ddlChauffeurAllocated.SelectedValue);

                DoResolution(int.Parse(gvComplaints.SelectedDataKey[0].ToString()), txtRemarks.Text.Trim(), Session["User"] != null ? Session["User"].ToString() : "noname", 2, Convert.ToChar(ddlChauffeurFault.SelectedValue), ddlChauffeurAllocated.SelectedItem.Text.ToString(), Convert.ToInt32(ddlComplaintResolutionCat.SelectedValue), Convert.ToInt32(ddlComplaintResolutionSubCat.SelectedValue), txtRemarksIfAny.Text.ToString(), Convert.ToInt32(ddlChauffeurAllocated.SelectedValue), CarRegNo);
                gvComplaints.DataBind();
                gvComplaints.SelectedIndex = -1;

            }
            else
            {
                ltrlErr.Text = "Select a Complaint First";
            }

        }
        else
        {
            ltrlErr.Text = "Give Reason For Openining This Complaint";
        }
        ResetAll();
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSearch.PageIndex = e.NewPageIndex;
        BindGridView_gvSearch();
    }

    #endregion Events

    #region NewCodeBlock
    private void ShowComplaints()
    {
        int BranchID;
        if (int.Parse(ddlLocation.SelectedValue) == 0)
            BranchID = int.Parse(Session["BranchID"].ToString());
        else
            BranchID = int.Parse(ddlLocation.SelectedValue);

        DataSource_For_gvComplaints.SelectParameters[0].DefaultValue = BranchID.ToString();
        DataSource_For_gvComplaints.SelectParameters[1].DefaultValue = ddlcomplaintStatus.SelectedValue;
        DataSource_For_gvComplaints.SelectParameters[2].DefaultValue = txtCID.Text.ToString().Trim() != "" ? txtCID.Text.ToString().Trim() : "0";
        DataSource_For_gvComplaints.SelectParameters[3].DefaultValue = rbblSearch.SelectedValue;

        gvComplaints.DataBind();
    }
    private void SearchComplaints(string ComplaintID)
    {
        int BranchID;
        if (int.Parse(ddlLocation.SelectedValue) == 0)
            BranchID = int.Parse(Session["BranchID"].ToString());
        else
            BranchID = int.Parse(ddlLocation.SelectedValue);

        DataSource_For_gvComplaints.SelectParameters[0].DefaultValue = BranchID.ToString();
        DataSource_For_gvComplaints.SelectParameters[1].DefaultValue = ddlcomplaintStatus.SelectedValue;
        DataSource_For_gvComplaints.SelectParameters[2].DefaultValue = ComplaintID;
        DataSource_For_gvComplaints.SelectParameters[3].DefaultValue = rbblSearch.SelectedValue;

        gvComplaints.DataBind();
    }
    private void ClearFields()
    {
        ddlJobLocation.SelectedIndex = 1;
        ddlPriority.SelectedIndex = 0;
        txtJobID.Text = "";
        txtJobDate.Text = "";
        txtCustName.Text = "";
        txtCustContact.Text = "";
        txtJobFrom.Text = "";
        txtJobTo.Text = "";
        ddlCabNo.SelectedIndex = 0;
        ddlResolution.SelectedIndex = 0;
        txtChauffName.Text = "";
        txtChauffeurContact.Text = "";
        ddlCompSrc.SelectedIndex = 0;
        txtCompDesc.Text = "";
        //txtCPDetails.Text = "";
        txtRemarks.Text = "";

        ltrlMsg.Text = "";
        ltrlErr.Text = "";

        lblCabID.Text = "";
        lblInvoice.Text = "";
        lblEscInfo.Text = "";
        lbl_CarCat.Text = "";
        txtEmailId.Text = "";
        txtCabNo.Text = "";
    }
    
    protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        ClearFields();
        ShowComplaints();

        if (int.Parse(ddlLocation.SelectedValue) < 26)
        {
            txtChauffName.Visible = true;
            txtChauffeurContact.Visible = true;
            ddlCabNo.Visible = true;

            lblFrom.Text = "Travel From";
            lblTo.Text = "To";

            //lblInvoiceH.Visible = false;
            lblInvoice.Visible = false;

            lblChauffeurNameH.Visible = true;
            lblContactH.Visible = true;
            lblCabID.Visible = false;

            lblGuestH.Text = "Customer Name";
        }
        else
        {
            txtChauffName.Visible = true; // by nalin  
            txtChauffeurContact.Visible = false;
            ddlCabNo.Visible = false;

            lblFrom.Text = "Pickup From";
            lblTo.Text = "Company Name";

            //lblInvoiceH.Visible = true;
            lblInvoice.Visible = true;

            //lblInvoiceH.Text = "Invoice";

            lblChauffeurNameH.Visible = true; // by nalin 
            lblContactH.Visible = false;
            lblCabID.Visible = true;
            lblGuestH.Text = "Guest Name";
            
        }
    }

    protected void ddlPriority_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        objInsta = new CRM_Insta();
        string resolution;
        ds = objInsta.GetSubComplaintSubType(Convert.ToInt32(ddlPriority.SelectedValue.ToString()));
       
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlComplaintSubCategory.DataSource = ds.Tables[0];
            ddlComplaintSubCategory.DataTextField = "Description";
            ddlComplaintSubCategory.DataValueField = "EntityValue";
            ddlComplaintSubCategory.DataBind();
            try
            {
                //ddlResolution.SelectedItem.Value = objInsta.GetResolutionId(Convert.ToInt32(ddlPriority.SelectedValue.ToString())).ToString();
                resolution = objInsta.GetResolutionId(Convert.ToInt32(ddlPriority.SelectedValue.ToString())).ToString();
                ddlResolution.Items.FindByValue(resolution).Selected = true;
            }
            catch (Exception)
            {
                ddlResolution.SelectedValue ="0";
            }
            
        }
        else
        {
            ddlComplaintSubCategory.DataSource = null;
            ddlComplaintSubCategory.DataBind();
        }


    }
    #endregion NewCodeBlock
      
    public void GetChauffeurDetails(int BookingId)
    {
        if (txtJobID.Text.Trim() != "")
        {
            DataSet dsChauffeur = new DataSet();
            objInsta = new CRM_Insta();
            //dsChauffeur = objInsta.GetChauffeurDetails(txtJobID.Text.Trim() != "" ? Convert.ToInt32(txtJobID.Text.Trim()) : 0);
            dsChauffeur = objInsta.GetChauffeurDetails(BookingId);
            if (dsChauffeur.Tables[0].Rows.Count > 0)
            {
                ddlChauffeurAllocated.DataSource = dsChauffeur.Tables[0];
                ddlChauffeurAllocated.DataTextField = "ChauffeurDetails";
                ddlChauffeurAllocated.DataValueField = "CarID";
                ddlChauffeurAllocated.DataBind();
            }
            else
            {
                ddlChauffeurAllocated.DataSource = null;
                ddlChauffeurAllocated.DataBind();
            }
        }
    
    }
    protected void ddlComplaintResolutionCat_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet dsComCat = new DataSet();
        objInsta = new CRM_Insta();
        dsComCat = objInsta.GetComplaintSubCat(Convert.ToInt32(ddlComplaintResolutionCat.SelectedValue));
        if (dsComCat.Tables[0].Rows.Count > 0)
        {

            ddlComplaintResolutionSubCat.DataSource = dsComCat.Tables[0];
            ddlComplaintResolutionSubCat.DataTextField = "ComplaintSubCategory";
            ddlComplaintResolutionSubCat.DataValueField = "ComplaintSubCatId";
            ddlComplaintResolutionSubCat.DataBind();
        }
        else
        {
            ddlComplaintResolutionSubCat.DataSource = null;
            ddlComplaintResolutionSubCat.DataBind();
        }
    }
    protected void Bind_PenalityType()
    {
        DataSet PenalityType;
        PenalityType = objInsta.GetPenality();
        if (PenalityType != null)
        {
            if (PenalityType.Tables.Count > 0)
            {
                if (PenalityType.Tables[0].Rows.Count > 0)
                {
                    lstPenality.DataSource = PenalityType;
                    lstPenality.DataTextField = "PenalityReason";
                    lstPenality.DataValueField = "Id";
                    lstPenality.DataBind();
                    //ddlCabNo.ClearSelection();
                   // ddlCabNo.Items.Insert(1, new ListItem("NA", "-1"));
                }
                else
                {
                    lstPenality.DataSource = null;
                    lstPenality.DataBind();
                }
            }
        }
    }
    protected void ddlChauffeurFault_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlChauffeurFault.SelectedValue.ToString()== "Y")
        {
            Bind_PenalityType();
            tdlstPenality.Visible = true;
            tdPenality.Visible = true;
        }
        else
        {
            lstPenality.Dispose();
            tdlstPenality.Visible = false;
            tdPenality.Visible = false;
        }
    }
}