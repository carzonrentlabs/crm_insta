Imports Microsoft.VisualBasic

Public Class MonthDateLibrary
    Public Function GetMonth(ByVal month As Integer) As String

        Dim ThisMonth As String = ""
        If month = 1 Then
            ThisMonth = "Januarary"
        ElseIf (month = 2) Then
            ThisMonth = "February"
        ElseIf (month = 3) Then
            ThisMonth = "March"
        ElseIf (month = 4) Then
            ThisMonth = "April"
        ElseIf (month = 5) Then
            ThisMonth = "May"
        ElseIf (month = 6) Then
            ThisMonth = "June"
        ElseIf (month = 7) Then
            ThisMonth = "July"
        ElseIf (month = 8) Then
            ThisMonth = "August"
        ElseIf (month = 9) Then
            ThisMonth = "September"
        ElseIf (month = 10) Then
            ThisMonth = "October"
        ElseIf (month = 11) Then
            ThisMonth = "November"
        ElseIf month = 12 Then
            ThisMonth = "December"
        End If
        GetMonth = ThisMonth
    End Function

    Public Function DataSourceLoadMonth() As ArrayList
        Dim allMonth As New ArrayList
        allMonth.Add("<-Select->")
        allMonth.Add("Janurary")
        allMonth.Add("February")
        allMonth.Add("March")
        allMonth.Add("April")
        allMonth.Add("May")
        allMonth.Add("June")
        allMonth.Add("July")
        allMonth.Add("August")
        allMonth.Add("September")
        allMonth.Add("October")
        allMonth.Add("November")
        allMonth.Add("December")
        Return allMonth
    End Function


End Class
