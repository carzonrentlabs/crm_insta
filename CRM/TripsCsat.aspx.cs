using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
public partial class TripsCsat : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (this.Page.IsPostBack == false)
        {
            ViewState["BranchID"] = int.Parse(Session["BranchID"].ToString());
            LoadRandomRecord();
            ViewCsatSummary();
        }
        Page.Title = "Insta : TRIPS";
    }

    private void LoadRandomRecord()
    {
        try
        {
            // SqlDataReader sdr = Insta.CRM_Insta.LimoReadRandomNextRecord<int>(int.Parse(Session["BranchID"].ToString()), int.Parse(ddlCsatFor.SelectedValue));
            DataSet sdr = Insta.CRM_Insta.TripsReadRandomNextRecord<int>(int.Parse(Session["BranchID"].ToString()), int.Parse(ddlCsatFor.SelectedValue));
            if (sdr.Tables[0].Rows.Count>0 )
            {
                lblCallerNo.Text = sdr.Tables[0].Rows[0]["CallerID"].ToString();
                ltrlChauffName.Text = sdr.Tables[0].Rows[0]["ChauffeurName"].ToString();
                ltrlChauffCont.Text = sdr.Tables[0].Rows[0]["Contact"].ToString();
                ltrlCabId.Text = sdr.Tables[0].Rows[0]["CabID"].ToString();
                ltrlCustomerName.Text = sdr.Tables[0].Rows[0]["CallerName"].ToString();
                ltrlJobDate.Text = sdr.Tables[0].Rows[0]["JobDate"].ToString();
                txtFrom.Text = sdr.Tables[0].Rows[0]["PickupAddress"].ToString().Replace("<br />", "");
                txtTo.Text = sdr.Tables[0].Rows[0]["DestinationAddress"].ToString();
                ltrlJobID.Text = sdr.Tables[0].Rows[0]["jobno"].ToString();
                ViewState["BranchID"] = int.Parse(sdr.Tables[0].Rows[0]["BranchID"].ToString());
                ViewState["CarCategory"] = sdr.Tables[0].Rows[0]["CarCategory"].ToString();
              
            }
        }
        catch (Exception err)
        {
            ltrlMsg.Text = "(When reading Random Record)" + err.Message;
        }
    }
    private void ViewCsatSummary()
    {
        try
        {
            SqlDataReader sdr = Insta.CRM_Insta.TRIPS_CsatSummary<int>(int.Parse(ViewState["BranchID"].ToString()));
            if (sdr.Read())
            {
                lblMonthlyTarget.Text = sdr[0].ToString();
                lblMonthlyCompleted.Text = sdr[1].ToString();
                lblTodayTarget.Text = sdr[2].ToString();
                lblTodayCompleted.Text = sdr[3].ToString();
            }
        }
        catch (Exception err)
        {
            ltrlMsg.Text = "(When showing Summary)" + err.Message;
        }
    }

    private void GetAnswers(out string AnsYes, out string AnsNo, out string AnsNotAns)
    {
        AnsYes = string.Empty;
        AnsNo = string.Empty;
        AnsNotAns = string.Empty;

        foreach (GridViewRow gvr in gvQuestion.Rows)
        {
            RadioButtonList rdblAnswer = (RadioButtonList)gvr.FindControl("rdblAnswer");
            Label lblQID = (Label)gvr.FindControl("lblQID");

            if (rdblAnswer.SelectedValue == "1")
            {
                AnsYes = AnsYes + "," + lblQID.Text;
            }
            else if (rdblAnswer.SelectedValue == "2")
            {
                AnsNo = AnsNo + "," + lblQID.Text;
            }
            else if (rdblAnswer.SelectedValue == "3")
            {
                AnsNotAns = AnsNotAns + "," + lblQID.Text;
            }
        }
    }
    private void ClearAnswers()
    {
        foreach (GridViewRow gvr in gvQuestion.Rows)
        {
            RadioButtonList rdblAnswer = (RadioButtonList)gvr.FindControl("rdblAnswer");
            rdblAnswer.SelectedValue = "3";
        }
        TextBox2.Text = "";
        txtCompDesc.Text = "";
        ddlComplaint.SelectedValue = "0";
    }

    protected void btnSkip_Click(object sender, EventArgs e)
    {
        LoadRandomRecord();
    }
    protected void btnSaveQ_Click(object sender, EventArgs e)
    {

        if (txtCompDesc.Text != "")
        {
            if (ddlComplaint.SelectedValue != "0")
            {
                int csatid = 0;
                ltrlMsg.Text = RegisterInCSAT(out csatid);
                RegisterInCRM();
            }
            else
            {
                ltrlMsg.Text = " Choose Complaint Type";
            }
        }
        else
        {
            int csatid = 0;
            ltrlMsg.Text = RegisterInCSAT(out csatid);
        }
        ClearAnswers();
        LoadRandomRecord();
        ViewCsatSummary();
    }
    private void RegisterInCRM()
    {
       // CRM objCRM = new CRM();
        //int LocationID = 0;
        //Insta.CRM_Insta.RegisterComplaint
        //(
        //  int.Parse(ViewState["BranchID"].ToString())
        //, LocationID
        //, ltrlJobID.Text
        //, DateTime.Parse(ltrlJobDate.Text)
        //, txtFrom.Text
        //, txtTo.Text
        //, ltrlCustomerName.Text
        //, lblCallerNo.Text
        //, "NA"
        //, "Register From CSAT"
        //, ltrlCabId.Text.ToString()
        //, ltrlChauffName.Text
        //, ltrlChauffCont.Text
        //, 4
        //, DateTime.Now
        //, txtCompDesc.Text
        //, Session["User"].ToString()
        //, DateTime.Now
        //, 3
        //, int.Parse(ddlComplaint.SelectedValue)
        //, 8
        //, ViewState["CarCategory"].ToString ()
        //);


    }
    private string RegisterInCSAT(out int csatid)
    {
        string Msg = string.Empty;
        csatid = 0;

        try
        {
            string Y = string.Empty, N = string.Empty, NA = string.Empty;

            GetAnswers(out Y, out N, out NA);

            SqlDataReader sdr = Systems.CRM.CSAT.RegisterInCSAT<int, long, string>
            (
              int.Parse(ViewState["BranchID"].ToString())
            , long.Parse(lblCallerNo.Text.ToString())
            , ltrlJobID.Text
            , Y
            , N
            , NA
            , TextBox2.Text.Trim()
            , Session["User"].ToString()
            );
            if (sdr.Read())
            {
                csatid = int.Parse(sdr[0].ToString());
                Msg = sdr[1].ToString();
            }

        }
        catch (Exception err)
        {
            Msg = err.Message;
            csatid = 0;
        }

        return Msg;
    }
    protected void ddlCsatFor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCsatFor.SelectedValue == "2")
        {
            Response.Redirect("LimoCsat.aspx");
        }
        else if (ddlCsatFor.SelectedValue == "3")
        {
            Response.Redirect("TripsCsat.aspx");
        }
        else
        {
           // LoadRandomRecord();
           // ViewCsatSummary();
            Response.Redirect("Csat.aspx");
        }
        
    }
}
