using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Insta;

public class CorporateModule
{
	public CorporateModule()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    
    public DataSet GetAccountManager()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_getUserList");
    }
    public DataSet GetImplant()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetImplant");
    }

    public DataSet GetClientName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetClientList");
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetSalesClientList");
    }
    public DataSet GetCityName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetCityForSalesCorporate");
    }
   
    public DataSet GetCompetitor()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetComCompetitor");
    }
    public DataSet CheckLoginValidate(string loginId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@LoginId", loginId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "CheckLoginIdExist", ObjparamUser);
    }

    public DataSet GetAccountType(int ClientCoId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSaleAccountType", ObjparamUser);
    }

    public DataSet GetSaleHeadQuarter(int ClientCoId,int AccountTypeId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        ObjparamUser[1] = new SqlParameter("@AccountTypeId", AccountTypeId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSaleHeadQuarter", ObjparamUser);
    }
    public DataSet GetSaleHeadQuarter(int ClientCoId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSaleHeadQuarter", ObjparamUser);
    }
    public DataSet GetSingedBy(int ClientCoId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSalesCorportateSingedBy", ObjparamUser);
    }
    public DataSet GetLocalInfluencer(int ClientCoId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetLocalInfluencer", ObjparamUser);
    }
    public DataSet GetCompetitorListClientBasis(int ClientCoId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSalesImplantCompetitor", ObjparamUser);
    }
    public DataSet GetImplantsClientBasis(int ClientCoId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSalesImplant", ObjparamUser);
    }

    public int SaveCorintSalesCorporate(SaleModule objSales)
    {
        int status = 0; 
        try
        {
            SqlParameter[] ObjparamUser = new SqlParameter[14];
            ObjparamUser[0] = new SqlParameter("@ClientCoID", objSales.ClientCoId);
            ObjparamUser[1] = new SqlParameter("@AccountType", objSales.AccountType);
            ObjparamUser[2] = new SqlParameter("@HeadQuarter", objSales.HQ);
            ObjparamUser[3] = new SqlParameter("@SignedBy", objSales.SignedBy);
            ObjparamUser[4] = new SqlParameter("@LocalInfluencer", objSales.LocalInfluencer);
            ObjparamUser[5] = new SqlParameter("@MonthlyPotential", objSales.MonthlyPotential);
            ObjparamUser[6] = new SqlParameter("@LocalAccountMgr", objSales.LocalAccountMgr);
            ObjparamUser[7] = new SqlParameter("@CreatedBy", objSales.SysUserID);
            ObjparamUser[8] = new SqlParameter("@LocalInfluencerName", objSales.LocalInfluencerName);
            ObjparamUser[9] = new SqlParameter("@LocalInfluencerMobileNo", objSales.LocalInfluencerMobileNo);
            ObjparamUser[10] = new SqlParameter("@ImplantId", objSales.ImplantId);
            ObjparamUser[11] = new SqlParameter("@Competitor", objSales.Competitor);
            ObjparamUser[12] = new SqlParameter("@ImplantSysUserId", objSales.ImplantSysUserId);
            ObjparamUser[13] = new SqlParameter("@NewImpantName", objSales.NewImplantName);
            object k = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Sp_CorintSalesCorporate_New", ObjparamUser);
            status =Convert.ToInt32(k);
            //return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveProspects_New", ObjparamUser);
            
        }
        catch (Exception)
        {
            status = 0; 
        }
        
        return status;
    }


    public DataSet EntryDetails(int ClientCoId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@ClientCoID", ClientCoId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSalesCorporateDetails", ObjparamUser);
    }
}
