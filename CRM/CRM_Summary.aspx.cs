using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CRM_CRM_Summary : System.Web.UI.Page
{
    protected void Init_Month()
    {
        ddlMonth.DataBind();
        int month = DateTime.Now.Month;
        ddlMonth.Items.FindByValue(month.ToString()).Selected = true;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Page.IsPostBack == false)
        {
            Init_Month();
            gvDaily.DataBind();
            gvMonthly.DataBind();
            gvTillDate.DataBind();
            gvOpen.DataBind();
        }
        Page.Title = "Insta : CRM";
    }
    protected void gvOpen_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Now Want to open pop up window from hyper link
            string l1 = ((Label)e.Row.FindControl("l1")).Text;
            string l2 = ((Label)e.Row.FindControl("l2")).Text;
            string l3 = ((Label)e.Row.FindControl("l3")).Text;
            string l4 = ((Label)e.Row.FindControl("l4")).Text;

            string Mth = ddlMonth.SelectedValue;
            string NavigateUrl = "CRM_LevelSummary.aspx?mid=" + Mth;

            string Url1 = NavigateUrl + "&l=1";
            string Url2 = NavigateUrl + "&l=2";
            string Url3 = NavigateUrl + "&l=3";
            string Url4 = NavigateUrl + "&l=4";

            string Title = "','CRM Summary','width=600,height=200')";

            string window1 = "window.open('" + Url1 + Title;
            string window2 = "window.open('" + Url2 + Title;
            string window3 = "window.open('" + Url3 + Title;
            string window4 = "window.open('" + Url4 + Title;


            e.Row.Cells[0].Attributes.Add("OnClick", "window.open('" + Url1 + "','BPCMS','width=750,height=200')");
            e.Row.Cells[1].Attributes.Add("OnClick", "window.open('" + Url2 + "','BPCMS','width=750,height=200')");
            e.Row.Cells[2].Attributes.Add("OnClick", "window.open('" + Url3 + "','BPCMS','width=750,height=200')");
            e.Row.Cells[3].Attributes.Add("OnClick", "window.open('" + Url4 + "','BPCMS','width=750,height=200')");

            //Ends Here

        }
    }
}
