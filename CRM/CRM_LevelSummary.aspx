<%@ Page Language="C#" AutoEventWireup="true" Theme="TMSTheme"  CodeFile="CRM_LevelSummary.aspx.cs" Inherits="CRM_CRM_LevelSummary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>EasyCabs : </title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="font-size: 9pt; font-family: Tahoma">
    <asp:GridView ID="gvLevel" runat="server" DataSourceID="ODS_For_gvLevel" Width="100%">
        </asp:GridView>
        <asp:ObjectDataSource ID="ODS_For_gvLevel" runat="server" SelectMethod="List_CRMSummary_More"
            TypeName="CRM">
            <SelectParameters>
                <asp:Parameter Name="BranchID" Type="Int32" />
                <asp:Parameter DefaultValue="" Name="MonthID" Type="Int32" />
                <asp:Parameter DefaultValue="" Name="Level" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
        
    </form>
</body>
</html>
