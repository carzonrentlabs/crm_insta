<%@ Page Language="C#" MasterPageFile="~/CRDMasterpage.master" Theme="TMSTheme" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="RegisterComplaintsD.aspx.cs" Inherits="CRM_RegisterComplaintsD"
    Title="CRM Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Page" runat="Server">

    <script type="text/jscript" src="../CalendarControl.js" language="javascript"></script>

    <script type="text/jscript" src="../JScript_DisableButton.js"></script>

    <script type="text/javascript" src="../JScript_ModalPopUp.js"></script>

    <asp:Label ID="Label1" SkinID="HeadingTop" runat="server" Text="Register Complaints"></asp:Label><br />
    <div>
        <table class="TmsClassic" width="100%">
            <tr>
                <td valign="top">
                    <table class="TmsClassic_NoBorder_Back" width="100%">
                        <tr>
                            <td>
                                <table class="TmsClassic_NoBorder" width="95%" style="text-align: left">
                                    <tr>
                                        <td align="right" colspan="8" style="font-size: 9pt; font-family: Tahoma" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td style="width: 50%">
                                                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="Maroon"
                                                            Text="CRM for :"></asp:Label></td>
                                                    <td align="left" colspan="4" valign="middle" style="width: 50%; text-align: left;">
                                                        <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" 
                                                           OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                       <%-- <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" DataSourceID="ODS_For_ddlLocation"
                                                            DataTextField="BranchName" DataValueField="BranchID" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged">
                                                        </asp:DropDownList>--%>
                                                        <%--<asp:ObjectDataSource ID="ODS_For_ddlLocation" runat="server" SelectMethod="ListCrmLocations"
                                                            TypeName="Systems.CRM.CRM_New">
                                                            <SelectParameters>
                                                                <asp:SessionParameter Name="UserID" SessionField="User" Type="String" />
                                                            </SelectParameters>
                                                        </asp:ObjectDataSource>--%>
                                                    </td>
                                                    <td>
                                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://insta.carzonrent.com"
                                                            Width="70px">Go To Insta</asp:HyperLink></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Search Complaint By :</td>
                                                    <td style="width: 20%" align="left">
                                                        <asp:RadioButtonList ID="rbblSearch" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Selected="True" Value="0">ComplaintID</asp:ListItem>
                                                            <asp:ListItem Value="1">Mobile</asp:ListItem>
                                                            <asp:ListItem Value="2">JobNo</asp:ListItem>
                                                            <asp:ListItem Value="3">CabID</asp:ListItem>
                                                        </asp:RadioButtonList></td>
                                                    <td style="width: 5%" align="left">
                                                        <asp:TextBox ID="txtCID" runat="server" Width="53px"></asp:TextBox></td>
                                                    <td style="width: 5%" align="left">
                                                        <asp:DropDownList ID="ddlcomplaintStatus" runat="server" OnSelectedIndexChanged="ddlcomplaintStatus_SelectedIndexChanged">
                                                            <asp:ListItem Value="1">Open</asp:ListItem>
                                                            <asp:ListItem Value="11">Closed</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td align="left">
                                                        <asp:Button ID="btnShowComplaints" runat="server" Text="complaints" Width="81px"
                                                            ForeColor="Maroon" OnClick="btnShowComplaints_Click" /></td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" colspan="1" style="font-size: 9pt; font-family: Tahoma" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="8" style="font-size: 7pt; font-family: Tahoma; height: 205px;" valign="top">
                                            <asp:GridView ID="gvComplaints" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2"
                                                DataKeyNames="ComplaintID" DataSourceID="DataSource_For_gvComplaints" ForeColor="Black"
                                                GridLines="None" OnSelectedIndexChanged="gvComplaints_SelectedIndexChanged" PageSize="5"
                                                Width="100%" EmptyDataText="No Record Found For Open Complaints ">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sl.No">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text="<%# Container.DataItemIndex+1 %>"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="ComplaintID" HeaderText="ComplaintID" />
                                                    <asp:BoundField DataField="Location" HeaderText="Location" />
                                                    <asp:BoundField DataField="JobID" HeaderText="JobID" />
                                                    <asp:BoundField DataField="CustomerName" HeaderText="CustomerName" />
                                                    <asp:BoundField DataField="CabID" HeaderText="CabID" />
                                                    <asp:BoundField DataField="ChauffeurName" HeaderText="Chauff/Comp Name" />
                                                    <asp:BoundField DataField="RegisteredAt" HeaderText="RegisteredAt" />
                                                    <asp:BoundField DataField="RegisteredBy" HeaderText="RegisteredBy">
                                                        <ItemStyle Font-Bold="True" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ResolutionAt" HeaderText="ResolutionAt" />
                                                    <asp:BoundField DataField="Days" HeaderText="T.Comp" />
                                                    <asp:BoundField DataField="Status" HeaderText="Status">
                                                        <ItemStyle Font-Bold="True" />
                                                    </asp:BoundField>
                                                    <asp:CommandField SelectText="Details" ShowSelectButton="True" />
                                                </Columns>
                                                <FooterStyle BackColor="Tan" />
                                                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="Tan" ForeColor="GhostWhite" />
                                                <HeaderStyle BackColor="Tan" Font-Bold="True" />
                                                <AlternatingRowStyle BackColor="PaleGoldenrod" />
                                            </asp:GridView>
                                            <asp:ObjectDataSource ID="DataSource_For_gvComplaints" runat="server" SelectMethod="ShowAllOpenComplaints"
                                                TypeName="CRM">
                                                <SelectParameters>
                                                    <asp:Parameter DefaultValue="0" Name="BranchID" Type="Int32" />
                                                    <asp:Parameter DefaultValue="0" Name="Status" Type="Int32" />
                                                    <asp:Parameter DefaultValue="0" Name="ComplaintID" Type="String" />
                                                    <asp:Parameter DefaultValue="0" Name="Type" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                        </td>
                                        <td align="left" colspan="1" style="font-size: 7pt; font-family: Tahoma; height: 205px;" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="TmsClassic_LeftH" align="left">
                                            Job Details :</td>
                                        <td align="left" class="TmsClassic_LeftH" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%; border-right: silver 1px solid; border-top: silver 1px solid;
                                            font-weight: bold; font-size: 8pt; border-left: silver 1px solid; border-bottom: silver 1px solid;
                                            font-family: Tahoma; height: 20px;" align="center" colspan="8">
                                            <asp:Label ID="lblEscInfo" runat="server" BackColor="Tan"></asp:Label></td>
                                        <td align="center" colspan="1" style="border-right: silver 1px solid; border-top: silver 1px solid;
                                            font-weight: bold; font-size: 8pt; border-left: silver 1px solid; width: 100%;
                                            border-bottom: silver 1px solid; font-family: Tahoma; height: 20px;">
                                        </td>
                                    </tr>
                                    <%--<tr>
                                        <td colspan="5" style="border-right: silver 2px solid; border-top: silver 2px solid;
                                            border-left: silver 2px solid; width: 50%; border-bottom: silver 2px solid; text-align: center">
                                        </td>
                                        <td colspan="3" style="font-size: 7pt; font-family: Tahoma">
                                            [CA=Cab,CH=Chauffeur,CC=Call Center,CM=Common]</td>
                                        <td colspan="1" style="font-size: 7pt; font-family: Tahoma">
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td style="text-align: left; width: 24%; height: 24px;">
                                            Complaint Type</td>
                                        <td style="text-align: center; width: 1px; height: 24px;">
                                            :</td>
                                        <td colspan="1" style="height: 24px">
                                            <asp:DropDownList ID="ddlPriority" runat="server" OnSelectedIndexChanged="ddlPriority_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </asp:DropDownList><asp:RequiredFieldValidator ID="RFV_ddlPriority" runat="server"
                                                ControlToValidate="ddlPriority" Display="Dynamic" ErrorMessage=" Priority " InitialValue="0"
                                                SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td style="text-align: left; width: 20%; height: 24px;">
                                            Complaint Category</td>
                                        <td style="text-align: center; width: 1px; height: 24px;">
                                            :</td>
                                        <td rowspan="1" valign="top" style="height: 24px">
                                            <asp:DropDownList ID="ddlResolution" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RFV_ddlResolution" runat="server" ControlToValidate="ddlResolution"
                                                Display="Dynamic" ErrorMessage="<b>At which level Resolution is Required ?</b> <div> you must select resolution level </div>"
                                                InitialValue="0" SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td colspan="1" style="height: 24px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; width: 24%; height: 31px;">
                                            Complaint Sub Type
                                        </td>
                                        <td style="text-align: center; width: 1px; height: 31px;">
                                            :</td>
                                        <td colspan="1" style="height: 31px">
                                            <asp:DropDownList ID="ddlComplaintSubCategory" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfVComplaintSubCategory" runat="server" ControlToValidate="ddlComplaintSubCategory"
                                                Display="Dynamic" ErrorMessage=" Priority " InitialValue="0" SetFocusOnError="True"
                                                ValidationGroup="r">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td style="text-align: left; width: 20%; height: 22px;">
                                            Guest EmailId</td>
                                        <td style="text-align: center; width: 1px; height: 22px;">
                                            :</td>
                                        <td valign="top">
                                            <asp:TextBox ID="txtEmailId" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            Job/Booking ID</td>
                                        <td style="text-align: center; width: 1px;" align="center">
                                            :</td>
                                        <td>
                                            <asp:TextBox ID="txtJobID" runat="server" AutoPostBack="false" OnTextChanged="txtJobID_TextChanged"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RFV_txtJobID" runat="server" ControlToValidate="txtJobID"
                                                ErrorMessage=" Job ID" ValidationGroup="r">*</asp:RequiredFieldValidator>&nbsp;&nbsp;
                                        </td>
                                        <td style="text-align: left; width: 20%;">
                                            Contact no.</td>
                                        <td style="text-align: center; width: 1px;">
                                            :</td>
                                        <td style="white-space: nowrap;">
                                            <asp:TextBox ID="txtCustContact" runat="server" Width="107px"></asp:TextBox>
                                            &nbsp;<%--<asp:RequiredFieldValidator    ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCustContact" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="search">*</asp:RequiredFieldValidator>--%><asp:Button
                                                ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" Width="57px"
                                                OnClick="btnSearch_Click" /></td>
                                        <td style="font-size: 7pt; font-family: Tahoma" colspan="2" rowspan="8" valign="top">
                                            <asp:GridView ID="gvSearch" runat="server" Width="100%" AutoGenerateColumns="False"
                                                BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" Caption="search results"
                                                CellPadding="2" DataKeyNames="JobID" ForeColor="Black" GridLines="None" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged"
                                                AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gvSearch_PageIndexChanging"
                                                PageSize="8">
                                                <Columns>
                                                    <asp:BoundField DataField="JobID" HeaderText="JobID" />
                                                    <asp:BoundField DataField="CabID" HeaderText="CabID" />
                                                    <asp:BoundField DataField="JobDate" HeaderText="JobDate" DataFormatString="{0:dd/MM/yy}" />
                                                    <asp:CommandField SelectText="Details" ShowSelectButton="True" />
                                                </Columns>
                                                <FooterStyle BackColor="Tan" />
                                                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="Tan" ForeColor="GhostWhite" />
                                                <HeaderStyle BackColor="Tan" Font-Bold="True" />
                                                <AlternatingRowStyle BackColor="PaleGoldenrod" />
                                            </asp:GridView>
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                            &nbsp; &nbsp; &nbsp; &nbsp;
                                        </td>
                                        <td colspan="1" rowspan="8" style="font-size: 7pt; font-family: Tahoma" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblGuestH" runat="server" Text="Customer Name"></asp:Label></td>
                                        <td align="center" style="text-align: center; width: 1px;">
                                            :</td>
                                        <td>
                                            <asp:TextBox ID="txtCustName" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RFV_txtCustName" runat="server" ControlToValidate="txtCustName"
                                                Display="Dynamic" ErrorMessage="<b>What is  Customer Name ?</b> <div>For registering this complaint you must enter customer Name.</div>"
                                                SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator></td>
                                        <td style="width: 20%; text-align: left">
                                            Job Date</td>
                                        <td style="text-align: center; width: 1px;">
                                            :</td>
                                        <td>
                                            <asp:TextBox ID="txtJobDate" onfocus="showCalendarControl(this);" runat="server"
                                                MaxLength="10"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RFV_txtJobDate" runat="server" ControlToValidate="txtJobDate"
                                                ErrorMessage="Job Date " SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="Label3" runat="server" Text="Job Type"></asp:Label></td>
                                        <td align="center" style="width: 1px; text-align: center">
                                            :</td>
                                        <td rowspan="1" valign="top">
                                            <asp:Label ID="lblJobType" runat="server" Font-Bold="True"></asp:Label></td>
                                        <td style="width: 22%; text-align: left">
                                        </td>
                                        <td style="width: 1px; text-align: center">
                                        </td>
                                        <td style="width: 12%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblFrom" runat="server" Text="Travel From"></asp:Label>&nbsp;
                                        </td>
                                        <td align="center" style="width: 1px; text-align: center">
                                            :</td>
                                        <td rowspan="3" valign="top">
                                            <asp:TextBox ID="txtJobFrom" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox></td>
                                        <td style="text-align: left; width: 22%;">
                                            <asp:Label ID="lblTo" runat="server" Text="To"></asp:Label></td>
                                        <td style="text-align: center; width: 1px;">
                                            :</td>
                                        <td style="width: 12%">
                                            <asp:TextBox ID="txtJobTo" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                        </td>
                                        <td align="center" style="width: 1px; text-align: center">
                                        </td>
                                        <td style="width: 22%; text-align: left">
                                            <asp:Label ID="lblInvoiceH" runat="server" ForeColor="Maroon"></asp:Label></td>
                                        <td style="width: 1px; text-align: center">
                                        </td>
                                        <td style="width: 12%">
                                            <asp:Label ID="lblInvoice" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; height: 22px;">
                                        </td>
                                        <td align="center" style="height: 22px">
                                            &nbsp;
                                        </td>
                                        <td>
                                            Job Location</td>
                                        <td align="center" style="text-align: center; width: 1px; height: 31px;">
                                            :</td>
                                        <td style="height: 31px">
                                            <asp:DropDownList ID="ddlJobLocation" runat="server" AutoPostBack="false" Enabled="false"
                                                OnSelectedIndexChanged="ddlJobLocation_SelectedIndexChanged">
                                            </asp:DropDownList><asp:RequiredFieldValidator ID="RFV_ddlJobLocation" runat="server"
                                                ControlToValidate="ddlJobLocation" Display="Dynamic" ErrorMessage="Job Location "
                                                InitialValue="-1" SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 22px; text-align: left">
                                            Car Category
                                        </td>
                                        <td align="center" style="height: 22px; text-align: left">
                                            :</td>
                                        <td style="width: 20%; height: 22px; text-align: left">
                                            <asp:Label ID="lbl_CarCat" runat="server" Font-Bold="True"></asp:Label></td>
                                        <td style="width: 1px; height: 22px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="TmsClassic_LeftH" colspan="8" style="text-align: left; height: 25px;">
                                            Cab Details :</td>
                                        <td class="TmsClassic_LeftH" colspan="1" style="text-align: left; height: 25px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="text-align: left">
                                            <table width="100%">
                                                <tr>
                                                    <td style="width: 409px" align="left">
                                                        Cab Number :
                                                        <asp:TextBox ID="txtCabNo" runat ="server" ></asp:TextBox>
                                                        <asp:DropDownList ID="ddlCabNo" runat="server" Visible="false"  AutoPostBack="True" OnSelectedIndexChanged="ddlCabNo_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <%-- <asp:RequiredFieldValidator ID="RFV_ddlCabNo" runat="server" ControlToValidate="ddlCabNo"
                        ErrorMessage=" Cab No"
                        InitialValue="0" SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator>--%>
                                                        <asp:Label ID="lblCabID" runat="server"></asp:Label></td>
                                                    <td>
                                                        &nbsp;<asp:Label ID="lblChauffeurNameH" runat="server" Text="Chauffeur Name :">
                                                        </asp:Label>
                                                        <asp:TextBox ID="txtChauffName" runat="server"></asp:TextBox>
                                                        <%-- <asp:RequiredFieldValidator ID="RFV_txtChauffName" runat="server" ControlToValidate="txtChauffName"
                            ErrorMessage="Chauffeur Name"
                            SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator>--%>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblContactH" runat="server" Text="Contact No."></asp:Label>
                                                        <asp:TextBox ID="txtChauffeurContact" runat="server" Width="129px"></asp:TextBox>
                                                        <%-- <asp:RequiredFieldValidator ID="RFV_txtChauffeurContact" runat="server" ControlToValidate="txtChauffeurContact"
                            ErrorMessage="Contact No. "
                            SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender_Chauff" runat="server" FilterType="Numbers"
                                                TargetControlID="txtChauffeurContact">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td colspan="1" style="text-align: left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="TmsClassic_LeftH" colspan="8" style="height: 25px">
                                            Complaint Details :</td>
                                        <td align="left" class="TmsClassic_LeftH" colspan="1" style="height: 25px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 22px">
                                            Complaint Source</td>
                                        <td style="text-align: center; width: 1px; height: 22px;">
                                            :</td>
                                        <td style="height: 22px">
                                            <asp:DropDownList ID="ddlCompSrc" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RFV_ddlCompSrc" runat="server" ControlToValidate="ddlCompSrc"
                                                ErrorMessage="Complaint Source" InitialValue="0" SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator>&nbsp;
                                        </td>
                                        <td style="width: 20%; height: 22px;">
                                        </td>
                                        <td style="text-align: center; width: 1px; height: 22px;">
                                        </td>
                                        <td colspan="3" style="height: 22px">
                                            &nbsp;
                                        </td>
                                        <td colspan="1" style="height: 22px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Description/Remarks</td>
                                        <td style="text-align: center; width: 1px;">
                                            :</td>
                                        <td colspan="6" rowspan="2">
                                            <asp:TextBox ID="txtCompDesc" runat="server" Columns="63" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RFV_txtCompDesc" runat="server" ControlToValidate="txtCompDesc"
                                                ErrorMessage="Complaint Detail" SetFocusOnError="True" ValidationGroup="r">*</asp:RequiredFieldValidator>
                                            &nbsp;&nbsp; &nbsp;
                                        </td>
                                        <td colspan="1" rowspan="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="text-align: center; width: 1px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" align="center" class="Messages">
                                            <asp:Literal ID="ltrlMsg" runat="server"></asp:Literal>
                                            <asp:Literal ID="ltrlErr" runat="server"></asp:Literal></td>
                                        <td align="center" class="Messages" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%">
                                            <!--Cust. contact details</td>-->
                                            <td rowspan="2" style="text-align: center; width: 1px;">
                                                <!--:-->
                                            </td>
                                            <td colspan="6" rowspan="2">
                                                <!-- <asp:TextBox ID="txtCPDetails" runat="server" Columns="53" TextMode="MultiLine"></asp:TextBox>-->
                                                <input type="button" id="btnhide" class="button" style="display: none;" disabled="disabled"
                                                    value="Save" />
                                                <asp:Button ID="btnRegister" runat="server" Text="Register" Width="110px" ValidationGroup="r"
                                                    OnClientClick="return CheckDetails(this);" OnClick="btnRegister_Click" />
                                                <asp:Button ID="btnNew" runat="server" OnClick="btnNew_Click" Text="Register New"
                                                    Width="106px" /></td>
                                            <td colspan="1" rowspan="2">
                                            </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="8" class="TmsClassic_LeftH" style="height: 18px">
                                            Concerned Person For Resolutions :</td>
                                        <td align="left" class="TmsClassic_LeftH" colspan="1" style="height: 18px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 60px">
                                            Resolution :</td>
                                        <%--<td style="text-align: center; width: 1px;">
                                            :</td>--%>
                                        <td colspan="1" style="height: 60px">
                                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Columns="63" ValidationGroup="update"
                                                Rows="4"></asp:TextBox>&nbsp;
                                            </td>
                                            <td align="right" style="height: 60px">Was Chauffeur Fault :</td>
                                            <td align="left" style="height: 60px">
                                            <asp:DropDownList ID="ddlChauffeurFault" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlChauffeurFault_SelectedIndexChanged">
                                                                                       
                                            <asp:ListItem Value="NA">Select Is Fault</asp:ListItem>
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                            </asp:DropDownList>
                                            </td>
                                                                                  
                                            <td align="right" style="height: 60px" id="tdPenality" runat="server" visible="false">Penalty Reason:</td>
                                            <td align="left" style="height: 60px" id="tdlstPenality" runat="server" visible="false">
                                             <asp:ListBox ID="lstPenality" runat="server" 
                                CssClass="black_normal_txt" Height="125px" Width="144px" 
                                    SelectionMode="Multiple" ></asp:ListBox>
                                            
                                            </td>
                                             
                                             
                                            <td align="right" style="height: 60px">Chauffeur at fault :</td>
                                            <td align="left" style="height: 60px">
                                            <asp:DropDownList ID="ddlChauffeurAllocated" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rvChauffeurAtFault" runat="server" ControlToValidate="ddlChauffeurAllocated"
                                                ErrorMessage="Chauffeur at fault" InitialValue="0" SetFocusOnError="True" ValidationGroup="UC">*</asp:RequiredFieldValidator>
                                            
                                            </td>
                                       
                                    </tr>
                                    <tr>
                                        <td>Complaint Category :</td>
                                        <td>
                                        <asp:DropDownList ID="ddlComplaintResolutionCat" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlComplaintResolutionCat_SelectedIndexChanged">
                                        <asp:ListItem Value="0">Select Complaint</asp:ListItem>
                                        <asp:ListItem Value="1">High Risk</asp:ListItem>
                                        <asp:ListItem Value="2">Medium Risk</asp:ListItem>
                                        <asp:ListItem Value="3">Low Risk</asp:ListItem>
                                        <asp:ListItem Value="4">Appreciation</asp:ListItem>
                                        </asp:DropDownList>
                                       <%-- <asp:RequiredFieldValidator ID="rvCompResCat" runat="server" ControlToValidate="ddlComplaintResolutionCat"
                                                ErrorMessage="Select Complaint Category" InitialValue="0" SetFocusOnError="True" ValidationGroup="UC">*</asp:RequiredFieldValidator>--%>
                                        </td>
                                        
                                        <td align="right">Complaint Sub Category :</td>
                                        <td align="left">
                                        <asp:DropDownList ID="ddlComplaintResolutionSubCat" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rvCompResSubCat" runat="server" ControlToValidate="ddlComplaintResolutionSubCat"
                                                ErrorMessage="Complaint Sub Category" InitialValue="0" SetFocusOnError="True" ValidationGroup="UC">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td colspan="2" align="right">Remarks(If Any) :</td>
                                        <td><asp:TextBox ID="txtRemarksIfAny" runat="server" TextMode="MultiLine" Height="50px" Width="250px"/></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                    <td colspan="6">
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" Width="80px" ValidationGroup="UC" OnClick="btnUpdate_Click" />
                                            &nbsp;
                                            <asp:Button ID="btnClose" OnClientClick="showConfirm(this); return false;" runat="server"
                                                Text="Closure" OnClick="btnClose_Click" Width="80px" ValidationGroup="UC"/>
                                            <asp:Button ID="btnReOpen" runat="server" OnClick="btnReOpen_Click" Text="Re-Open"
                                                Visible="False" Width="80px" />
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="text-align: center; width: 1px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="color: #0066cc" align="center">
                                        </td>
                                        <td align="center" colspan="1" style="color: #0066cc">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="height: 235px">
                                            <asp:GridView ID="gvResolutions" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow"
                                                BorderColor="Tan" BorderWidth="1px" CellPadding="2" DataSourceID="DataSource_For_gvResolutions"
                                                EmptyDataText="No Resolutions For This Complaint" ForeColor="Black" GridLines="None"
                                                Width="100%">
                                                <Columns>
                                                    <asp:BoundField DataField="ResolutionBy" HeaderText="Name" />
                                                    <asp:BoundField DataField="Reg_DateTime" HeaderText="Date" />
                                                    <asp:TemplateField HeaderText="Resolution">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Resolution") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TextBox2" runat="server" Columns="80" ReadOnly="True" Text='<%# Bind("Resolution") %>'
                                                                TextMode="MultiLine"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="Tan" />
                                                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                                                <HeaderStyle BackColor="Tan" Font-Bold="True" />
                                                <AlternatingRowStyle BackColor="PaleGoldenrod" />
                                            </asp:GridView>
                                            <asp:ObjectDataSource ID="DataSource_For_gvResolutions" runat="server" SelectMethod="ListResolutionsForComplaint"
                                                TypeName="CRM">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="gvComplaints" DefaultValue="0" Name="ComplaintID"
                                                        PropertyName="SelectedValue" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                        </td>
                                        <td colspan="1" style="height: 235px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8">
                                            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BehaviorID="mdlPopup"
                                                TargetControlID="div" PopupControlID="div" OkControlID="btnOk" OnOkScript="okClick();"
                                                CancelControlID="btnNo" OnCancelScript="cancelClick();" BackgroundCssClass="modalBackground">
                                            </cc1:ModalPopupExtender>
                                            <div style="display: none" id="div" class="confirm" align="center" runat="server">
                                                <img src="../App_Images/logo_carzonret.gif" align="absMiddle" />
                                                <br />
                                                <br />
                                                <b style="font-family: Tahoma">Are you sure you want to close this complaint ?</b>
                                                <br />
                                                <br />
                                                <asp:Button ID="btnOk" runat="server" Width="50px" Text="Yes"></asp:Button>
                                                <asp:Button ID="btnNo" runat="server" Width="50px" Text="No"></asp:Button>
                                            </div>
                                        </td>
                                        <td colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="text-align: left">
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td style="text-align: left">
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
