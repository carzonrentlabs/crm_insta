using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Insta;

public partial class ProspectMaster : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.bntExprot);

        objCordrive = new CorDrive();

        if (!IsPostBack)
        {
            BindCity();
            BindUserName();
            Reset();
        }
    }

    private void BindUserName()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dtuserNmae = new DataSet();
            dtuserNmae = objCordrive.GetUserName(Convert.ToInt32(Session["UserID"]));
            if (dtuserNmae.Tables[0].Rows.Count > 0)
            {
                lblloginBy.Text = "Logged By : " + dtuserNmae.Tables[0].Rows[0]["Username"].ToString();
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }

    private void BindCity()
    { 
        try
        {
            objCordrive = new CorDrive();
            DataSet dsCustName = new DataSet();
            dsCustName = objCordrive.GetCityName();
            if (dsCustName.Tables[0].Rows.Count > 0)
            {
                ddlcityName.DataTextField = "cityname";
                ddlcityName.DataValueField = "cityid";
                ddlcityName.DataSource = dsCustName;
                ddlcityName.DataBind();
                ddlcityName.Items.Insert(0, "--Select City Name--");
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            int rslt = 0;
            double Potentail = 0;
            int result;
            if (int.TryParse(txtPotentialAmt.Text, out result))
            {
                try
                {
                    Potentail = Convert.ToDouble(txtPotentialAmt.Text);
                }
                catch (Exception ee)
                {
                    // Nothing
                }
            }

            rslt = objCordrive.SaveProspect(txtCustomerName.Text, Convert.ToInt32(ddlcityName.SelectedItem.Value), Potentail
                , txtContactperson.Text, txtEmailID.Text, txtMobileNo.Text, Convert.ToInt32(Session["UserID"]), 1, Convert.ToInt32(hdnIDs.Value));
            if (rslt > 0)
            {
                lblMessage.Text = "Updated Successfully";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                ClearControl();
            }
            else
            {
                lblMessage.Text = "Not Updated Successfully";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }

            FillGrid();
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            int rslt = 0;

            double Potentail = 0;
            int result;
            if (int.TryParse(txtPotentialAmt.Text, out result))
            {
                try
                {
                    Potentail = Convert.ToDouble(txtPotentialAmt.Text);
                }
                catch (Exception ee)
                {
                    // Nothing
                }
            }

            rslt = objCordrive.SaveProspect(txtCustomerName.Text.Trim(), Convert.ToInt32(ddlcityName.SelectedItem.Value)
                , Potentail, txtContactperson.Text.Trim(), txtEmailID.Text.Trim(), txtMobileNo.Text.Trim(), Convert.ToInt32(Session["UserID"]), 0, 0);
            if (rslt > 0)
            {
                lblMessage.Text = "Submitted Successfully";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                ClearControl();
            }
            else if (rslt == -1)
            {
                lblMessage.Text = "Record already Present with same Customer Name, Mobile and EmailID.";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                lblMessage.Text = "Not Submitted Successfully";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }

    public void ClearControl()
    {
        txtCustomerName.Text = string.Empty;
        ddlcityName.SelectedIndex = 0;
        txtPotentialAmt.Text = string.Empty;
        txtContactperson.Text = string.Empty;
        txtEmailID.Text = string.Empty;
        txtMobileNo.Text = string.Empty;
    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            string colorSet3 = string.Empty;

            DataSet _objDs = new DataSet();
            _objDs = objCordrive.GetProspectsReport(Convert.ToDateTime(FromDate.Text), Convert.ToDateTime(ToDate.Text));
            if (_objDs != null)
            {
                if (_objDs.Tables.Count > 0)
                {
                    dt = _objDs.Tables[0];
                }
            }
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    Response.ClearContent();
                    Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                    Response.Clear();
                    Response.AppendHeader("content-disposition", "attachment;filename=ProspectList.xls");
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    this.EnableViewState = false;
                    Response.Write("\r\n");
                    Response.Write("<table border = '1' align = 'center'> ");
                    int[] iColumns = { 1, 3, 4, 5, 6, 7, 9, 11, 10 };
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        if (i == 0)
                        {
                            Response.Write("<tr>");
                            for (int j = 0; j < iColumns.Length; j++)
                            {
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "ClientCoName")
                                {
                                    Response.Write("<td align='left'><b>Client Name</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "cityName")
                                {
                                    Response.Write("<td align='left'><b>City Name</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "PotentialAmout")
                                {
                                    Response.Write("<td align='left'><b>Potential Amout</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "ContactPersonName")
                                {
                                    Response.Write("<td align='left'><b>Contact Person Name</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "EmailID")
                                {
                                    Response.Write("<td align='left'><b>EmailID</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "Phone")
                                {
                                    Response.Write("<td align='left'><b>Phone</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "CreateDate")
                                {
                                    Response.Write("<td align='left'><b>Create Date</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "CreatedByName")
                                {
                                    Response.Write("<td align='left'><b>Created By</b></td>");
                                }
                            }
                            Response.Write("</tr>");
                        }

                        for (int j = 0; j < iColumns.Length; j++)
                        {
                            Response.Write("<td align='left'>" + dt.Rows[i][iColumns[j]].ToString() + "</td>");
                        }
                        Response.Write("</tr>");
                    }
                    Response.Write("</table>");
                    Response.End();
                }
                else
                {
                    Response.Write("<table border = 1 align = 'center' width = '100%'>");
                    Response.Write("<td align='center'><b>No Record Found</b></td>");
                    Response.Write("</table>");
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    void FillGrid()
    {
        DataSet GetBatchDetail = new DataSet();
        GetBatchDetail = objCordrive.GetProspectsReport(Convert.ToDateTime(FromDate.Text), Convert.ToDateTime(ToDate.Text));

        gdprospect.DataSource = GetBatchDetail.Tables[0];
        gdprospect.DataBind();
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        FillGrid();
    }
    protected void gdprospect_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow row = (GridViewRow)gdprospect.Rows[e.RowIndex];
        HiddenField hdnid = (HiddenField)row.FindControl("hdnid");

        int id = 0;
        id = objCordrive.DeactivateProspects(Convert.ToInt32(hdnid.Value), Convert.ToInt32(Session["UserID"]));
        if (id > 0)
        {
            lblMessage.Text = "Deactivated Successfully";
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            ClearControl();
        }
        else
        {
            lblMessage.Text = "Not Deactivated Successfully";
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
        FillGrid();
    }
    protected void gdprospect_RowEditing(object sender, GridViewEditEventArgs e)
    {
       gdprospect.EditIndex = e.NewEditIndex;
        FillGrid();
    }

    protected void gdprospect_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdprospect.PageIndex = e.NewPageIndex;
        FillGrid();
    }

    protected void gdprospect_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gdprospect.EditIndex = -1;
        FillGrid();
    }

    protected void gdprospect_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int id = Convert.ToInt32(gdprospect.DataKeys[e.RowIndex].Value.ToString());
        hdnIDs.Value = id.ToString();
        //GridViewRow row = (GridViewRow)gdprospect.Rows[e.RowIndex];
        DataSet ds = new DataSet();
        ds = objCordrive.GetProspectsDataByID(Convert.ToInt32(hdnIDs.Value));

        if (ds.Tables[0].Rows.Count > 0)
        {
            txtCustomerName.Text = ds.Tables[0].Rows[0]["ClientCoName"].ToString();
            ddlcityName.SelectedValue = ds.Tables[0].Rows[0]["CityID"].ToString();
            txtPotentialAmt.Text = ds.Tables[0].Rows[0]["PotentialAmout"].ToString();
            txtContactperson.Text = ds.Tables[0].Rows[0]["ContactPersonName"].ToString();
            txtEmailID.Text = ds.Tables[0].Rows[0]["EmailID"].ToString();
            txtMobileNo.Text = ds.Tables[0].Rows[0]["Phone"].ToString();
        }

        btnSubmit.Visible = false;
        btnUpdate.Visible = true;
    }

    public void Reset()
    {
        btnSubmit.Visible = true;
        btnUpdate.Visible = false;
    }
}