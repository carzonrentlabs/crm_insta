using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Sys;
namespace Systems
{
    /// <summary>
    /// Summary description for Finance
    /// </summary>
    public class Finance
    {
        public Finance()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #region BankMaster

        public void InsertBankDetails(int BranchId, String Name, String AccountId, String BankBranch,String Address,String ContactNo,String RM)
        {
            SqlParameter[] ObjParam = new SqlParameter[7];

            ObjParam[0] = new SqlParameter("@BranchId", SqlDbType.Int);
            ObjParam[0].Value = BranchId;

            ObjParam[1] = new SqlParameter("@Name", SqlDbType.VarChar);
            ObjParam[1].Value = Name;

            ObjParam[2] = new SqlParameter("@Account", SqlDbType.VarChar);
            ObjParam[2].Value = AccountId;

            ObjParam[3] = new SqlParameter("@Branch", SqlDbType.VarChar);
            ObjParam[3].Value = BankBranch;

            ObjParam[4] = new SqlParameter("@Address", SqlDbType.VarChar);
            ObjParam[4].Value = Address;

            ObjParam[5] = new SqlParameter("@ContactNumber", SqlDbType.VarChar);
            ObjParam[5].Value = ContactNo;

            ObjParam[6] = new SqlParameter("@RelationManager", SqlDbType.VarChar);
            ObjParam[6].Value = RM;

            Sys.Data.layer.DbConnect.ExecuteNonQuery("Bk_BankMaster_Insert", ObjParam);

        }

        public void UpdateBankDetails(int BranchId, String Name, String AccountId, String BankBranch, String Address, String ContactNo, String RM,int bankid)
        {
            SqlParameter[] ObjParam = new SqlParameter[8];

            ObjParam[0] = new SqlParameter("@BranchId", SqlDbType.Int);
            ObjParam[0].Value = BranchId;

            ObjParam[1] = new SqlParameter("@Name", SqlDbType.VarChar);
            ObjParam[1].Value = Name;

            ObjParam[2] = new SqlParameter("@Account", SqlDbType.VarChar);
            ObjParam[2].Value = AccountId;

            ObjParam[3] = new SqlParameter("@Branch", SqlDbType.VarChar);
            ObjParam[3].Value = BankBranch;

            ObjParam[4] = new SqlParameter("@Address", SqlDbType.VarChar);
            ObjParam[4].Value = Address;

            ObjParam[5] = new SqlParameter("@ContactNumber", SqlDbType.VarChar);
            ObjParam[5].Value = ContactNo;

            ObjParam[6] = new SqlParameter("@RelationManager", SqlDbType.VarChar);
            ObjParam[6].Value = RM;

            ObjParam[7] = new SqlParameter("@BankID", SqlDbType.Int);
            ObjParam[7].Value = bankid;

            Sys.Data.layer.DbConnect.ExecuteNonQuery("Bk_BankMaster_Update", ObjParam);

        }
        public SqlDataReader FillBankRecords()
        {
           return Sys.Data.layer.DbConnect.ExecuteReader("Bk_BankMaster_FillGrid");
        }
        public SqlDataReader MaxBatchId()
        {
            return Sys.Data.layer.DbConnect.ExecuteReader("Bk_SelectMaxBatch");
        }
        public DataSet GetBankRecords(int Bankid)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];

            ObjParam[0] = new SqlParameter("@BankId", SqlDbType.Int);
            ObjParam[0].Value = Bankid;
            return Sys.Data.layer.DbConnect.GetDataSet("Bk_BankMaster_Edit", ObjParam);
        }

        #endregion

        #region Bank_Deposit

        public SqlDataReader GetDepositRecord(String Branch)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];

            ObjParam[0] = new SqlParameter("@Branch", SqlDbType.VarChar);
            ObjParam[0].Value = Branch;

            return Sys.Data.layer.DbConnect.ExecuteReader("Bank_Deposit",ObjParam);
        }
        public SqlDataReader GetSecurityRecord(String Branch)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];

            ObjParam[0] = new SqlParameter("@Branch", SqlDbType.VarChar);
            ObjParam[0].Value = Branch;

            return Sys.Data.layer.DbConnect.ExecuteReader("Bk_SecurityDetail", ObjParam);
        }


        public SqlDataReader GetAccountNo(int BranchID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];

            ObjParam[0] = new SqlParameter("@BranchId", SqlDbType.Int);
            ObjParam[0].Value = BranchID;

            return Sys.Data.layer.DbConnect.ExecuteReader("Bk_GetAccountNo", ObjParam);
        }

        public SqlDataReader GetSubBranch(int BranchID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];

            ObjParam[0] = new SqlParameter("@BranchId", SqlDbType.Int);
            ObjParam[0].Value = BranchID;

            return Sys.Data.layer.DbConnect.ExecuteReader("GetSubBranchId", ObjParam);
        }
        public SqlDataReader GetReciept(int BatchID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];

            ObjParam[0] = new SqlParameter("@BatchID", SqlDbType.Int);
            ObjParam[0].Value = BatchID;

            return Sys.Data.layer.DbConnect.ExecuteReader("Bk_ChequeReciept", ObjParam);
        }
        public SqlDataReader ShowBatch(DateTime From, DateTime To)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];

            ObjParam[0] = new SqlParameter("@From", SqlDbType.DateTime);
            ObjParam[0].Value = From;

            ObjParam[1] = new SqlParameter("@To", SqlDbType.DateTime);
            ObjParam[1].Value = To;


            return Sys.Data.layer.DbConnect.ExecuteReader("Bk_ShowBatch_Priodical", ObjParam);
        }
        public void UpdateDepositedDate(String ChequeNo, int CabOwnerID)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];

            ObjParam[0] = new SqlParameter("@IdentifierId", SqlDbType.VarChar);
            ObjParam[0].Value = ChequeNo;

            ObjParam[1] = new SqlParameter("@CabOwnerId", SqlDbType.Int);
            ObjParam[1].Value = CabOwnerID;
            
            Sys.Data.layer.DbConnect.ExecuteNonQuery("Bk_Update_Depositeddate", ObjParam);

        }

        public void UpdateDepositedSecurity(String ChequeNo, int CabOwnerID)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];

            ObjParam[0] = new SqlParameter("@IdentifierId", SqlDbType.VarChar);
            ObjParam[0].Value = ChequeNo;

            ObjParam[1] = new SqlParameter("@CabOwnerId", SqlDbType.Int);
            ObjParam[1].Value = CabOwnerID;

            Sys.Data.layer.DbConnect.ExecuteNonQuery("Bk_Update_Depositeddate_Security", ObjParam);

        }

        public void InsertTransBatch(int SubbranchID, int BankID, Decimal Cash, int ChequeCount, Decimal ChequeAmount, String DepositedBy, int SecChequeCount,Decimal SecChequeAmt)
        {
            SqlParameter[] ObjParam = new SqlParameter[8];

            ObjParam[0] = new SqlParameter("@SubbranchID", SqlDbType.Int);
            ObjParam[0].Value = SubbranchID;

            ObjParam[1] = new SqlParameter("@BankID", SqlDbType.Int);
            ObjParam[1].Value = BankID;

            ObjParam[2] = new SqlParameter("@Cash", SqlDbType.Decimal);
            ObjParam[2].Value = Cash;

            ObjParam[3] = new SqlParameter("@ChequeCount", SqlDbType.Int);
            ObjParam[3].Value = ChequeCount;

            ObjParam[4] = new SqlParameter("@ChequeAmount", SqlDbType.Decimal);
            ObjParam[4].Value = ChequeAmount;

            //ObjParam[5] = new SqlParameter("@DepositDate", SqlDbType.DateTime);
            //ObjParam[5].Value = DepositDate;

            ObjParam[5] = new SqlParameter("@DepositedBy", SqlDbType.VarChar);
            ObjParam[5].Value = DepositedBy;

            ObjParam[6] = new SqlParameter("@SecChequeCount", SqlDbType.Int);
            ObjParam[6].Value = SecChequeCount;

            ObjParam[7] = new SqlParameter("@SecChequeAmount", SqlDbType.Decimal);
            ObjParam[7].Value = SecChequeAmt;
            Sys.Data.layer.DbConnect.ExecuteNonQuery("TrnschequeBatch_Insert", ObjParam);

        }

        public void InsertTransChequeDetails(String IdentifierID, DateTime IdentifierDate, String IdentifierBank,Decimal Amount,int OwnerID,Decimal SecAmount)
        {
            SqlParameter[] ObjParam = new SqlParameter[6];

            ObjParam[0] = new SqlParameter("@IdentifierID", SqlDbType.VarChar);
            ObjParam[0].Value = IdentifierID;

            ObjParam[1] = new SqlParameter("@IdentifierDate", SqlDbType.DateTime);
            ObjParam[1].Value = IdentifierDate;

            ObjParam[2] = new SqlParameter("@IdentifierBank", SqlDbType.VarChar);
            ObjParam[2].Value = IdentifierBank;
                        
            ObjParam[3] = new SqlParameter("@Amount", SqlDbType.Decimal);
            ObjParam[3].Value = Amount;

            ObjParam[4] = new SqlParameter("@CabOwnerID", SqlDbType.Int);
            ObjParam[4].Value = OwnerID;

            ObjParam[5] = new SqlParameter("@SecAmount", SqlDbType.Decimal);
            ObjParam[5].Value = SecAmount;

           

            Sys.Data.layer.DbConnect.ExecuteNonQuery("TrnschequeDetails_Insert", ObjParam);

        }
        #endregion

       
    }
}
