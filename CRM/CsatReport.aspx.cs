using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ExcelUtil;
public partial class CRM_CsatReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Page.IsPostBack == false)
        {
            Init_Dates();
            ddlBranch.DataBind();
            bind_report();
        }
    }

    private void Init_Dates()
    {
        txtFromDate.Text = DateTime.Now.ToString("MM-dd-yyyy");
        txtToDate.Text = DateTime.Now.ToString("MM-dd-yyyy");

        txtStartDate.Text = DateTime.Now.ToString("MM-dd-yyyy");
        txtEndDate.Text = DateTime.Now.ToString("MM-dd-yyyy");
    }
    protected void btnShow_Click(object sender, EventArgs e)
    {
        bind_report();
    }

    protected void bind_report()
    {
        string BranchID = "";
        if (ddlBranch.SelectedValue == "1")
            BranchID = Session["BranchID"].ToString();
        else
            BranchID = "21";

        ODS_For_gvReport.SelectParameters[0].DefaultValue = BranchID;
        ODS_For_gvReport.SelectParameters[1].DefaultValue = txtFromDate.Text.Trim();
        ODS_For_gvReport.SelectParameters[2].DefaultValue = txtToDate.Text.Trim();
        gvReport.DataBind();
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        string URL = string.Empty;
        string msg = string.Empty;
        // msg = ExportReportToExcel(out URL);      
        int BranchID = 0;
        if (ddlBranch.SelectedValue == "1")
            BranchID = int.Parse(Session["BranchID"].ToString());
        else
            BranchID = 21;
        DataSet ds = new DataSet();
        ds = Insta.CRM_Insta.CSAT_CSATSuggestionReport(BranchID, Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
        if (ds.Tables[0].Rows.Count > 0)
        {
            ExcelUtil.WorkbookEngine.ExportDataSetToExcel(ds, "C-SAT Reports.xls");
        }
        //ltrlMsg.Text = msg;
        //if (msg == "")
        //{
        //    Response.Redirect(URL);
        //}
    }

    //private DateTime IsValidDate(string Date)
    //{
    //    DateTime FormatedDate = DateTime.Now;
    //    try
    //    {
    //        FormatedDate = DateTime.Parse(Date);
    //    }
    //    catch
    //    {

    //    }
    //    return FormatedDate;
    //}

    //private string ExportReportToExcel(out string URL)
    //{
    //    DateTime startDate, endDate;

    //    startDate = IsValidDate(txtStartDate.Text);
    //    endDate = IsValidDate(txtEndDate.Text);

    //    string msg = string.Empty;
    //    string url = "";
    //    try
    //    {
    //        msg = "";

    //        int BranchID = 0;
    //        if (ddlBranch.SelectedValue == "1")
    //            BranchID = int.Parse(Session["BranchID"].ToString());
    //        else
    //            BranchID = 21;


    //        DutyMatrixToExcel objDME = new DutyMatrixToExcel();
    //        url = objDME.CSATSuggestionReportToExcel<string, DateTime, int>(Server.MapPath("~//CRM"), startDate, endDate, BranchID);


    //    }
    //    catch (Exception Err)
    //    {
    //        msg = Err.Message;
    //    }
    //    URL = url;
    //    return msg;
    //}
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlBranch.SelectedValue == "3")
        {
            Response.Redirect("LimoCsatReport.aspx");
        }
        else if (ddlBranch.SelectedValue == "4")
        {
            Response.Redirect("TripsCsatReport.aspx");
        }
        else
        {
            Response.Redirect("CsatReport.aspx");
        }
    }
}