using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class SalesCorporateModule : System.Web.UI.Page
{
    CorporateModule objCorModule = null;
    SaleModule objSale = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindClient();
            BindCityName(ddlHQ);
            BindCityName(ddlSigned);
            BindAccountManager();
            BindImplant();
            BindCompetitor();
        }

    }

    private void BindCompetitor()
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        ds = objCorModule.GetCompetitor();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCompetitor.DataSource = ds.Tables[0];
            ddlCompetitor.DataTextField = "Competitor";
            ddlCompetitor.DataValueField = "competitorID";
            ddlCompetitor.DataBind();
            ddlCompetitor.Items.Insert(0, new ListItem("--Add Competitor--", "0"));
            //ddlCompetitor.Items.Add("--Add Competitor--");

        }

        else
        {
            ddlCompetitor.DataSource = null;
            ddlCompetitor.DataBind();
        }
    }

    private void BindImplant()
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        ds = objCorModule.GetImplant();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlImplant.DataSource = ds.Tables[0];
            ddlImplant.DataTextField = "ImplantName";
            ddlImplant.DataValueField = "ImplantId";
            ddlImplant.DataBind();
            ddlImplant.Items.Insert(0, new ListItem("--Add Implant--", "0"));
            //ddlCompetitor.Items.Add("--Add Implant--");
        }

        else
        {
            ddlImplant.DataSource = null;
            ddlImplant.DataBind();
        }
    }

    private void BindClient()
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        ds = objCorModule.GetClientName();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlClientName.DataSource = ds.Tables[0];
            ddlClientName.DataTextField = "clientConame";
            ddlClientName.DataValueField = "clientCoId";
            ddlClientName.DataBind();
            ddlClientName.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        else
        {
            ddlClientName.DataSource = null;
            ddlClientName.DataBind();
        }


    }

    private void BindAccountManager()
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        ds = objCorModule.GetAccountManager();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlLocalAccountManager.DataSource = ds.Tables[0];
            ddlLocalAccountManager.DataTextField = "LoginId";
            ddlLocalAccountManager.DataValueField = "Sysuserid";
            ddlLocalAccountManager.DataBind();
            ddlLocalAccountManager.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        else
        {
            ddlLocalAccountManager.DataSource = null;
            ddlLocalAccountManager.DataBind();
        }


    }
    private void BindCityName(DropDownList ddlist)
    {
        DataSet dsCity = new DataSet();
        objCorModule = new CorporateModule();
        dsCity = objCorModule.GetCityName();
        if (dsCity.Tables[0].Rows.Count > 0)
        {
            ddlist.DataSource = dsCity.Tables[0];
            ddlist.DataTextField = "cityname";
            ddlist.DataValueField = "CityID";
            ddlist.DataBind();
            ddlist.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {
            ddlist.DataSource = null;
            ddlist.DataBind();
        }

    }

    public void InitializeControls(Control objcontrol)
    {
        foreach (Control control in objcontrol.Controls)
        {
            if ((control.GetType() == typeof(TextBox)))
            {
                ((TextBox)(control)).Text = "";
            }
            if ((control.GetType() == typeof(DropDownList)))
            {
                if (((DropDownList)(control)).Items.Count > 0)
                {
                    ((DropDownList)(control)).ClearSelection();
                    ((DropDownList)(control)).SelectedIndex = 0;
                }
            }
            if ((control.GetType() == typeof(ListBox)))
            {
                if (((ListBox)(control)).Items.Count > 0)
                {
                    ((ListBox)(control)).ClearSelection();
                    ((ListBox)(control)).SelectedIndex = 0;
                }
            }

            if ((control.GetType() == typeof(CheckBox)))
            {
                ((CheckBox)(control)).Checked = false;
            }

            if (control.HasControls() && control.GetType() != typeof(MultiView))
            {
                InitializeControls(control);
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        int result = 0;
        Int64 mobileNo;
        objSale = new SaleModule();
        objCorModule = new CorporateModule();
        string multipleImplantId = string.Empty;
        string multipleCompetitor = string.Empty;
        try
        {
            if (Convert.ToString(ddlLocalInf.SelectedValue) == "Yes" && Int64.TryParse(txtLocalInfluencerMobile.Text, out mobileNo) == false)
            {
                lblMsg.Text = "Mobile number should be numeric";
                return;
            }
            else if (Convert.ToString(ddlLocalInf.SelectedValue) == "Yes" && Int64.TryParse(txtLocalInfluencerMobile.Text, out mobileNo) == true && mobileNo.ToString().Length < 10)
            {
                lblMsg.Text = "Mobile number should not be less than 10 digits";
                return;
            }
            if (!string.IsNullOrEmpty(txtImplant.Text.Trim()))
            {
                DataSet ds = new DataSet();
                ds = objCorModule.CheckLoginValidate(txtImplant.Text.Trim());
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objSale.ImplantSysUserId = Convert.ToInt32(ds.Tables[0].Rows[0]["sysUseriD"]);
                    objSale.NewImplantName = Convert.ToString(ds.Tables[0].Rows[0]["UserName"]);
                }
                else
                {
                    objSale.ImplantSysUserId = 0;
                    objSale.NewImplantName = string.Empty;
                    lblMsg.Text = "Enter correct loginid ";
                    return;
                }
            }
            else
            {
                objSale.ImplantSysUserId = 0;
                objSale.NewImplantName = string.Empty;
            }
            if (ddlCompetitor.SelectedItem != null && ddlImplant.SelectedItem.Text == "0" && string.IsNullOrEmpty(txtImplant.Text))
            {
                lblMsg.Text = "Enter implant name";
                return;
            }
            else if (ddlCompetitor.SelectedItem != null && ddlCompetitor.SelectedItem.Text == "0" && string.IsNullOrEmpty(txtCompetitor.Text))
            {
                lblMsg.Text = "Enter competitor name";
                return;
            }
            else
            {

                objSale.ClientCoId = Convert.ToInt32(ddlClientName.SelectedValue);
                objSale.AccountType = ddlAccountType.SelectedValue;
                objSale.HQ = Convert.ToInt32(ddlHQ.SelectedValue);
                objSale.SignedBy = Convert.ToInt32(ddlSigned.SelectedValue);
                objSale.LocalInfluencer = ddlLocalInf.SelectedValue;
                objSale.MonthlyPotential = txtMonthlyPotential.Text.Trim();
                objSale.LocalAccountMgr = ddlLocalAccountManager.SelectedValue;
                objSale.SysUserID = Convert.ToInt32(Session["UserID"].ToString());
                objSale.LocalInfluencerName = txtLocalInfluencerName.Text;
                objSale.LocalInfluencerMobileNo = txtLocalInfluencerMobile.Text;

                if (ddlCompetitor.SelectedItem != null && objSale.ImplantSysUserId == 0 && Convert.ToString(ddlImplant.SelectedItem.Text) != "0")
                {

                    foreach (ListItem item in ddlImplant.Items)
                    {
                        if (item.Selected)
                        {
                            if (string.IsNullOrEmpty(multipleImplantId))
                            {
                                multipleImplantId = item.Value;
                            }
                            else
                            {
                                multipleImplantId = multipleImplantId + "," + item.Value;
                            }


                        }
                    }

                    //objSale.ImplantId = Convert.ToInt32(ddlImplant.SelectedValue);
                    objSale.ImplantId = multipleImplantId;
                }
                else
                {
                    objSale.ImplantId = "0";
                }

                if (!string.IsNullOrEmpty(txtCompetitor.Text))
                {
                    objSale.Competitor = txtCompetitor.Text.Trim();
                }
                else
                {
                    foreach (ListItem item in ddlCompetitor.Items)
                    {
                        if (item.Selected)
                        {
                            if (string.IsNullOrEmpty(multipleCompetitor))
                            {
                                multipleCompetitor = item.Text;
                            }
                            else
                            {
                                multipleCompetitor = multipleCompetitor + "," + item.Text;
                            }
                        }
                    }
                    objSale.Competitor = multipleCompetitor;

                    if (string.IsNullOrEmpty(objSale.Competitor.ToString()))
                    {
                        objSale.Competitor = "0";
                    }
                    //objSale.Competitor = ddlCompetitor.SelectedItem.Text.Trim();
                }

                result = objCorModule.SaveCorintSalesCorporate(objSale);
                if (result > 0)
                {
                    lblMsg.Text = "Record saved successfuly.";
                    InitializeControls(Form);
                }
                else
                {
                    lblMsg.Text = "Getting some issues to save data.";
                }
            }

        }
        catch (Exception Ex)
        {
            lblMsg.Text = Ex.Message;
        }



    }

    //public void ClearData()
    //{
    //  ddlClientName.SelectedValue="";
    //  ddlAccountType.SelectedValue="";
    //  ddlHQ.SelectedValue = "";
    //   ddlSigned.SelectedValue=""
    //    objSale.LocalInfluencer = ddlLocalInf.SelectedValue;
    //    objSale.MonthlyPotential = txtMonthlyPotential.Text.Trim();
    //    objSale.LocalAccountMgr = ddlLocalAccountManager.SelectedValue;
    //    objSale.SysUserID = Convert.ToInt32(Session["UserID"].ToString());
    //    objSale.LocalInfluencerName = txtLocalInfluencerName.Text;
    //    objSale.LocalInfluencerMobileNo = txtLocalInfluencerMobile.Text;
    //}

    protected void ddlLocalInf_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlLocalInf.SelectedItem.Text.ToString() == "Yes")
        {
            trLocalInfluencerName.Visible = true;
        }
        else
        {
            trLocalInfluencerName.Visible = false;
        }
    }
    protected void ddlCompetitor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCompetitor.Text == "0")
        {
            txtCompetitor.Visible = true;
        }
        else
        {
            txtCompetitor.Text = "";
            txtCompetitor.Visible = false;
        }
    }
    protected void ddlImplant_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlImplant.Text == "0")
        {
            txtImplant.Visible = true;
        }
        else
        {
            txtImplant.Text = "";
            txtImplant.Visible = false;

        }
    }
    protected void ddlClientName_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DataSet dsAccount = new DataSet();
        //objCorModule = new CorporateModule();
        //dsAccount = objCorModule.GetAccountType(Convert.ToInt32(ddlClientName.SelectedValue));
        //if (dsAccount.Tables[0].Rows.Count > 0)
        //{
        //    ddlAccountType.DataSource = dsAccount.Tables[0];
        //    ddlAccountType.DataTextField = "AccountName";
        //    ddlAccountType.DataValueField = "AccountTypeID";
        //    ddlAccountType.DataBind();
        //    ddlAccountType.Items.Insert(0, new ListItem("--Select--", "0"));
        //}
        //else
        //{
        //    ddlAccountType.DataSource = null;
        //    ddlAccountType.DataBind();
        //}

        DataSet ds = new DataSet();
        DataSet dsAccountType = new DataSet();
        DataSet dsHQ = new DataSet();
        DataSet dsSingedBy = new DataSet();

        objCorModule = new CorporateModule();
        string listAccountType = string.Empty;
        string listHQ1 = string.Empty;
        string listHQ2 = string.Empty;
        string listHQ3 = string.Empty;

        string listSignedBy1 = string.Empty;
        string listSignedBy2 = string.Empty;
        string listSignedBy3 = string.Empty;

        string listInfluencer1 = string.Empty;
        string listInfluencer2 = string.Empty;
        string listInfluencer3 = string.Empty;

        string listMonthlyPotential1 = string.Empty;
        string listMonthlyPotential2 = string.Empty;
        string listMonthlyPotential3 = string.Empty;

        string LocalInfluencerName1 = string.Empty;
        string LocalInfluencerName2 = string.Empty;
        string LocalInfluencerName3 = string.Empty;

        string LocalInfluencerMobileNo1 = string.Empty;
        string LocalInfluencerMobileNo2 = string.Empty;
        string LocalInfluencerMobileNo3 = string.Empty;

        string LocalAccountMgr1 = string.Empty;
        string LocalAccountMgr2 = string.Empty;
        string LocalAccountMgr3 = string.Empty;

        string listImplants1 = string.Empty;
        string listImplants2 = string.Empty;
        string listImplants3 = string.Empty;

        string listCompetitor1 = string.Empty;
        string listCompetitor2 = string.Empty;
        string listCompetitor3 = string.Empty;


        if (ddlClientName.SelectedIndex > 0)
        {
            //Account Type 
            dsAccountType = objCorModule.GetAccountType(Convert.ToInt32(ddlClientName.SelectedValue));
            if (dsAccountType.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsAccountType.Tables[0].Rows)
                {
                    if (string.IsNullOrEmpty(listAccountType))
                    {
                        listAccountType = Convert.ToString(dr["AccountType"]);
                    }
                    else
                    {
                        listAccountType = listAccountType + "," + Convert.ToString(dr["AccountType"]);
                    }

                }
            }
            if (!string.IsNullOrEmpty(listAccountType))
            {
                lblAccountType.Visible = true;
                lblAccountType.Text = listAccountType.ToString();
            }
            else
            {
                lblAccountType.Visible = false;
                lblAccountType.Text = string.Empty;
            }
            //End of Account Type

            //Start HeadQuarter
            ds = objCorModule.GetSaleHeadQuarter(Convert.ToInt32(ddlClientName.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (Convert.ToString(dr["AccountType"]) == "HQ")
                    {
                        if (string.IsNullOrEmpty(listHQ1))
                        {
                            listHQ1 = Convert.ToString(dr["cityName"]);
                        }
                        else
                        {
                            listHQ1 = listHQ1 + "," + Convert.ToString(dr["cityName"]);
                        }

                    }
                    if (Convert.ToString(dr["AccountType"]) == "Local Decent")
                    {
                        if (string.IsNullOrEmpty(listHQ2))
                        {
                            listHQ2 = Convert.ToString(dr["cityName"]);
                        }
                        else
                        {
                            listHQ2 = listHQ2 + "," + Convert.ToString(dr["cityName"]);
                        }
                    }
                    if (Convert.ToString(dr["AccountType"]) == "Network")
                    {
                        if (string.IsNullOrEmpty(listHQ3))
                        {
                            listHQ3 = Convert.ToString(dr["cityName"]);
                        }
                        else
                        {
                            listHQ3 = listHQ3 + "," + Convert.ToString(dr["cityName"]);
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(listHQ1))
            {
                lblddlHQ1.Visible = true;
                lblddlHQ1.Text = "Head Quarter HQ:- " + listHQ1.ToString();
            }
            else
            {
                lblddlHQ1.Visible = false;
                lblddlHQ1.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(listHQ2))
            {
                lblddlHQ2.Visible = true;
                lblddlHQ2.Text = "Head Quarter Local Decent:- " + listHQ2.ToString();
            }
            else
            {
                lblddlHQ2.Visible = false;
                lblddlHQ2.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(listHQ3))
            {
                lblddlHQ3.Visible = true;
                lblddlHQ3.Text = "Head Quarter Network:- " + listHQ3.ToString();
            }
            else
            {
                lblddlHQ3.Visible = false;
                lblddlHQ3.Text = string.Empty;
            }
            //End of HeadQuarter

            //Singed By
            dsSingedBy = objCorModule.GetSingedBy(Convert.ToInt32(ddlClientName.SelectedValue));
            if (dsSingedBy.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsSingedBy.Tables[0].Rows)
                {
                    if (Convert.ToString(dr["AccountType"]) == "HQ")
                    {
                        if (string.IsNullOrEmpty(listSignedBy1))
                        {
                            listSignedBy1 = Convert.ToString(dr["cityName"]);
                        }
                        else
                        {
                            listSignedBy1 = listSignedBy1 + "," + Convert.ToString(dr["cityName"]);
                        }

                    }
                    if (Convert.ToString(dr["AccountType"]) == "Local Decent")
                    {
                        if (string.IsNullOrEmpty(listSignedBy2))
                        {
                            listSignedBy2 = Convert.ToString(dr["cityName"]);
                        }
                        else
                        {
                            listSignedBy2 = listSignedBy2 + "," + Convert.ToString(dr["cityName"]);
                        }
                    }
                    if (Convert.ToString(dr["AccountType"]) == "Network")
                    {
                        if (string.IsNullOrEmpty(listSignedBy3))
                        {
                            listSignedBy3 = Convert.ToString(dr["cityName"]);
                        }
                        else
                        {
                            listSignedBy3 = listSignedBy3 + "," + Convert.ToString(dr["cityName"]);
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(listSignedBy1))
            {
                lblSingedByHQ.Visible = true;
                lblSingedByHQ.Text = "Signed by HQ:- " + listSignedBy1.ToString();
            }
            else
            {
                lblSingedByHQ.Visible = false;
                lblSingedByHQ.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(listSignedBy2))
            {
                lblSingedByLocal.Visible = true;
                lblSingedByLocal.Text = "Signed by Local Decent:- " + listSignedBy2.ToString();
            }
            else
            {
                lblSingedByLocal.Visible = false;
                lblSingedByLocal.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(listSignedBy3))
            {
                lblSingedByNetwork.Visible = true;
                lblSingedByNetwork.Text = "Signed by Network:- " + listSignedBy3.ToString();
            }
            else
            {
                lblSingedByNetwork.Visible = false;
                lblSingedByNetwork.Text = string.Empty;
            }
            //End Singed By

            //Local Influencer
            dsSingedBy = objCorModule.GetLocalInfluencer(Convert.ToInt32(ddlClientName.SelectedValue));
            if (dsSingedBy.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsSingedBy.Tables[0].Rows)
                {
                    if (Convert.ToString(dr["AccountType"]) == "HQ")
                    {
                        if (string.IsNullOrEmpty(listInfluencer1))
                        {
                            listInfluencer1 = Convert.ToString(dr["LocalInfluencer"]);
                        }
                        else
                        {
                            listInfluencer1 = listInfluencer1 + "," + Convert.ToString(dr["LocalInfluencer"]);
                        }
                        if (string.IsNullOrEmpty(listMonthlyPotential1))
                        {
                            listMonthlyPotential1 = Convert.ToString(dr["MonthlyPotential"]);
                        }
                        else
                        {
                            listMonthlyPotential1 = listMonthlyPotential1 + "," + Convert.ToString(dr["MonthlyPotential"]);
                        }
                        if (string.IsNullOrEmpty(LocalAccountMgr1))
                        {
                            LocalAccountMgr1 = Convert.ToString(dr["LocalAccountMgr"]);
                        }
                        else
                        {
                            LocalAccountMgr1 = LocalAccountMgr1 + "," + Convert.ToString(dr["LocalAccountMgr"]);
                        }

                        if (string.IsNullOrEmpty(LocalInfluencerName1))
                        {
                            LocalInfluencerName1 = Convert.ToString(dr["LocalInfluencerName"]);
                        }
                        else
                        {
                            LocalInfluencerName1 = LocalInfluencerName1 + "," + Convert.ToString(dr["LocalInfluencerName"]);
                        }
                        if (string.IsNullOrEmpty(LocalInfluencerMobileNo1))
                        {
                            LocalInfluencerMobileNo1 = Convert.ToString(dr["LocalInfluencerMobileNo"]);
                        }
                        else
                        {
                            LocalInfluencerMobileNo1 = LocalInfluencerMobileNo1 + "," + Convert.ToString(dr["LocalInfluencerMobileNo"]);
                        }

                    }
                    if (Convert.ToString(dr["AccountType"]) == "Local Decent")
                    {
                        if (string.IsNullOrEmpty(listInfluencer2))
                        {
                            listInfluencer2 = Convert.ToString(dr["LocalInfluencer"]);
                        }
                        else
                        {
                            listInfluencer2 = listInfluencer2 + "," + Convert.ToString(dr["LocalInfluencer"]);
                        }
                        if (string.IsNullOrEmpty(listMonthlyPotential2))
                        {
                            listMonthlyPotential2 = Convert.ToString(dr["MonthlyPotential"]);
                        }
                        else
                        {
                            listMonthlyPotential2 = listMonthlyPotential2 + "," + Convert.ToString(dr["MonthlyPotential"]);
                        }
                        if (string.IsNullOrEmpty(LocalAccountMgr2))
                        {
                            LocalAccountMgr2 = Convert.ToString(dr["LocalAccountMgr"]);
                        }
                        else
                        {
                            LocalAccountMgr2 = LocalAccountMgr2 + "," + Convert.ToString(dr["LocalAccountMgr"]);
                        }

                        if (string.IsNullOrEmpty(LocalInfluencerName2))
                        {
                            LocalInfluencerName2 = Convert.ToString(dr["LocalInfluencerName"]);
                        }
                        else
                        {
                            LocalInfluencerName2 = LocalInfluencerName2 + "," + Convert.ToString(dr["LocalInfluencerName"]);
                        }
                        if (string.IsNullOrEmpty(LocalInfluencerMobileNo2))
                        {
                            LocalInfluencerMobileNo2 = Convert.ToString(dr["LocalInfluencerMobileNo"]);
                        }
                        else
                        {
                            LocalInfluencerMobileNo2 = LocalInfluencerMobileNo2 + "," + Convert.ToString(dr["LocalInfluencerMobileNo"]);
                        }

                    }
                    if (Convert.ToString(dr["AccountType"]) == "Network")
                    {
                        if (string.IsNullOrEmpty(listInfluencer3))
                        {
                            listInfluencer3 = Convert.ToString(dr["LocalInfluencer"]);
                        }
                        else
                        {
                            listInfluencer3 = listInfluencer3 + "," + Convert.ToString(dr["LocalInfluencer"]);
                        }
                        if (string.IsNullOrEmpty(listMonthlyPotential3))
                        {
                            listMonthlyPotential3 = Convert.ToString(dr["MonthlyPotential"]);
                        }
                        else
                        {
                            listMonthlyPotential3 = listMonthlyPotential3 + "," + Convert.ToString(dr["MonthlyPotential"]);
                        }
                        if (string.IsNullOrEmpty(LocalAccountMgr3))
                        {
                            LocalAccountMgr3 = Convert.ToString(dr["LocalAccountMgr"]);
                        }
                        else
                        {
                            LocalAccountMgr3 = LocalAccountMgr3 + "," + Convert.ToString(dr["LocalAccountMgr"]);
                        }

                        if (string.IsNullOrEmpty(LocalInfluencerName3))
                        {
                            LocalInfluencerName3 = Convert.ToString(dr["LocalInfluencerName"]);
                        }
                        else
                        {
                            LocalInfluencerName3 = LocalInfluencerName3 + "," + Convert.ToString(dr["LocalInfluencerName"]);
                        }
                        if (string.IsNullOrEmpty(LocalInfluencerMobileNo3))
                        {
                            LocalInfluencerMobileNo3 = Convert.ToString(dr["LocalInfluencerMobileNo"]);
                        }
                        else
                        {
                            LocalInfluencerMobileNo3 = LocalInfluencerMobileNo3 + "," + Convert.ToString(dr["LocalInfluencerMobileNo"]);
                        }

                    }
                }
            }
            if (!string.IsNullOrEmpty(listInfluencer1))
            {
                lblLocalInf1.Visible = true;
                lblLocalInf1.Text = "Local Influencer HQ:- " + listInfluencer1.ToString();
            }
            else
            {
                lblLocalInf1.Visible = false;
                lblLocalInf1.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(listInfluencer2))
            {
                lblLocalInf2.Visible = true;
                lblLocalInf2.Text = "Local Influencer Local Decent:- " + listInfluencer2.ToString();
            }
            else
            {
                lblLocalInf2.Visible = false;
                lblLocalInf2.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(listInfluencer3))
            {
                lblLocalInf3.Visible = true;
                lblLocalInf3.Text = "Local Influencer Network:- " + listInfluencer3.ToString();
            }
            else
            {
                lblLocalInf3.Visible = false;
                lblLocalInf3.Text = string.Empty;
            }


            if (!string.IsNullOrEmpty(listMonthlyPotential1))
            {
                lblMonthlyPotential1.Visible = true;
                lblMonthlyPotential1.Text = "Monthly Potential HQ:- " + listMonthlyPotential1.ToString();
            }
            else
            {
                lblMonthlyPotential1.Visible = false;
                lblMonthlyPotential1.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(listMonthlyPotential2))
            {
                lblMonthlyPotential2.Visible = true;
                lblMonthlyPotential2.Text = "Monthly Potential Local Decent:- " + listMonthlyPotential2.ToString();
            }
            else
            {
                lblMonthlyPotential2.Visible = false;
                lblMonthlyPotential2.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(listMonthlyPotential3))
            {
                lblMonthlyPotential3.Visible = true;
                lblMonthlyPotential3.Text = "Monthly Potential Network:- " + listMonthlyPotential3.ToString();
            }
            else
            {
                lblMonthlyPotential3.Visible = false;
                lblMonthlyPotential3.Text = string.Empty;
            }


            if (!string.IsNullOrEmpty(LocalInfluencerName1))
            {
                lblLocalInfluencerName1.Visible = true;
                lblLocalInfluencerName1.Text = "Local Influencer Name HQ:- " + LocalInfluencerName1.ToString();
            }
            else
            {
                lblLocalInfluencerName1.Visible = false;
                lblLocalInfluencerName1.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(LocalInfluencerName2))
            {
                lblLocalInfluencerName2.Visible = true;
                lblLocalInfluencerName2.Text = "Local Influencer Name Local Decent:- " + LocalInfluencerName2.ToString();
            }
            else
            {
                lblLocalInfluencerName2.Visible = false;
                lblLocalInfluencerName2.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(LocalInfluencerName3))
            {
                lblLocalInfluencerName3.Visible = true;
                lblLocalInfluencerName3.Text = "Local Influencer Name Network:- " + LocalInfluencerName3.ToString();
            }
            else
            {
                lblLocalInfluencerName3.Visible = false;
                lblLocalInfluencerName3.Text = string.Empty;
            }

            if (!string.IsNullOrEmpty(LocalInfluencerMobileNo1))
            {
                lblLocalInfluencerMobile1.Visible = true;
                lblLocalInfluencerMobile1.Text = "Local Influencer Mobile No HQ:- " + LocalInfluencerMobileNo1.ToString();
            }
            else
            {
                lblLocalInfluencerMobile1.Visible = false;
                lblLocalInfluencerMobile1.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(LocalInfluencerMobileNo2))
            {
                lblLocalInfluencerMobile2.Visible = true;
                lblLocalInfluencerMobile2.Text = "Local Influencer Mobile No Local Decent:- " + LocalInfluencerMobileNo2.ToString();
            }
            else
            {
                lblLocalInfluencerMobile2.Visible = false;
                lblLocalInfluencerMobile2.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(LocalInfluencerMobileNo3))
            {
                lblLocalInfluencerMobile3.Visible = true;
                lblLocalInfluencerMobile3.Text = "Local Influencer Mobile No Network:- " + LocalInfluencerMobileNo3.ToString();
            }
            else
            {
                lblLocalInfluencerMobile3.Visible = false;
                lblLocalInfluencerMobile3.Text = string.Empty;
            }

            if (!string.IsNullOrEmpty(LocalAccountMgr1))
            {
                lblLocalAccountManager1.Visible = true;
                lblLocalAccountManager1.Text = "Local Account Mgr HQ:- " + LocalAccountMgr1.ToString();
            }
            else
            {
                lblLocalAccountManager1.Visible = false;
                lblLocalAccountManager1.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(LocalAccountMgr1))
            {
                lblLocalAccountManager2.Visible = true;
                lblLocalInfluencerMobile2.Text = "Local Account Mgr Local Decent:- " + LocalAccountMgr1.ToString();
            }
            else
            {
                lblLocalInfluencerMobile2.Visible = false;
                lblLocalInfluencerMobile2.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(LocalAccountMgr1))
            {
                lblLocalInfluencerMobile3.Visible = true;
                lblLocalInfluencerMobile3.Text = "Local Account Mgr Network:- " + LocalAccountMgr1.ToString();
            }
            else
            {
                lblLocalInfluencerMobile3.Visible = false;
                lblLocalInfluencerMobile3.Text = string.Empty;
            }
            //Local Influencer

            //Implant 

            ds = objCorModule.GetImplantsClientBasis(Convert.ToInt32(ddlClientName.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (Convert.ToString(dr["AccountType"]) == "HQ")
                    {
                        if (string.IsNullOrEmpty(listImplants1))
                        {
                            listImplants1 = Convert.ToString(dr["ImplantName"]);
                        }
                        else
                        {
                            listImplants1 = listImplants1 + "," + Convert.ToString(dr["ImplantName"]);
                        }

                    }
                    if (Convert.ToString(dr["AccountType"]) == "Local Decent")
                    {
                        if (string.IsNullOrEmpty(listImplants2))
                        {
                            listImplants2 = Convert.ToString(dr["ImplantName"]);
                        }
                        else
                        {
                            listImplants2 = listImplants2 + "," + Convert.ToString(dr["ImplantName"]);
                        }
                    }
                    if (Convert.ToString(dr["AccountType"]) == "Network")
                    {
                        if (string.IsNullOrEmpty(listImplants3))
                        {
                            listImplants3 = Convert.ToString(dr["ImplantName"]);
                        }
                        else
                        {
                            listImplants3 = listImplants3 + "," + Convert.ToString(dr["ImplantName"]);
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(listImplants1))
            {
                lblImplant1.Visible = true;
                lblImplant1.Text = "Implant HQ:- " + listImplants1.ToString();
            }
            else
            {
                lblImplant1.Visible = false;
                lblImplant1.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(listImplants2))
            {
                lblImplant2.Visible = true;
                lblImplant2.Text = "Implant Local Decent:- " + listImplants2.ToString();
            }
            else
            {
                lblImplant2.Visible = false;
                lblImplant2.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(listImplants3))
            {
                lblImplant3.Visible = true;
                lblImplant3.Text = "Implant Network:- " + listImplants3.ToString();
            }
            else
            {
                lblImplant3.Visible = false;
                lblImplant3.Text = string.Empty;
            }
            //End of implant 
            //Competitore

            ds = objCorModule.GetCompetitorListClientBasis(Convert.ToInt32(ddlClientName.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (Convert.ToString(dr["AccountType"]) == "HQ")
                    {
                        if (string.IsNullOrEmpty(listCompetitor1))
                        {
                            listCompetitor1 = Convert.ToString(dr["Competitor"]);
                        }
                        else
                        {
                            listCompetitor1 = listCompetitor1 + "," + Convert.ToString(dr["Competitor"]);
                        }

                    }
                    if (Convert.ToString(dr["AccountType"]) == "Local Decent")
                    {
                        if (string.IsNullOrEmpty(listCompetitor2))
                        {
                            listCompetitor2 = Convert.ToString(dr["Competitor"]);
                        }
                        else
                        {
                            listCompetitor2 = listCompetitor2 + "," + Convert.ToString(dr["Competitor"]);
                        }
                    }
                    if (Convert.ToString(dr["AccountType"]) == "Network")
                    {
                        if (string.IsNullOrEmpty(listCompetitor3))
                        {
                            listCompetitor3 = Convert.ToString(dr["Competitor"]);
                        }
                        else
                        {
                            listCompetitor3 = listCompetitor3 + "," + Convert.ToString(dr["Competitor"]);
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(listCompetitor1))
            {
                lblCompetitor1.Visible = true;
                lblCompetitor1.Text = "Competitor HQ:- " + listCompetitor1.ToString();
            }
            else
            {
                lblCompetitor1.Visible = false;
                lblCompetitor1.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(listCompetitor2))
            {
                lblCompetitor2.Visible = true;
                lblCompetitor2.Text = "Competitor Local Decent:- " + listCompetitor2.ToString();
            }
            else
            {
                lblCompetitor2.Visible = false;
                lblCompetitor2.Text = string.Empty;
            }
            if (!string.IsNullOrEmpty(listCompetitor3))
            {
                lblCompetitor3.Visible = true;
                lblCompetitor3.Text = "Competitor Network:- " + listCompetitor3.ToString();
            }
            else
            {
                lblCompetitor3.Visible = false;
                lblCompetitor3.Text = string.Empty;
            }

            //End Copletitor
        }


    }
    protected void ddlAccountType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet dsHQ = new DataSet();
        objCorModule = new CorporateModule();
        dsHQ = objCorModule.GetSaleHeadQuarter(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlAccountType.SelectedValue));
        if (dsHQ.Tables[0].Rows.Count > 0)
        {
            ddlHQ.DataSource = dsHQ.Tables[0];
            ddlHQ.DataTextField = "CityName";
            ddlHQ.DataValueField = "CityID";
            ddlHQ.DataBind();
            ddlHQ.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {
            ddlHQ.DataSource = null;
            ddlHQ.DataBind();
        }
    }
    protected void bntViewDetails_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        if (ddlClientName.SelectedIndex > 0)
        {
            lblMsg.Visible = false;
            ds = objCorModule.EntryDetails(Convert.ToInt32(ddlClientName.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvShowDetails.DataSource = ds.Tables[0];
                gvShowDetails.DataBind();
            }
            else
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Record not available.";
                gvShowDetails.DataSource = null;
                gvShowDetails.DataBind();

            }

        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "Select client name first.";
        }
        

    }
   
}
