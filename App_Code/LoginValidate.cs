using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Data.Sql;
using Insta;

/// <summary>
/// Summary description for CloseInvoice
/// </summary>
public class LoginValidate
{
    public LoginValidate()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public DataTable ValidateUser(string LoginID, string Password)
    {
        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@LoginID", LoginID);
        param[1] = new SqlParameter("@Password", Password);
        return SqlHelper.ExecuteDatatable("SP_AuthenticateUser", param);
    }

    public DataTable InsertLoginDetail(string UserName, string IPaddress)
    {
        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@UserName", UserName);
        param[1] = new SqlParameter("@IPaddress", IPaddress);
        return SqlHelper.ExecuteDatatable("InsertLoginDetail", param);
    }

    public DataTable LogOUt(string MaxLogID)
    {
        SqlParameter[] param = new SqlParameter[1];

        param[0] = new SqlParameter("@MaxLogID", MaxLogID);
        return SqlHelper.ExecuteDatatable("Prc_LogOut", param);
    }

}