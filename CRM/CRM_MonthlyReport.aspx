<%@ Page Language="C#" MasterPageFile="~/CRDMasterPage.master" AutoEventWireup="true"
    Theme="TMSTheme" CodeFile="CRM_MonthlyReport.aspx.cs" Inherits="Reporting_CRM_MonthlyReport"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder_Page" runat="Server">

    <script type="text/javascript" src="../App_Scripts/JScript_CalendarControl.js"> </script>

    <table width="100%" style="font-size: 11pt; font-family: Tahoma">
        <tr>
            <td>
            </td>
            <td align="center" colspan="2" style="font-weight: bold">
                <asp:Label ID="LblHeading1" runat="server" Font-Names="Tahoma" Width="234px" Text="CRM Monthly Report"
                    Font-Size="Small" Font-Bold="True" BorderWidth="1px" BorderStyle="Solid" BorderColor="#CCCC99"
                    BackColor="#F7F7DE"></asp:Label>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right" style="font-weight: bold">
                <asp:Label ID="Label4" runat="server" Text="Report For:"></asp:Label></td>
            <td align="left" style="font-weight: bold">
                <asp:DropDownList ID="ddlBranch" runat="server">
                    <asp:ListItem Value="1" Enabled="false">EasyCabs</asp:ListItem>
                    <asp:ListItem Value="2">CRD</asp:ListItem>
                    <asp:ListItem Value="3">TRIPS</asp:ListItem>
                </asp:DropDownList></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td style="font-weight: bold" align="right">
                <asp:Label ID="Label1" runat="server" Text="From Date:"></asp:Label>
                <asp:TextBox ID="txtFrom" onfocus="showCalendarControl(this);" runat="server" Width="110px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFrom"
                    ErrorMessage="RequiredFieldValidator" SetFocusOnError="True">*</asp:RequiredFieldValidator>
            </td>
            <td style="font-weight: bold" align="left">
                <asp:Label ID="Label2" runat="server" Text="To Date :"></asp:Label>
                <asp:TextBox ID="txtTo" onfocus="showCalendarControl(this);" runat="server" Width="110px">*</asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTo"
                    ErrorMessage="RequiredFieldValidator" SetFocusOnError="True">*</asp:RequiredFieldValidator>
                <asp:Button ID="btnShow" runat="server" OnClick="btnShow_Click" Text="Show" />
                &nbsp;&nbsp;
                <asp:Button ID="btnExport" runat="server" ForeColor="Maroon" OnClick="btnExport_Click"
                    Text="Export To Excel" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td colspan="3" style="font-size: 8pt">
                <asp:GridView ID="gvReport" runat="server" BackColor="LightGoldenrodYellow" BorderColor="Tan"
                    BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both" Width="100%"
                     EmptyDataText="0 Records Found">
                    <FooterStyle BackColor="Tan" />
                    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                    <HeaderStyle BackColor="Tan" Font-Bold="True" />
                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text="<%# Container.DataItemIndex+1 %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
