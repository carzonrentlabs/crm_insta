using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for JsonClass
/// </summary>
public class JsonClass
{
	public JsonClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string ConvertObjecttoJSON(object clsobj)
    {
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        string jsonString = serializer.Serialize(clsobj);
        return jsonString;
    }
    public String sendJSONResponse(String json)
    {

        HttpContext.Current.Response.ContentType = "application/json";
        HttpContext.Current.Response.Output.Write(json);
        HttpContext.Current.Response.End();
        return string.Empty;
    }
}
