using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for OL_LoginUser
/// </summary>
public class OL_LoginUser
{
   
    private int sysUserId;

    public int SysUserId
    {
        get { return sysUserId; }
        set { sysUserId = value; }
    }

    private string loginId;

    public string LoginId
    {
        get { return loginId; }
        set { loginId = value; }
    }


    private string password;

    public string Password
    {
        get { return password; }
        set { password = value; }
    }

    private string loginStatus;

    public string LoginStatus
    {
        get { return loginStatus; }
        set { loginStatus = value; }
    }
    private string userName;

    public string UserName
    {
        get { return userName; }
        set { userName = value; }
    }

    private string userCityName;

    public string UserCityName
    {
        get { return userCityName; }
        set { userCityName = value; }
    }

   
    private string maxLoginID;

    public string MaxLoginID
    {
        get { return maxLoginID; }
        set { maxLoginID = value; }
    }

}
