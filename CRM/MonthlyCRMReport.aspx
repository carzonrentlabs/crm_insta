<%@ Page Language="C#" MasterPageFile="~/CRDMasterpage.master" AutoEventWireup="true" CodeFile="MonthlyCRMReport.aspx.cs" Inherits="CallCentre_MonthlyCRMReport" Title="Insta:CRMMonthlyReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Page" Runat="Server">
    <table width="100%">
        <tr>
            <td >
            </td>
        </tr>
        <tr>
            <td>
                <table width="60%" bgcolor="#ffffcc" border="1" bordercolor="#000000" cellpadding="1" cellspacing="1">
                    <tr>
                        <td align="left" >
                            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Report Type"></asp:Label></td>
                        <td align="left">
                            <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="From Date"></asp:Label></td>
                        <td align="left">
                            <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="To Date"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:DropDownList ID="ddlrptType" runat="server" Width="80%">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                                <%--<asp:ListItem Value="1">EasyCabs</asp:ListItem>--%>
                                <asp:ListItem Value="2">CRD</asp:ListItem>
                            </asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                ControlToValidate="ddlrptType" ErrorMessage="Please Select Report Type" InitialValue="0">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFromDate" runat="server" Width="70%"></asp:TextBox><asp:RequiredFieldValidator
                                ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate" ErrorMessage="Please Select From Date">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtToDate" runat="server" Width="70%"></asp:TextBox><asp:RequiredFieldValidator
                                ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate" ErrorMessage="Please Select To Date">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtToDate"
                        ControlToValidate="txtFromDate" ErrorMessage="From Date Should be less than or equal to ToDate."
                        Operator="LessThanEqual" Type="Date" Width="1px">*</asp:CompareValidator></td>
                    </tr>
                </table>
                <asp:Button ID="btnView" runat="server" OnClick="btnView_Click" Text="View" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                    ShowSummary="False" />
                &nbsp;&nbsp;
                <cc1:calendarextender id="CalendarExtender1" runat="server" format="MM-dd-yyyy" targetcontrolid="txtFromDate" PopupPosition="TopLeft"></cc1:calendarextender>
                <cc1:calendarextender id="CalendarExtender2" runat="server" format="MM-dd-yyyy" targetcontrolid="txtToDate" PopupPosition="TopRight"></cc1:calendarextender>
            </td>
        </tr>
        
        <tr>
            <td align="center" valign="middle">
                &nbsp;<asp:Panel ID="Panel1" runat="server" Visible="False"
                    Width="800px"  Height="300px" >
                    <CR:CrystalReportViewer ID="CRMMonthlyReport" runat="server" AutoDataBind="true"
                        DisplayGroupTree="False" EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False"
                        ReuseParameterValuesOnRefresh="True" BestFitPage="False" BorderStyle="Solid" BorderWidth="1px" Width="800px" Height="300px"/>
                    &nbsp;</asp:Panel>
            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="sdcsYear" runat="server" ConnectionString="<%$ ConnectionStrings:EasyCabsConnString %>"
        ProviderName="<%$ ConnectionStrings:EasyCabsConnString.ProviderName %>" SelectCommand="BL_SP_GetYear"
        SelectCommandType="StoredProcedure"></asp:SqlDataSource>
</asp:Content>

