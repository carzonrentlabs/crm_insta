Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

Public Class ClsGeneric
    Dim cmd As New SqlCommand
    Dim con As ClsCon
    Dim dtResult As New Data.DataTable
    Dim sqlconn As SqlConnection
    Dim daSql As New SqlDataAdapter


    Function FetchData(ByVal strSql As String) As Data.DataTable
        ' Try

        dtResult.Clear()
        con = New ClsCon
        sqlConn = con.con
        cmd = New SqlCommand(strSql, sqlconn)
        daSql.SelectCommand = cmd
        If Not Data.ConnectionState.Open Then sqlConn.Open()
        cmd.ExecuteNonQuery()
        ' con.SqlConnection.Close()

        daSql.Fill(dtResult)
        Return dtResult
        'Catch ex As Exception
        '    'Response.Write(ex.Message)

        'Finally
        '    con.SqlConnection.Close()
        'End Try

    End Function

    Function RunQuerry(ByVal strSql As String) As Boolean
        Dim Executed As Boolean
        Try
            con = New ClsCon
            sqlConn = con.con
            cmd = New SqlCommand(strSql, sqlconn)
            If Not Data.ConnectionState.Open Then sqlConn.Open()
            cmd.ExecuteNonQuery()
            Executed = True
        Catch ex As Exception
            Executed = False
        Finally
            con.con.Close()
        End Try
        Return Executed
    End Function
    Public Sub PopulateListBox(ByVal ColumnCodeName As String, ByVal ColumnDescName As String, ByVal TableName As String, ByVal ListName As DropDownList, ByVal strSelect As String)
        'Overrided function that generates a dropdown list box having a value 
        'and text field different
        Dim lstrSql As String
        Dim daList As SqlDataAdapter
        Dim dsList As New Data.DataSet
        con = New ClsCon
        sqlConn = con.con
       
        sqlConn.Open()
        lstrSql = "select distinct RTrim(" & ColumnCodeName & ") as " & ColumnCodeName & ",RTrim(" & ColumnDescName & ")as " & ColumnDescName & "1 from " & TableName & " order by " & ColumnCodeName & ""
        daList = New SqlDataAdapter(lstrSql, sqlconn)
        daList.Fill(dsList, "DropDown")
       
        ListName.DataTextField = dsList.Tables(0).Columns(1).ToString()
        ListName.DataValueField = dsList.Tables(0).Columns(0).ToString()
        ListName.DataSource = dsList
        ListName.DataBind()
        ListName.Items.Insert(0, strSelect)

    End Sub
    Public Sub PopulateDDLQuerry(ByVal sqlquerry As String, ByVal ListName As DropDownList, ByVal strSelect As String)
        'Overrided function that generates a dropdown list box having a value 
        'and text field different
        ' Dim lstrSql As String
        Dim daList As SqlDataAdapter
        Dim dsList As New Data.DataSet
        con = New ClsCon
        sqlConn = con.con
        sqlConn.Open()
        'lstrSql = "select distinct RTrim(" & ColumnCodeName & ") as " & ColumnCodeName & ",RTrim(" & ColumnDescName & ")as " & ColumnDescName & "1 from " & TableName & " order by " & ColumnCodeName & ""
        daList = New SqlDataAdapter(sqlquerry, sqlconn)
        daList.Fill(dsList, "DropDown")

        ListName.DataTextField = dsList.Tables(0).Columns(1).ToString()
        ListName.DataValueField = dsList.Tables(0).Columns(0).ToString()
        ListName.DataSource = dsList
        ListName.DataBind()
        ListName.Items.Insert(0, strSelect)

    End Sub
    Public Function FetchDataReader(ByVal sql As String) As SqlDataReader

        Dim cmd As New SqlCommand
        con = New ClsCon
        sqlconn = con.con
        sqlconn.Open()
        cmd.CommandText = sql
        cmd.Connection = sqlconn
        Dim dr As SqlDataReader = cmd.ExecuteReader
        Return dr
    End Function

Function FetchRecord(ByVal strSql As String) As String

        Dim RecordValue As String
        Try
            con = New ClsCon
            sqlConn = con.con
            cmd = New SqlCommand(strSql, sqlconn)
            If sqlConn.State = Data.ConnectionState.Closed Then sqlConn.Open()
            RecordValue = cmd.ExecuteScalar()

        Catch ex As Exception
            RecordValue = False
        Finally
            con.con.Close()
        End Try
        Return RecordValue
    End Function
    Function FormatDate(ByVal currentDate) As String
        Dim ChangedFormat As String
        ChangedFormat = Format(DateTime.ParseExact(currentDate, "dd/mm/yyyy", System.Globalization.CultureInfo.CurrentCulture), "mm/dd/yyyy")
        Return ChangedFormat
    End Function
End Class
