<%@ Page Language="C#" MasterPageFile="~/CRDMasterpage.master" Theme="TMSTheme" AutoEventWireup="true"
    CodeFile="CsatReport.aspx.cs" Inherits="CRM_CsatReport" Title="Insta : CSAT summary report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder_Page" runat="Server">

    <script type="text/javascript" src="../App_Scripts/JScript_CalendarControl.js"> </script>

    <script type="text/javascript" src="../App_Scripts/JScript_KeyPress.js"></script>

    <fieldset class="FieldsetClassic">
        <legend class="LegendClassic">CSAT Summary Report</legend>
        <table width="70%" style="font-size: 9pt; font-family: Tahoma">
            <tr>
                <td align="right" style="font-weight: bold">
                    Select Branch :</td>
                <td align="left" style="font-weight: bold">
                    <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
                        <asp:ListItem Selected="True" Value="2">--CRD--</asp:ListItem>
                        <asp:ListItem Value="3">CRD LIMO</asp:ListItem>
                        <asp:ListItem Value="4">CRD TRIPS</asp:ListItem>
                    </asp:DropDownList></td>
                <td align="right" style="font-weight: bold">
                    From Date :</td>
                <td align="left" style="font-weight: bold">
                    <asp:TextBox ID="txtFromDate" onfocus="showCalendarControl(this);" runat="server"
                        Width="97px" MaxLength="10"></asp:TextBox></td>
                <td align="right" style="font-weight: bold">
                    To Date :</td>
                <td align="left" style="font-weight: bold">
                    <asp:TextBox ID="txtToDate" onfocus="showCalendarControl(this);" runat="server" Width="97px"
                        MaxLength="10"></asp:TextBox>
                    <asp:Button ID="btnShow" runat="server" Text="Show" OnClick="btnShow_Click" /></td>
            </tr>
        </table>
        <table width="80%">
            <tr>
                <td align="center">
                    <div align="left">
                        <asp:GridView ID="gvReport" runat="server" BackColor="LightGoldenrodYellow" BorderColor="Tan"
                            BorderWidth="1px" CellPadding="2" DataSourceID="ODS_For_gvReport" ForeColor="Black"
                            GridLines="None" Width="100%">
                            <FooterStyle BackColor="Tan" />
                            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                            <HeaderStyle BackColor="Tan" Font-Bold="True" />
                            <AlternatingRowStyle BackColor="PaleGoldenrod" />
                        </asp:GridView>
                    </div>
                    <asp:ObjectDataSource ID="ODS_For_gvReport" runat="server" SelectMethod="ListCSAT_Report"
                        TypeName="Systems.CRM.CSAT">
                        <SelectParameters>
                            <asp:Parameter Name="BranchID" Type="Int32" />
                            <asp:Parameter Name="StartDate" Type="DateTime" />
                            <asp:Parameter Name="EndDate" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
        </table>
        <br />
        <br />
    </fieldset>
    <fieldset class="FieldsetClassic">
        <legend class="LegendClassic">CSAT Suggestion Report </legend>
        <table width="60%" style="font-size: 9pt; font-family: Tahoma">
            <tr>
                <td align="left">
                    From Date :
                    <asp:TextBox ID="txtStartDate" onfocus="showCalendarControl(this);" runat="server"
                        Width="100px"></asp:TextBox></td>
                <td align="left">
                    To Date :
                    <asp:TextBox ID="txtEndDate" onfocus="showCalendarControl(this);" runat="server"
                        Width="100px"></asp:TextBox></td>
                <td colspan="2">
                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" /></td>
            </tr>
            <tr>
                <td align="center" colspan="4" style="color: #ff0000">
                    <asp:Literal ID="ltrlMsg" runat="server"></asp:Literal></td>
            </tr>
        </table>
    </fieldset>
</asp:Content>
