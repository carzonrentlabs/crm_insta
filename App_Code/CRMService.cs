using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Insta;


/// <summary>
/// Summary description for CRMService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class CRMService : System.Web.Services.WebService {

    public CRMService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }
    [WebMethod]
    [ScriptMethod]
    public string ComplaintRegistration()
    {
        string Json = "{}";
        string CrmId = string.Empty;
        CRMRes objCrm = new CRMRes();
        JsonClass objjson = new JsonClass();
        try
        {
            
            CRM_Insta objInsta = new CRM_Insta();
            string postData = new System.IO.StreamReader(Context.Request.InputStream).ReadToEnd();
            CRMServiceClass crmRequest = (new JavaScriptSerializer()).Deserialize<CRMServiceClass>(postData);
            CrmId=objInsta.RegisterComplaintService(crmRequest.BranchID, crmRequest.JobLocationId, crmRequest.JobNo,Convert.ToDateTime(crmRequest.JobDate), crmRequest.Job_From, crmRequest.Job_To, crmRequest.CustomerName,
            crmRequest.CustomerContact, crmRequest.ContactDetails, crmRequest.Remarks, crmRequest.CabID, crmRequest.ChauffeurName, crmRequest.ChauffeurContact,
            crmRequest.ComplaintSourceID,Convert.ToDateTime(crmRequest.ComplaintDateTime), crmRequest.ComplaintDescription, crmRequest.RegisteredBy,Convert.ToDateTime(crmRequest.Reg_DateTime),
            crmRequest.ResolutionAt, crmRequest.PriorityID, crmRequest.ComplaintStatus, crmRequest.CarCategory, crmRequest.SubCategoryId, crmRequest.GuestEmailId, crmRequest.CarNo,
            crmRequest.TotalUsageTime,crmRequest.TotalUsageKms,crmRequest.TotalTollParking,crmRequest.Concern,crmRequest.ConcernText
            );

      //          int BranchID,int JobLocationID,string JobID,DateTime JobDate,string Job_From,string Job_To,string CustomerName
      //, string CustomerContact
      //, string ContactDetails
      //, string Remarks
      //, string CabID
      //, string ChauffeurName
      //, string ChauffeurContact
      //, int ComplaintSourceID
      //, DateTime ComplaintDateTime
      //, string ComplaintDescription
      //, string RegisteredBy
      //, DateTime Reg_DateTime
      //, int ResolutionAt
      //, int PriorityID
      //, int ComplaintStatus
      //, string CarCategory
      //, int SubCategoryId
      //, string guestEmailId
      //, string CarNo

     //,string TotalUsageTime 
     // ,string TotalUsageKms
     // ,string TotalParkingTollExtra
     // ,string Concern 
     // ,string ConcernText 

            //////////////////////
            //objBal.RegisterComplaint(drmRequest.BusinessTypeId, drmRequest.ComplaintCityId, drmRequest.ComplaintCityId, drmRequest.ComplaintCity, drmRequest.ChauffeurName, drmRequest.ChauffeurContactNo, drmRequest.CarVendorName, drmRequest.CarVendorContactNo, drmRequest.CarCatName, drmRequest.CarRegNo, drmRequest.CabId, string.IsNullOrEmpty(drmRequest.RMName) ? "NA" : drmRequest.RMName, string.IsNullOrEmpty(drmRequest.RMContactNo) ? "00" : drmRequest.RMContactNo, drmRequest.ComplaintMode, drmRequest.ComplaintTypeId, drmRequest.ComplaintSubTypeId, drmRequest.ComplaintDesc, Convert.ToDateTime(drmRequest.ComplaintDateTime), drmRequest.ComplaintRegisteredBy, drmRequest.ResolutionStatus, out ComplaintId, drmRequest.Calller, drmRequest.Misbehaviour_Abusive, drmRequest.BookingId, drmRequest.AlternateContactNo);
            //string CompID = ComplaintId;
            //if (Convert.ToInt32(CompID) > 0)
            //{
            //    objMail.SendMail(Convert.ToInt32(CompID), drmRequest.ComplaintRegisteredBy, drmRequest.SearchById, drmRequest.ComplaintCityId);
            //}
            //if (CompID.Contains("Err:"))
            //{
            //    ComplaintRes objComp = new ComplaintRes();
            //    objComp.ComplaintId = CompID;
            //    Json = objjson.ConvertObjecttoJSON(objComp);

            //}
            //else
            //{
            //    ComplaintRes objComp = new ComplaintRes();
            //    objComp.ComplaintId = "Complaint Registered with Complaint ID- " + drmRequest.ComplaintCityId + CompID;
            //    Json = objjson.ConvertObjecttoJSON(objComp);

            //}
           
            objCrm.CrmServiceRes = CrmId;
            Json = objjson.ConvertObjecttoJSON(objCrm);
        }
        catch (Exception ex)
        {
             Json = objjson.ConvertObjecttoJSON(ex.Message.ToString());
        }
        return objjson.sendJSONResponse(Json);
       
    }
    
}


