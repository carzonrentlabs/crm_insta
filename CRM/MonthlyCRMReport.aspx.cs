using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.CrystalReports.ViewerObjectModel;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;

public partial class CallCentre_MonthlyCRMReport : System.Web.UI.Page
{

    ReportDocument CReport = new ReportDocument();
    String Path;
    String From;
    String To;
    ConnectionClass objconncls = new ConnectionClass();

    private void ConfigureCrystalReports()
    {
        ConnectionInfo myConnectionInfo = new ConnectionInfo();

        myConnectionInfo.DatabaseName = objconncls.GetConfigConnection("d");
        myConnectionInfo.UserID = objconncls.GetConfigConnection("u");
        myConnectionInfo.Password = objconncls.GetConfigConnection("p");
        myConnectionInfo.ServerName = objconncls.GetConfigConnection("s");

        CReport = new ReportDocument();

        string reportPath = Server.MapPath("CRM_New.rpt");

        CReport.Load(reportPath);
        CRMMonthlyReport.ReportSource = CReport;
        SetDBLogonForReport(myConnectionInfo, CReport);
    }
    protected void Page_UnLoad(object sender, EventArgs e)
    {
        CReport.Close();
        CReport.Dispose();
    }

    private void SetDBLogonForReport(ConnectionInfo myConnectionInfo, ReportDocument myReportDocument)
    {
        Tables myTables = myReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table myTable in myTables)
        {
            TableLogOnInfo myTableLogonInfo = myTable.LogOnInfo;
            myTableLogonInfo.ConnectionInfo = myConnectionInfo;
            myTable.ApplyLogOnInfo(myTableLogonInfo);
        }
    }

    protected void Page_Init(object sender, System.EventArgs e)
    {


        ConfigureCrystalReports();
        if (!IsPostBack)
        {

            BindReport();

        }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
            
            BindReport();



    }
    protected void btnView_Click(object sender, EventArgs e)
    {
         
                Panel1.Visible = true;

                ConfigureCrystalReports();

                BindReport();
            
           

        


    }
    public void BindReport()
    {
      int ReportType;
        //int frommonth;
        //    int tomonth;
        //    int fromyear;       
        //    int toyear;
      if (!Page.IsPostBack)
      {
          txtFromDate.Text = DateTime.Now.ToShortDateString();
          txtToDate.Text = DateTime.Now.ToShortDateString();
      }
        DateTime FromDate;
        DateTime ToDate;
         ReportType=  Convert.ToInt32(ddlrptType.SelectedValue);
         FromDate = Convert.ToDateTime(txtFromDate.Text);
         ToDate = Convert.ToDateTime(txtToDate.Text);

         //frommonth = Convert.ToInt32(ddlMName.SelectedValue);
         // tomonth= Convert.ToInt32(DropDownList2.SelectedValue);
         //fromyear = Convert.ToInt32(ddlYear.SelectedValue);
         //toyear = Convert.ToInt32(DropDownList1.SelectedValue);
     
        Path = "CRM_New.rpt";
        string reportPath1 = Server.MapPath(Path);
        CReport.Load(reportPath1);
        CReport.SetParameterValue("@ReportType", ReportType);
        CReport.SetParameterValue("@FromDate", FromDate);
        CReport.SetParameterValue("@ToDate", ToDate);
        //CReport.SetParameterValue("@fromYear", fromyear);
        //CReport.SetParameterValue("@toyear", toyear);
        CRMMonthlyReport.ReportSource = CReport;
        // SetDBLogonForReport(myConnectionInfo, CReport);
        CRMMonthlyReport.DataBind();
        return;
    }
    public void ShowMessage(string message)
    {
        string strerrscript;
        strerrscript = "<script language='javascript'>alert('" + message + "');</script>";
        if ((!ClientScript.IsClientScriptBlockRegistered(strerrscript)))
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(String), "myScript", strerrscript);
        }
    } 
}
