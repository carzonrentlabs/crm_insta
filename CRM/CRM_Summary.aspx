<%@ Page Language="C#" MasterPageFile="~/CRDMasterPage.master" AutoEventWireup="true"  Theme="TMSTheme" CodeFile="CRM_Summary.aspx.cs" Inherits="CRM_CRM_Summary" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder_Page" Runat="Server">
<fieldset class="FieldsetClassic">
<legend class="LegendClassic" > CRM Summary </legend>
<table width="60%" style="font-size: 9pt; font-family: Tahoma">
    <tr>
        <td>
            Select Month :
            <asp:DropDownList ID="ddlMonth" runat="server" AutoPostBack="True" DataSourceID="ODS_For_ddlMonth" DataTextField="MonthName" DataValueField="MonthID">
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ODS_For_ddlMonth" runat="server" SelectMethod="LoadMonths" TypeName="MonthYear"></asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td  style="font-weight: bold; font-size: 10pt; color: #ffffff; font-family: Tahoma; background-color: tan" align="left">
            Todays Summary</td>
    </tr>
<tr>
<td>
    <asp:GridView ID="gvDaily" runat="server" Width="100%" DataSourceID="ODS_For_gvDaily">
    </asp:GridView>
    <asp:ObjectDataSource ID="ODS_For_gvDaily" runat="server" SelectMethod="List_CRMSummary" TypeName="CRM">
        <SelectParameters>
            <asp:SessionParameter Name="BranchID" SessionField="BranchID" Type="Int32" />
            <asp:ControlParameter ControlID="ddlMonth" Name="MonthID" PropertyName="SelectedValue"
                Type="Int32" />
            <asp:Parameter DefaultValue="1" Name="Type" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</td>
</tr>
    <tr>
        <td  style="font-weight: bold; font-size: 10pt; color: #ffffff; font-family: Tahoma; background-color: tan" align="left">
            Monthly Summary</td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="gvMonthly" runat="server" Width="100%" DataSourceID="ODS_For_gvMonthly">
            </asp:GridView>
            <asp:ObjectDataSource ID="ODS_For_gvMonthly" runat="server" SelectMethod="List_CRMSummary" TypeName="CRM">
                <SelectParameters>
                    <asp:SessionParameter Name="BranchID" SessionField="BranchID" Type="Int32" />
                    <asp:ControlParameter ControlID="ddlMonth" Name="MonthID" PropertyName="SelectedValue"
                        Type="Int32" />
                    <asp:Parameter DefaultValue="2" Name="Type" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td   style="font-weight: bold; font-size: 10pt; color: #ffffff; font-family: Tahoma; background-color: tan" align="left">
            Till Date Summary</td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="gvTillDate" runat="server" Width="100%" DataSourceID="ODS_For_gvTillDate">
            </asp:GridView>
            <asp:ObjectDataSource ID="ODS_For_gvTillDate" runat="server" SelectMethod="List_CRMSummary" TypeName="CRM">
                <SelectParameters>
                    <asp:SessionParameter Name="BranchID" SessionField="BranchID" Type="Int32" />
                    <asp:ControlParameter ControlID="ddlMonth" Name="MonthID" PropertyName="SelectedValue"
                        Type="Int32" />
                    <asp:Parameter DefaultValue="3" Name="Type" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td style="font-weight: bold; font-size: 10pt; color: #ffffff; font-family: Tahoma; background-color: tan" align="left">
            All open complaints</td>
    </tr>
    <tr>
        <td >
            <asp:GridView ID="gvOpen" runat="server" Width="100%" DataSourceID="ODS_For_gvOpen" AutoGenerateColumns="False" OnRowDataBound="gvOpen_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Level1 (0-48 hrs)">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Level1") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="l1"  style="cursor:pointer;" runat="server" ForeColor="Blue" Text='<%# Bind("Level1") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Level2 (49-96 hrs)">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Level2") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="l2" style="cursor:pointer;" runat="server" ForeColor="Blue" Text='<%# Bind("Level2") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Level3 (97-144 hrs)">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Level3") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="l3" style="cursor:pointer;" runat="server" ForeColor="Blue" Text='<%# Bind("Level3") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Level4 (above 144 hrs)">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Level4") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="l4"  style="cursor:pointer;" runat="server" ForeColor="Blue" Text='<%# Bind("Level4") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ODS_For_gvOpen" runat="server" SelectMethod="List_CRMSummary" TypeName="CRM">
                <SelectParameters>
                    <asp:SessionParameter Name="BranchID" SessionField="BranchID" Type="Int32" />
                    <asp:ControlParameter ControlID="ddlMonth" Name="MonthID" PropertyName="SelectedValue"
                        Type="Int32" />
                    <asp:Parameter DefaultValue="4" Name="Type" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
</table>
</fieldset>
</asp:Content>

