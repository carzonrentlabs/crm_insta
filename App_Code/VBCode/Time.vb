Imports Microsoft.VisualBasic

Public Class Time
    Public Function LoadHour() As ArrayList
        Dim AllHour As New ArrayList
        Dim i As Integer
        For i = 0 To 23
            AllHour.Add(i)

        Next i
        Return AllHour
    End Function

    Public Function LoadMinute() As ArrayList
        Dim AllMinute As New ArrayList
        Dim i As Integer
        For i = 0 To 59
            AllMinute.Add(i)

        Next i
        Return AllMinute
    End Function

End Class
