Imports Microsoft.VisualBasic
Imports System.Net.Mail
'Imports System.web.Mail

Public Class MailUtility
    'Public f As String
    'Public t As String
    Private mm As MailMessage

    Public Sub New(ByVal frAddr As String, ByVal toAddr As String)

        'f = frAddr
        't = toAddr
        mm = New MailMessage(frAddr, "tms@easycabs.com")
        Dim i As Integer
        i = 0
        Dim strMAilAddr As Array
        strMAilAddr = FormatMailAddress(toAddr)
        'Dim ma As New System.Net.Mail.MailAddress(strMAilAddr(0).ToString)
        While i < strMAilAddr.Length
            'ma.Address.Insert(i, strMAilAddr(i).ToString)
            mm.To.Add(strMAilAddr(i).ToString)
            i += 1
        End While


    End Sub

    Public Function FormatMailAddress(ByVal [EMail] As String) As Array
        Dim arr2 As String() = {""}
        If [EMail] <> "" Then
            Dim dt As String = [EMail]
            Dim arr1 As String() = dt.Split(";"c)

            Return arr1
        Else
            Return arr2
        End If
    End Function
    ' 
    Public Function SendMail(ByVal FrAddress As String, ByVal ToAddress As String, ByVal Subject As String, ByVal Body As String, ByVal BccAddress As String, ByVal CcAddress As String) As Boolean
        Try
            'mm.To = ToAddress
            'mm.From = FrAddress
            'mm.Cc = CcAddress
            'mm.Bcc = BccAddress
            mm.IsBodyHtml = True
            mm.Priority = MailPriority.Normal
            mm.Subject = Subject
            mm.Body = Body

            'If (CcAddress <> "") Then
            '    mm.CC.Add(CcAddress)
            'End If
            Dim c As Integer
            c = 0
            Dim strMAilcAddr As Array
            strMAilcAddr = FormatMailAddress(CcAddress)
            'Dim ma As New System.Net.Mail.MailAddress(strMAilAddr(0).ToString)
            While c < strMAilcAddr.Length
                'ma.Address.Insert(i, strMAilAddr(i).ToString)
                If (strMAilcAddr(c).ToString <> "") Then
                    mm.CC.Add(strMAilcAddr(c).ToString)
                End If
                c += 1
            End While

            'If (BccAddress <> "") Then
            '    mm.Bcc.Add(BccAddress)
            'End If


            Dim b As Integer
            b = 0
            Dim strMAilbAddr As Array
            strMAilbAddr = FormatMailAddress(BccAddress)
            'Dim ma As New System.Net.Mail.MailAddress(strMAilAddr(0).ToString)
            While b < strMAilbAddr.Length
                'ma.Address.Insert(i, strMAilAddr(i).ToString)
                If (strMAilbAddr(b).ToString <> "") Then
                    mm.Bcc.Add(strMAilbAddr(b).ToString)
                End If
                b += 1
            End While


            Dim ss As New System.Net.Mail.SmtpClient
            ss.Host = "mail.easycabs.com"
            ss.Send(mm)

            Return True
        Catch exMail As Exception
            Return False
        End Try
    End Function
End Class
