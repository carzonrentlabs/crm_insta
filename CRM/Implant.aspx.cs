﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ExcelUtil;
using System.Collections;
using System.Text;
using System.Data.SqlClient;
public partial class CRM_Implant : System.Web.UI.Page
{
    ImplantBAL objBal = new ImplantBAL();
    DataSet dsExportToExcel = new DataSet();
    ArrayList list=new ArrayList();
    StringBuilder builder = new StringBuilder();
    string CheckCompetitor = string.Empty;
    string msgComp = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        lblMsg.Text = string.Empty;
       // Session["Userid"] = 3122;
        if (!Page.IsPostBack)
        {
            BindCity();
            BindClient();
                  
        }
    }
    public void BindCity()
    {
        DataSet dsLocation = new DataSet();
        try
        {
            dsLocation = objBal.GetLocations(Convert.ToInt32(Session["Userid"]));
            if (dsLocation.Tables.Count > 0)
            {
                //ddlCityName.DataTextField = "Cityname";
                //ddlCityName.DataValueField = "CityId";
                //ddlCityName.DataSource = dsLocation;
                //ddlCityName.DataBind();

                ddlReportCity.DataTextField = "Cityname";
                ddlReportCity.DataValueField = "CityId";
                ddlReportCity.DataSource = dsLocation;
                ddlReportCity.DataBind();
                

                
            }
            
        }
        catch (Exception ex)
        { 
            lblMsg.Text=ex.Message.ToString();
        }
                
    }
    public void BindClient()
    {
        DataSet dsClient = new DataSet();
        try
        {
            dsClient = objBal.GetClientImplantCompetitor();
            if (dsClient.Tables[0].Rows.Count > 0)
            {
                ddlCmpName.DataTextField = "ClientCoName";
                ddlCmpName.DataValueField = "ClientCoID";
                ddlCmpName.DataSource = dsClient.Tables[0];
                ddlCmpName.DataBind();

                ddlReportCmp.DataTextField = "ClientCoName";
                ddlReportCmp.DataValueField = "ClientCoID";
                ddlReportCmp.DataSource = dsClient.Tables[0];
                ddlReportCmp.DataBind();
                
                
                //if (dsClient.Tables[1].Rows.Count > 0)
                //{
                //    //ddlCompetitorName.DataTextField = "Competitor";
                //    //ddlCompetitorName.DataValueField = "CompetitorId";
                //    //ddlCompetitorName.DataSource = dsClient.Tables[1];
                //    //ddlCompetitorName.DataBind();
                //}
                if (dsClient.Tables[1].Rows.Count > 0)
                {
                    ddlImplant.DataTextField = "ImplantName";
                    ddlImplant.DataValueField = "implantSysUserid";
                    ddlImplant.DataSource = dsClient.Tables[1];
                    ddlImplant.DataBind();
                    
                }
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }

    }
   
    protected void btnNewReg_Click(object sender, EventArgs e)
    {
        string msg=string.Empty,ComBooking1=string.Empty,ComBooking2=string.Empty,ComBooking3=string.Empty,ComBooking4=string.Empty,ComBooking5=string.Empty;
        try
        {
            if (ddlCompetitorName.SelectedIndex == 0)
            {
                lblMsg.Text = "Please Select Competitor";
                return;
            }
            if (ddlImplantName.SelectedIndex == 0)
            {
                lblMsg.Text = "Please Select Implant";
                return;
            }
            if (pnlComp1.Visible == true)
            {
                 ComBooking1 = lblComBooking1.Text + "-" + txtCompetitorBooking1.Text;
            }
            if (pnlComp2.Visible == true)
            {
                 ComBooking2 = lblComBooking2.Text + "-" + txtCompetitorBooking2.Text;
            }
            if (pnlComp3.Visible == true)
            {
                 ComBooking3 = lblComBooking3.Text + "-" + txtCompetitorBooking3.Text;
            }
            if (pnlComp4.Visible == true)
            {
                 ComBooking4 = lblComBooking4.Text + "-" + txtCompetitorBooking4.Text;
            }
            if (pnlComp5.Visible == true)
            {
                 ComBooking5 = lblComBooking5.Text + "-" + txtCompetitorBooking5.Text;
            }
            string BookingIdComplaintResolution = string.Empty, CityWiseStatus=string.Empty;

            if (GvResolution.Rows.Count > 0)
            {
                foreach (GridViewRow item in GvResolution.Rows)
                {
                    //string CityName = item.Cells[0].Text.ToString();
                    int BookingId = Convert.ToInt32(item.Cells[1].Text);
                    TextBox txtRes = (TextBox)item.FindControl("txtResolution");
                    BookingIdComplaintResolution += BookingId + "-" + txtRes.Text.ToString() + ",";
                }
                BookingIdComplaintResolution = BookingIdComplaintResolution.TrimEnd(',');
            }
          
            if (GvFeedBack.Rows.Count > 0)
            {
                foreach (GridViewRow item in GvFeedBack.Rows)
                {
                    string  CityName =item.Cells[0].Text.ToString();
                    string  Total =item.Cells[1].Text.ToString();
                    string  Status =item.Cells[2].Text.ToString();
                    CityWiseStatus+= CityName + "-" + Total+ "-"+Status+ ",";
                   
                }
                CityWiseStatus = CityWiseStatus.TrimEnd(',');
            }
          
            string CompetitorBooking = string.IsNullOrEmpty(ComBooking1) ? "0" : ComBooking1 + "," + Convert.ToString(string.IsNullOrEmpty(ComBooking2) ? "0" : ComBooking2) + "," + Convert.ToString(string.IsNullOrEmpty(ComBooking3) ? "0" : ComBooking3) + "," + Convert.ToString(string.IsNullOrEmpty(ComBooking4) ? "0" : ComBooking4) + "," + Convert.ToString(string.IsNullOrEmpty(ComBooking5) ? "0" : ComBooking5);
            msg = objBal.SaveImplantPerformance(Convert.ToInt32(ddlCmpName.SelectedValue), Convert.ToInt32(ddlImplantName.SelectedValue), Convert.ToInt32(ddlCityName.SelectedValue), Convert.ToDateTime(txtDate.Text), Convert.ToInt32(txtCorBooking.Text), Convert.ToInt32(ddlCompetitorName.SelectedValue), Convert.ToString(ddlCompetitorName.SelectedItem.Text), CompetitorBooking.ToString(), Convert.ToInt32(txtBookingTakenByComplant.Text), Convert.ToInt32(txtBookingMDT.Text), Convert.ToInt32(txtComplaintCount.Text), Convert.ToInt32(txtSMSFeedbackCount.Text), Convert.ToInt32(txtPerformance.Text), Convert.ToInt32(txtClientPerformance.Text), Convert.ToInt32(txtComplaintCountInstaCRM.Text), lblFeedback.Text, txtAction.Text, Convert.ToInt32(lblQualityOfCab.Text), Convert.ToInt32(lblChauffeurComplaint.Text), Convert.ToInt32(lblServiceFailure.Text), Convert.ToInt32(lblSOSComplaint.Text), Convert.ToInt32(lblBilling.Text), BookingIdComplaintResolution, CityWiseStatus, Convert.ToInt32(Session["Userid"]));
            
        }
        catch (Exception ex)
        {
            msg = ex.Message.ToString();
        }
        lblMsg.Text = msg;
    }
    protected void txtDate_TextChanged(object sender, EventArgs e)
    {
        ClearControl();
        BookingByCLientAndImplant();
        GetClientComplaintAndFeedback();
        GetSMSFeedBackCountByClient();
        GetFeedTypeByClient();
        GetComplaintTypeByClient();
        GetBookingByClientForResolution();
        
       
    }
    public void BookingByCLientAndImplant()
    {
        DataSet dsBooking = new DataSet();
        try
        {
            dsBooking = objBal.GetBookingByClient(Convert.ToInt32(ddlCmpName.SelectedValue), Convert.ToInt32(ddlCityName.SelectedValue), Convert.ToDateTime(txtDate.Text), Convert.ToInt32(ddlImplantName.SelectedValue));
            if (dsBooking.Tables.Count > 0)
            {
                txtCorBooking.Text = dsBooking.Tables[0].Rows[0]["TotalBooking"].ToString();
                txtBookingMDT.Text = dsBooking.Tables[0].Rows[1]["TotalBooking"].ToString();
                txtBookingTakenByComplant.Text = dsBooking.Tables[0].Rows[2]["TotalBooking"].ToString();
                txtPerformance.Text = dsBooking.Tables[0].Rows[3]["TotalBooking"].ToString();
                txtClientPerformance.Text = dsBooking.Tables[0].Rows[4]["TotalBooking"].ToString();
                lblBookingTakenByImplant.Text = Convert.ToDateTime(txtDate.Text).AddDays(-1).ToShortDateString();
                lblClientPerformance.Text = Convert.ToDateTime(txtDate.Text).AddDays(-1).ToShortDateString();
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
    }
    public void GetClientComplaintAndFeedback()
    {
        DataSet dsComplaintFeedBack = new DataSet();
        try
        {
            dsComplaintFeedBack = objBal.GetBookingByClient(Convert.ToInt32(ddlCmpName.SelectedValue), Convert.ToInt32(ddlCityName.SelectedValue), Convert.ToDateTime(txtDate.Text));
            if (dsComplaintFeedBack.Tables.Count > 0)
            {
                txtComplaintCount.Text = dsComplaintFeedBack.Tables[0].Rows[0]["TotalComplaint"].ToString();
              
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
    }

    public void GetSMSFeedBackCountByClient()
    {
        DataSet dsGetFeedBackCountByClient = new DataSet();
        try
        {
            dsGetFeedBackCountByClient = objBal.GetFeedBackCountByClient(Convert.ToInt32(ddlCmpName.SelectedValue), Convert.ToInt32(ddlCityName.SelectedValue), Convert.ToDateTime(txtDate.Text));
            if (dsGetFeedBackCountByClient.Tables.Count > 0)
            {
                txtSMSFeedbackCount.Text = dsGetFeedBackCountByClient.Tables[0].Rows[0]["TotalSMSFeedBack"].ToString();
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
    }
    public void GetFeedTypeByClient()
    {
        DataSet dsGetFeedTypeByClient = new DataSet();
        try
        {
            dsGetFeedTypeByClient = objBal.GetFeedbackTypeByClient(Convert.ToInt32(ddlCmpName.SelectedValue), Convert.ToInt32(ddlCityName.SelectedValue), Convert.ToDateTime(txtDate.Text));
           // dsGetFeedTypeByClient = objBal.GetFeedbackTypeByClient(Convert.ToInt32("2205"), Convert.ToInt32(ddlCityName.SelectedValue), Convert.ToDateTime("06-24-2016"));
            if(dsGetFeedTypeByClient.Tables[0].Rows.Count > 0)
            {
                lblFeedback.Text = dsGetFeedTypeByClient.Tables[1].Rows.Count.ToString();
               // lblFeedback.Text = dsGetFeedTypeByClient.Tables[0].Rows[0]["FeedbackStatus"].ToString();

                if (dsGetFeedTypeByClient.Tables[1].Rows.Count > 0)
                {
                    GvFeedBack.DataSource = dsGetFeedTypeByClient.Tables[1];
                    GvFeedBack.DataBind();
                }
                else
                {
                    GvFeedBack.DataSource = null;
                    GvFeedBack.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
    }

    public void GetComplaintTypeByClient()
    {
        DataSet dsGetComplaintTypeByClient = new DataSet();
        try
        {
            dsGetComplaintTypeByClient = objBal.GetComplaintTypeByClient(Convert.ToInt32(ddlCmpName.SelectedValue), Convert.ToInt32(ddlCityName.SelectedValue), Convert.ToDateTime(txtDate.Text));
            //dsGetComplaintTypeByClient = objBal.GetComplaintTypeByClient(Convert.ToInt32("2205"), Convert.ToInt32(ddlCityName.SelectedValue), Convert.ToDateTime("06-24-2016"));
            if (dsGetComplaintTypeByClient.Tables[0].Rows.Count > 0)
            {
                txtComplaintCountInstaCRM.Text = dsGetComplaintTypeByClient.Tables[0].Rows[0]["TotalComplaint"].ToString();
                if (dsGetComplaintTypeByClient.Tables[1].Rows.Count > 0)
                {
                    lblQualityOfCab.Text = dsGetComplaintTypeByClient.Tables[1].Rows[0]["QualityOfCab"].ToString();
                    lblChauffeurComplaint.Text = dsGetComplaintTypeByClient.Tables[1].Rows[0]["ChauffeurComplaints"].ToString();
                    lblServiceFailure.Text = dsGetComplaintTypeByClient.Tables[1].Rows[0]["ServiceFailure"].ToString();
                                       
                    lblSOSComplaint.Text = dsGetComplaintTypeByClient.Tables[1].Rows[0]["SOSComplaint"].ToString();
                    lblBilling.Text = dsGetComplaintTypeByClient.Tables[1].Rows[0]["InvoiceComplaint"].ToString();
                    trFeedbackDetails.Visible = true;
                }
                
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
    }

    public void GetBookingByClientForResolution()
    {
        DataSet dsGetBookingByClientForResolution = new DataSet();
        try
        {
            dsGetBookingByClientForResolution = objBal.GetBookingByClientForResolution(Convert.ToInt32(ddlCmpName.SelectedValue), Convert.ToDateTime(txtDate.Text));
            //dsGetBookingByClientForResolution = objBal.GetBookingByClientForResolution(Convert.ToInt32("2205"), Convert.ToDateTime("06-24-2016"));
            if (dsGetBookingByClientForResolution.Tables[0].Rows.Count > 0)
            {
                GvResolution.DataSource = dsGetBookingByClientForResolution.Tables[0];
                GvResolution.DataBind();
            }
            else
            {
                GvResolution.DataSource = null;
                GvResolution.DataBind();
            }

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
    }

    protected void ddlCmpName_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet dsImplantDetails = new DataSet();
        DataSet dsCompetitor = new DataSet();
        try
        {
           
            ClearControl();
            GvFeedBack.DataSource = null;
            GvFeedBack.DataBind();
            GvResolution.DataSource = null;
            GvResolution.DataBind();
            txtDate.Text = string.Empty;
            //ddlCityName.DataSource = null;
            //ddlCityName.DataBind();
            pnlComp1.Visible = false;
            lblComBooking1.Text = string.Empty;
            pnlComp1.Visible = false;
            lblComBooking2.Text = string.Empty;
            pnlComp2.Visible = false;
            lblComBooking3.Text = string.Empty;
            pnlComp3.Visible = false;
            lblComBooking4.Text = string.Empty;
            pnlComp4.Visible = false;
            lblComBooking5.Text = string.Empty;
            pnlComp5.Visible = false;

            dsImplantDetails = objBal.GetImplantDetails(Convert.ToInt32(ddlCmpName.SelectedValue));
            if(dsImplantDetails.Tables[0].Rows.Count > 0)
            {
                ddlImplantName.DataTextField = "ImplantName";
                ddlImplantName.DataValueField = "implantSysUserid";
                ddlImplantName.DataSource = dsImplantDetails.Tables[0];
                ddlImplantName.DataBind();
                
                if (dsImplantDetails.Tables[1].Rows.Count > 0)
                {
                    ddlCompetitorName.DataTextField = "CompetitorName";
                    ddlCompetitorName.DataValueField = "CompetitorId";
                    ddlCompetitorName.DataSource = dsImplantDetails.Tables[1];
                    ddlCompetitorName.DataBind();
                    btnCompetitor.Visible = true;

                }
                
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
    }

    public void ClearControl()
    {
        txtCorBooking.Text = string.Empty;
        txtBookingMDT.Text =string.Empty;
        txtBookingTakenByComplant.Text =string.Empty;
        txtPerformance.Text = string.Empty;
        txtComplaintCount.Text=string.Empty;
        txtSMSFeedbackCount.Text=string.Empty;
        txtClientPerformance.Text = string.Empty;
        txtComplaintCountInstaCRM.Text = string.Empty;
        lblFeedback.Text = string.Empty;
        lblQualityOfCab.Text=string.Empty;
        lblChauffeurComplaint.Text=string.Empty;
        lblServiceFailure.Text=string.Empty;
        lblSOSComplaint.Text=string.Empty;
        lblBilling.Text = string.Empty;
       
    }
    protected void ddlCompetitorName_SelectedIndexChanged(object sender, EventArgs e)
    {
       // ViewState["Competitor"] = null;
        if (ddlCompetitorName.SelectedIndex != 0)
        {
            if (string.IsNullOrEmpty(lblComBooking1.Text) == true)
            {
                lblComBooking1.Text = ddlCompetitorName.SelectedItem.Text;
                pnlComp1.Visible = true;
            }
           
        }
        else
        {
            lblComBooking1.Text = string.Empty;
            pnlComp1.Visible = false;

            lblComBooking2.Text = string.Empty;
            pnlComp2.Visible = false;
            lblComBooking3.Text = string.Empty;
            pnlComp3.Visible = false;
            lblComBooking4.Text = string.Empty;
            pnlComp4.Visible = false;
            lblComBooking5.Text = string.Empty;
            pnlComp5.Visible = false;
                
        }
    }
    protected void btnNext2_Click(object sender, EventArgs e)
    {
      
       // builder =(StringBuilder) ViewState["Competitor"];
        CheckCompetitor= lblComBooking1.Text;
        string[] check = CheckCompetitor.Split(',');
        //if (builder.ToString().Contains(ddlCompetitorName.SelectedItem.Text))
        for (int i = 0; i < check.Length; i++)
        {
            if (check[i].ToString() == ddlCompetitorName.SelectedItem.Text)
            {
                lblMsg.Text = "Sorry Can not add same Competitor!";
                return;
            }
        }
     
        lblComBooking2.Text = ddlCompetitorName.SelectedItem.Text;
        pnlComp2.Visible = true;
    }
    protected void btnNext3_Click(object sender, EventArgs e)
    {
        
       // builder = (StringBuilder)ViewState["Competitor"];
        CheckCompetitor= lblComBooking1.Text +","+ lblComBooking2.Text;
        string[] check = CheckCompetitor.Split(',');
        //if (builder.ToString().Contains(ddlCompetitorName.SelectedItem.Text))
        for (int i = 0; i < check.Length; i++)
        {
            if (check[i].ToString() == ddlCompetitorName.SelectedItem.Text)
            {
                lblMsg.Text = "Sorry Can not add same Competitor!";
                return;
            }
        }
        //if (builder.ToString().Contains(lblComBooking2.Text.Trim()))
        //{
        //    lblMsg.Text = "Sorry Can not add same Competitor!";
        //    return;
        //}
        lblComBooking3.Text = ddlCompetitorName.SelectedItem.Text;
        pnlComp3.Visible = true;
    }
    protected void btnNext4_Click(object sender, EventArgs e)
    {
      
        CheckCompetitor = lblComBooking1.Text + "," + lblComBooking2.Text + "," + lblComBooking3.Text;
        string[] check = CheckCompetitor.Split(',');
        for (int i = 0; i < check.Length; i++)
        {
            if (check[i].ToString() == ddlCompetitorName.SelectedItem.Text)
            {
                lblMsg.Text = "Sorry Can not add same Competitor!";
                return;
            }
        }
        lblComBooking4.Text = ddlCompetitorName.SelectedItem.Text;
        pnlComp4.Visible = true;
    }
    protected void btnNext5_Click(object sender, EventArgs e)
    {
        CheckCompetitor = lblComBooking1.Text + "," + lblComBooking2.Text + "," + lblComBooking3.Text + "," + lblComBooking4.Text;
        string[] check = CheckCompetitor.Split(',');
        for (int i = 0; i < check.Length; i++)
        {
            if (check[i].ToString() == ddlCompetitorName.SelectedItem.Text)
            {
                lblMsg.Text = "Sorry Can not add same Competitor!";
                return;
            }
        }
        lblComBooking5.Text = ddlCompetitorName.SelectedItem.Text;
        pnlComp4.Visible = true;
    }
    protected void btnRemove2_Click(object sender, EventArgs e)
    {
        lblComBooking2.Text = string.Empty;
        pnlComp2.Visible = false;
        CheckCompetitor = string.Empty;
    }
    protected void btnRemove3_Click(object sender, EventArgs e)
    {
        lblComBooking3.Text = string.Empty;
        pnlComp3.Visible = false;
        CheckCompetitor = string.Empty;
    }
    protected void btnRemove4_Click(object sender, EventArgs e)
    {
        lblComBooking4.Text = string.Empty;
        pnlComp4.Visible = false;
        CheckCompetitor = string.Empty;
    }
    protected void btnRemove5_Click(object sender, EventArgs e)
    {
        lblComBooking5.Text = string.Empty;
        pnlComp5.Visible = false;
        CheckCompetitor = string.Empty;
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        dsExportToExcel = objBal.GetImplantDetails(Convert.ToInt32(ddlReportCmp.SelectedValue), Convert.ToInt32(ddlReportCity.SelectedValue), Convert.ToInt32(ddlImplant.SelectedValue), Convert.ToDateTime(txtFromDate.Text),Convert.ToDateTime(txtToDate.Text));
        WorkbookEngine.ExportDataSetToExcel(dsExportToExcel, "ImplantPerformanceReport" + ".xls");
    }
    protected void ddlImplantName_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet dsImplantCity = new DataSet();
        dsImplantCity = objBal.GetImplantCity(Convert.ToInt32(ddlImplantName.SelectedValue));
        if (dsImplantCity.Tables[0].Rows.Count > 0)
        {
            ddlCityName.DataTextField = "CityName";
            ddlCityName.DataValueField = "CityID";
            ddlCityName.DataSource = dsImplantCity.Tables[0];
            ddlCityName.DataBind();
           
        }
    }
     
    protected void btnAddCompetitor_Click1(object sender, EventArgs e)
    {
        SqlDataReader rdrResult; string Output = string.Empty;
        DataSet dsImplant = new DataSet();
        try
        {
            rdrResult = objBal.SaveCompetitor(Convert.ToInt32(ddlCmpName.SelectedValue), ddlCmpName.SelectedItem.Text.Trim().ToString(), txtCompetitor.Text.Trim(), Convert.ToInt32(Session["Userid"]));
            if (rdrResult.HasRows == true)
            {
                while (rdrResult.Read())
                {
                    Output = rdrResult["SqlResult"].ToString();
                }
                if (Output == "Success")
                {
                    dsImplant = objBal.GetImplantDetails(Convert.ToInt32(ddlCmpName.SelectedValue));
                    if (dsImplant.Tables[1].Rows.Count > 0)
                    {
                        if (dsImplant.Tables[1].Rows.Count > 0)
                        {
                            ddlCompetitorName.DataTextField = "CompetitorName";
                            ddlCompetitorName.DataValueField = "CompetitorId";
                            ddlCompetitorName.DataSource = dsImplant.Tables[1];
                            ddlCompetitorName.DataBind();
                            btnCompetitor.Visible = true;

                        }

                    }
                    tblAddComp.Visible = false;
                    lblMsg.Text = "Competitor Added Successfully";

                }
                else
                {
                    lblMsg.Text = "Competitor Not Added Successfully";
                    tblAddComp.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
    }
    protected void btnCompetitor_Click(object sender, EventArgs e)
    {
        tblAddComp.Visible = true;
    }
}