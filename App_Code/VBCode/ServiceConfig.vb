Imports Microsoft.VisualBasic

Imports System
Imports System.Net
Namespace NKUtilities

    Public Class DNSUtility
        Public Function ServerAddress() As String

            Dim strHostName As New String("")
            Dim add As New String("")

            'If args.Length = 0 Then
            ' Getting Ip address of local machine... 
            ' First get the host name of local machine. 
            strHostName = Dns.GetHostName()
            Console.WriteLine("Local Machine's Host Name: " + strHostName)
            'Else
            'strHostName = args(0)
            'End If

            ' Then using host name, get the IP address list.. 
            Dim ipEntry As IPHostEntry = Dns.GetHostEntry(strHostName)
            Dim addr As IPAddress() = ipEntry.AddressList
            For i As Integer = 0 To addr.Length - 1
                add = add + addr(i).ToString() + "."
                'Console.WriteLine("IP Address {0}: {1} ", i, addr(i).ToString())
            Next
            Return add.Remove(add.Length - 1, 1)
        End Function
    End Class
End Namespace
