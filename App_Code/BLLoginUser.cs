using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Insta;
using System.Data.SqlClient;
/// <summary>
/// Summary description for LoginUser
/// </summary>
public class BLLoginUser
{
    public OL_LoginUser ValidateUser(OL_LoginUser objLoginUserOL)
    {
        try
        {
            DataTable  dtLoginUser = new DataTable();
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@LoginID", objLoginUserOL.LoginId);
            param[1] = new SqlParameter("@Password", objLoginUserOL.Password);
            dtLoginUser = SqlHelper.ExecuteDatatable("SP_AuthenticateUser", param);
            if (dtLoginUser.Rows.Count > 0)
            {
                objLoginUserOL.SysUserId = Convert.ToInt32(dtLoginUser.Rows[0]["SysUserID"].ToString());
                objLoginUserOL.LoginId = objLoginUserOL.LoginId.ToString();
                objLoginUserOL.LoginStatus = "valid";
            }
            else
            {
                objLoginUserOL.LoginId = "0";
                objLoginUserOL.LoginStatus = "Invalid";
            }
            return objLoginUserOL;
        }
        catch (Exception Ex)
        {
            
            throw new Exception(Ex.Message);
        }

    }
}
