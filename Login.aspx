﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Carzonrent:Login page </title>

    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>

    <script src="ScriptsReveal/Popup.js" type="text/javascript"></script>

    <link href="CSS/login.css" type="text/css" rel="stylesheet" />
    <link href="CSSReveal/reveal.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 8]>
<link href="CSS/login_ie8.css" type="text/css" rel="stylesheet" />
<![endif]-->
    <style type="text/css">
        input
        {
            border-radius: 8px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            -o-border-radius: 8px;
            behavior: url(border-radius.htc);
        }
        input[type="button"]
        {
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -o-border-radius: 5px;
            behavior: url(border-radius.htc);
        }
        .login_area
        {
            border-radius: 10px;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            -o-border-radius: 10px;
            behavior: url(border-radius.htc);
            border: none;
            outline: none;
        }
        .top_heading
        {
            border-top-left-radius: 7px;
            border-top-right-radius: 7px;
            -moz-border-top-left-radius: 7px;
            -moz-border-top-right-radius: 7px;
            -khtml-border-top-left-radius: 7px;
            -khtml-border-top-right-radius: 7px;
            -webkit-border-top-left-radius: 7px;
            -webkit-border-top-right-radius: 7px;
            -o-border-top-left-radius: 7px;
            -o-border-top-right-radius: 7px;
            behavior: url(PIE.htc);
        }
    </style>

    <script type="text/javascript">
        function WaterMark(txtName, event) {
            var defaultText = "Enter login id";
            // Condition to check textbox length and event type
            if (txtName.value.length == 0 & event.type == "blur") {
                //if condition true then setting text color and default text in textbox
                txtName.style.color = "Gray";
                txtName.value = defaultText;
            }
            // Condition to check textbox value and event type
            if (txtName.value == defaultText & event.type == "focus") {
                txtName.style.color = "black";
                txtName.value = "";
            }
        }
        function WaterMarkPassword(txtName, event) {
            var defaultText = "Enter password";
            // Condition to check textbox length and event type
            if (txtName.value.length == 0 & event.type == "blur") {
                //if condition true then setting text color and default text in textbox
                txtName.style.color = "Gray";
                txtName.value = defaultText;
            }
            // Condition to check textbox value and event type
            if (txtName.value == defaultText & event.type == "focus") {
                txtName.style.color = "black";
                txtName.value = "";
            }
        }
        $(document).ready(function () {
            $("#bntLogin").click(function () {
                $.ajax({
                    type: "GET",
                    cache: false,
                    url: "HTTPHandler/LoginUser.ashx",
                    data: { username: $("#txtLoginId").val(), Password: $("#txtPassword").val() },
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (response) {
                        if (response == "valid") {
                            location.href = "Sales/Implant.aspx"
                        }
                        else {
                            alert(response)
                            $("#txtLoginId").val("");
                            $("#txtPassword").val("");
                            $("#txtLoginId").focus();
                        }
                    },
                    error: function (msg) {
                        alert(msg.responseText);
                    }
                });
                return false;
            });
        });

        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td colspan="2" align="left">
                        <a href="">
                            <img src="App_Images/logo_carzonret.gif" border="0" style="height: 58px; width: 100px" alt="" />
                        </a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="login_area">
            <div class="top_heading">
                Login</div>
            <center>
                <div class="padding5">
                    <table cellpadding="3" cellspacing="3">
                        <tr>
                            <td align="left">
                                Login ID
                            </td>
                            <td align="left" class="pl5">
                                Password
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" id="txtLoginId" size="20" maxlength="50" value="Enter login id"
                                    onblur="WaterMark(this, event);" onfocus="WaterMark(this, event);" />
                            </td>
                            <td class="pl5">
                                <input type="password" id="txtPassword" size="20" maxlength="50" value="Enter password"
                                    onblur="WaterMarkPassword(this, event);" onfocus="WaterMarkPassword(this, event);"
                                    onkeypress="return clickButton(event,'bntLogin');" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="lh">
                                <!-- <input type="radio" name="" id="" value=""> Remember me -->
                            </td>
                            <td align="left" class="pl5">
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="middle" colspan="2">
                                <input type="button" id="bntLogin" value="Login" class="button_quote" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width: 400px" id="myModal" class="reveal-modal">
                    <fieldset style="border-color: Green">
                        <legend><b>Password Recovery</b></legend>
                        <table>
                            <tr>
                                <td colspan="2">
                                    <h2>
                                        Password Recovery
                                    </h2>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Use the form below to Reset your password.
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 22px">
                                    Login ID: &nbsp;
                                    <input type="text" id="txtLoginIdRecovery" size="20" maxlength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <input type="button" id="ResetPasswordButton" value="Reset Password" class="button_quote"
                                        runat="server" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <a class="close-reveal-modal" title="click to close"><b>&#215;</b></a>
                </div>
            </center>
        </div>
    </form>
</body>
</html>
