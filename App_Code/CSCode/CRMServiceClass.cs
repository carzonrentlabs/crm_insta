using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for CRMServiceClass
/// </summary>
public class CRMServiceClass
{
            //BranchID
            //, int.Parse(ddlJobLocation.SelectedValue.ToString())
            //, txtJobID.Text.Trim()
            //, Convert.ToDateTime(txtJobDate.Text.Trim())
            //, txtJobFrom.Text.Trim()
            //, txtJobTo.Text.Trim()
            //, txtCustName.Text.Trim()
            //, txtCustContact.Text.Trim() != "" ? txtCustContact.Text.Trim() : "0"
            //, ""
            //, txtRemarks.Text.Trim()
            //, CabID
            //, ChauffeurName
            //, ChauffeurContact == "" ? "0" : ChauffeurContact
            //, int.Parse(ddlCompSrc.SelectedValue.ToString())
            //, ComplaintDateTime
            //, txtCompDesc.Text.Trim()
            //, RegisteredBy
            //, Reg_DateTime
            //, int.Parse(ddlResolution.SelectedValue.ToString())
            //, int.Parse(ddlPriority.SelectedValue.ToString())
            //, 8
            //, lbl_CarCat.Text.Trim()
            //, int.Parse(ddlComplaintSubCategory.SelectedValue.ToString())
            //, txtEmailId.Text.Trim()
            //, txtCabNo.Text.Trim()
    private int _BranchId;
    private int _JobLocationId;
    private string _JobNo;
    private string  _JobDate;
    private string _Job_From;
    private string _Job_To;
    private string _CustomerName;
    private string _CustomerContact;
    private string _ContactDetails;
      private string _Remarks;
      private string _CabID;
      private string _ChauffeurName;
      private string _ChauffeurContact;
      private int _ComplaintSourceID;
      private string _ComplaintDateTime;
      private string _ComplaintDescription;
      private string _RegisteredBy;
      private  string _Reg_DateTime;
      private int _ResolutionAt;
      private int _PriorityID;
      private int _ComplaintStatus;
      private string _CarCategory;
    private int _SubCategoryId;
    private string _guestEmailId;
    private string _CarNo;

    private string _TotalUsageTime;
    private string _TotalUsageKms;
    private string _TotalTollParking;
    private string _Concern;
    private string _ConcernText;
	public CRMServiceClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public int BranchID 
    {
        get { return _BranchId; }
        set { _BranchId = value; }
    }
    public int JobLocationId
    {
        get { return _JobLocationId; }
        set { _JobLocationId = value; }
    }
    public string JobNo
    {
        get { return _JobNo; }
        set { _JobNo = value; }
    }
    public string JobDate
    {
        get { return _JobDate; }
        set { _JobDate = value; }
    }
    public string Job_From
    {
        get{return _Job_From;}
        set { _Job_From = value; }
    }
    public string Job_To
    {
        get { return _Job_To; }
        set { _Job_To = value; }
    }
    public string CustomerName
    {
        get { return _CustomerName; }
        set { _CustomerName = value; }
    }
    public string CustomerContact
    {
        get {return _CustomerContact; }
        set {_CustomerContact=value; }
    }
    public string ContactDetails
    {
        get { return _ContactDetails; }
        set {_ContactDetails=value; }
    }

    public string Remarks
    {
        get { return _Remarks; }
        set {_Remarks=value; }
    }
    public string CabID
    {
        get { return _CabID; }
        set {_CabID=value; }
    }
    public string ChauffeurName
    {
        get { return _ChauffeurName; }
        set {_ChauffeurName=value; }
    }
    public string ChauffeurContact
    {
        get { return _ChauffeurContact; }
        set { _ChauffeurContact=value; }
    }
    public int ComplaintSourceID
    {
        get { return _ComplaintSourceID; }
        set {_ComplaintSourceID=value; }
    }
    public string ComplaintDateTime
    {
        get { return _ComplaintDateTime; }
        set { _ComplaintDateTime = value; }
    }
    public string ComplaintDescription
    {
        get { return _ComplaintDescription; }
        set { _ComplaintDescription=value; }
    }
    public string RegisteredBy
    {
        get { return _RegisteredBy; }
        set { _RegisteredBy = value; }
    }
    public string Reg_DateTime
    {
        get { return _Reg_DateTime;}
        set { _Reg_DateTime = value; }
    }
    public int ResolutionAt
    {
        get { return _ResolutionAt; }
        set { _ResolutionAt=value; }
    }
    public int PriorityID
    {
        get { return _PriorityID; }
        set {_PriorityID=value; }
    }
    public int ComplaintStatus
    {
        get { return _ComplaintStatus; }
        set { _ComplaintStatus=value; }
    }
    public string CarCategory
    {
        get { return _CarCategory; }
        set {_CarCategory=value; }
    }
    public int SubCategoryId
    {
        get { return _SubCategoryId; }
        set {_SubCategoryId=value; }
    }
    public string GuestEmailId
    {
        get { return _guestEmailId; }
        set {_guestEmailId=value; }
    }
    public string CarNo
    {
        get { return _CarNo; }
        set { _CarNo=value; }
    }
    
    public string TotalUsageTime
    {
        get { return _TotalUsageTime; }
        set {_TotalUsageTime=value; }
    }
    public string TotalUsageKms
    {
        get { return _TotalUsageKms; }
        set { _TotalUsageKms = value; }
    }
    public string TotalTollParking
    {
        get { return _TotalTollParking; }
        set { _TotalTollParking=value; }
    }
    public string Concern
    {
        get { return _Concern; }
        set { _Concern = value; }
    }
    public  string ConcernText
    {
        get { return _ConcernText; }
        set {_ConcernText=value; }
    }
}
