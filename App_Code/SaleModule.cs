using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Insta;

/// <summary>
/// Summary description for SaleModule
/// </summary>
public class SaleModule
{
    
    private int clientCoId;

    public int ClientCoId
    {
        get { return clientCoId; }
        set { clientCoId = value; }
    }
    private string accountType;

    public string AccountType
    {
        get { return accountType; }
        set { accountType = value; }
    }
    private int hQ;

    public int HQ
    {
        get { return hQ; }
        set { hQ = value; }
    }

    private int signedBy;

    public int SignedBy
    {
        get { return signedBy; }
        set { signedBy = value; }
    }

    private string localInfluencer;

    public string LocalInfluencer
    {
        get { return localInfluencer; }
        set { localInfluencer = value; }
    }

    private string monthlyPotential;

    public string MonthlyPotential
    {
        get { return monthlyPotential; }
        set { monthlyPotential = value; }
    }

    private string localAccountMgr;

    public string LocalAccountMgr
    {
        get { return localAccountMgr; }
        set { localAccountMgr = value; }
    }
    private int sysUserID;

    public int SysUserID
    {
        get { return sysUserID; }
        set { sysUserID = value; }
    }

    private string localInfluencerName;

    public string LocalInfluencerName
    {
        get { return localInfluencerName; }
        set { localInfluencerName = value; }
    }
    private string localInfluencerMobileNo;

    public string LocalInfluencerMobileNo
    {
        get { return localInfluencerMobileNo; }
        set { localInfluencerMobileNo = value; }
    }
    private string implantId;
    public string ImplantId
    {
        get { return implantId; }
        set { implantId = value; }
    }

    private string competitor;

    public string Competitor
    {
        get { return competitor; }
        set { competitor = value; }
    }

    private string  newImplantName;

    public string NewImplantName
    {
        get { return newImplantName; }
        set { newImplantName = value; }
    }

    private int implantSysUserId;

    public int ImplantSysUserId
    {
        get { return implantSysUserId; }
        set { implantSysUserId = value; }
    }
	
	
}
