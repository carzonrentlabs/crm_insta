using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Insta;

public partial class SalesFollowUp : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.bntExprot);

        objCordrive = new CorDrive();

        if (!IsPostBack)
        {
            BindCustName();
            BindCity();
            BindActionManager();
            BindUserName();
            ClientType();
        }
        // btnSubmit.Attributes.Add("Onclick", "return validate()");
    }

    private void BindUserName()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dtuserNmae = new DataSet();
            dtuserNmae = objCordrive.GetUserName(Convert.ToInt32(Session["UserID"]));
            if (dtuserNmae.Tables[0].Rows.Count > 0)
            {
                lblloginBy.Text = "Logged By : " + dtuserNmae.Tables[0].Rows[0]["Username"].ToString();
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }
    private void BindCity()
    {
        objCordrive = new CorDrive();
        DataSet dscityname = new DataSet();
        dscityname = objCordrive.GetCityName();
        ddlcityName.DataTextField = "cityname";
        ddlcityName.DataValueField = "cityid";
        ddlcityName.DataSource = dscityname;
        ddlcityName.DataBind();
        ddlcityName.Items.Insert(0, "All");
    }
    private void BindCustName()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dsCustName = new DataSet();

            if (ddlClientType.SelectedItem.Text == "Prospect")
            {
                dsCustName = objCordrive.GetProspectName();
            }
            else
            {
                dsCustName = objCordrive.GetCustName();
            }
            if (dsCustName.Tables[0].Rows.Count > 0)
            {
                ddlCustName.DataTextField = "ClientCoName";
                ddlCustName.DataValueField = "ClientCoID";
                ddlCustName.DataSource = dsCustName;
                ddlCustName.DataBind();
                ddlCustName.Items.Insert(0, "--Select Action Client--");
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }

    private void BindCustName1()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dsCustName1 = new DataSet();

            if (ddlClientType1.SelectedItem.Text == "Prospect")
            {
                dsCustName1 = objCordrive.GetProspectName();
            }
            else
            {
                dsCustName1 = objCordrive.GetCustName();
            }
            if (dsCustName1.Tables[0].Rows.Count > 0)
            {
                ddlCustName1.DataTextField = "ClientCoName";
                ddlCustName1.DataValueField = "ClientCoID";
                ddlCustName1.DataSource = dsCustName1;
                ddlCustName1.DataBind();
                ddlCustName1.Items.Insert(0, "All");
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }

    private void BindActionManager()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dsActionManager = new DataSet();
            dsActionManager = objCordrive.GetActionManagerName();
            if (dsActionManager.Tables[0].Rows.Count > 0)
            {
                ddlActionMgrName.DataTextField = "UserName";
                ddlActionMgrName.DataValueField = "SysUserID";
                ddlActionMgrName.DataSource = dsActionManager;
                ddlActionMgrName.DataBind();
                ddlActionMgrName.Items.Insert(0, "--Select Action Manager--");
                
            }


            if (dsActionManager.Tables[0].Rows.Count > 0)
            {
                ddlActionMgrName1.DataTextField = "UserName";
                ddlActionMgrName1.DataValueField = "SysUserID";
                ddlActionMgrName1.DataSource = dsActionManager;
                ddlActionMgrName1.DataBind();
                ddlActionMgrName1.Items.Insert(0, "All");

            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            int ClientCoID = 0, ProspectID = 0;
            string ProspectMovement = "";
            if (ddlCustName.SelectedIndex == 0)
            {
                if (ddlClientType.SelectedItem.Text == "Prospect")
                {
                    // //ddlCustName
                    if (txtcustname.Text.Trim() == "" || txtcustname.Text == "--Select Action Client--")
                    {
                        lblMessage.Text = "Please Type Customer Name";
                        lblMessage.Visible = true;
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        return;
                    }
                    if (ddlProspectMovement.SelectedItem.Value == "0")
                    {
                        lblMessage.Text = "Please Select Prospect Movement.";
                        lblMessage.Visible = true;
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        return;
                    }
                    ProspectMovement = ddlProspectMovement.SelectedItem.Value;
                }
                else
                {
                    ProspectMovement = "";
                    lblMessage.Text = "Please Select Customer Name";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    return;
                }
            }
            else
            {
                if (ddlClientType.SelectedItem.Text != "Prospect")
                {
                    ClientCoID = Convert.ToInt32(ddlCustName.SelectedValue);

                    ProspectMovement = "";

                }
                else
                {
                    ProspectID = Convert.ToInt32(ddlCustName.SelectedValue);
                }
            }
            
            if (ddlActionMgrName.SelectedIndex == 0)
            {
                lblMessage.Text = "Please Select Action Manager Name";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }
            

            int rslt = objCordrive.SubmitSalesFollowUp(Convert.ToDateTime(txtDateOfMeeting.Text), ClientCoID
                , txtMeetingDesc.SelectedItem.Text, txtActionItem.Text, Convert.ToDateTime(txtActionByDate.Text), Convert.ToInt32(ddlActionMgrName.SelectedValue)
                , Convert.ToDateTime(txtNextMeetingSchedule.Text), Convert.ToInt32(Session["UserID"])
                , txtPersonName.Text, txtMobileNo.Text, txtcustname.Text, ddlClientType.SelectedItem.Text
                , ddlOutcomeofmeeting.SelectedItem.Text, ddlmodeconnect.SelectedItem.Text, ProspectID, ProspectMovement);
            if (rslt > 0)
            {
                lblMessage.Text = "Submitted Successfully";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                ClearControl();
            }
            else if (rslt == -1)
            {
                lblMessage.Text = "Record already Present for the Client and Meeting Date.";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                lblMessage.Text = "Not Submitted Successfully";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }
    public void ClearControl()
    {
        txtDateOfMeeting.Text = string.Empty;
        ddlCustName.SelectedIndex = 0;
        txtMeetingDesc.SelectedIndex = 0;
        txtActionItem.Text = string.Empty;
        txtActionByDate.Text = string.Empty;
        ddlActionMgrName.SelectedIndex = 0;
        txtNextMeetingSchedule.Text = string.Empty;
        txtcustname.Text = string.Empty;
        ddlClientType.SelectedIndex = 0;
        ddlClientType1.SelectedIndex = 0;
        txtPersonName.Text = string.Empty;
        
        txtMobileNo.Text = string.Empty;
        ddlOutcomeofmeeting.SelectedIndex = 0;
        ddlmodeconnect.SelectedIndex = 0;
        ddlProspectMovement.SelectedIndex = 0;
    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {
        //try
        //{
            DataTable dt = new DataTable();
            string colorSet3 = string.Empty;

            DataSet _objDs = new DataSet();
            int CustomeriD = 0;
            if (ddlClientType.SelectedItem.Value != "0")
            {
                if (ddlCustName1.SelectedItem.Value != "All")
                {
                    CustomeriD = Convert.ToInt32(ddlCustName1.SelectedItem.Value);
                }
            }

            int CreatedBy = 0;
            if (ddlActionMgrName1.SelectedItem.Value != "All")
            {
                CreatedBy = Convert.ToInt32(ddlActionMgrName1.SelectedItem.Value);
            }
            int cityid = 0;
            if (ddlcityName.SelectedItem.Value != "All")
            {
                cityid = Convert.ToInt32(ddlcityName.SelectedItem.Value);
            }
            _objDs = objCordrive.GetSalesFollowUpReport(Convert.ToDateTime(FromDate.Text), Convert.ToDateTime(ToDate.Text)
                , CreatedBy, ddlClientType1.SelectedItem.Value, ddlmodeconnect1.SelectedItem.Value
                , CustomeriD, cityid);
            if (_objDs != null)
            {
                if (_objDs.Tables.Count > 0)
                {
                    dt = _objDs.Tables[0];
                }
            }
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    Response.ClearContent();
                    Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                    Response.Clear();
                    Response.AppendHeader("content-disposition", "attachment;filename=SalesFollowUP.xls");
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    this.EnableViewState = false;
                    Response.Write("\r\n");
                    Response.Write("<table border = '1' align = 'center'> ");
                    int[] iColumns = { 20, 3, 19, 13, 15, 17, 2, 4, 5, 6, 8, 10, 11, 14, 9, 16, 18 };
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        if (i == 0)
                        {
                            Response.Write("<tr>");
                            for (int j = 0; j < iColumns.Length; j++)
                            {
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "ClientCoName")
                                {
                                    Response.Write("<td align='left'><b>Client Name</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "MeetingDate")
                                {
                                    Response.Write("<td align='left'><b>Meeting Date</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "MeetingDesc")
                                {
                                    Response.Write("<td align='left'><b>Meeting Desc</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "ActionItem")
                                {
                                    Response.Write("<td align='left'><b>Action Item</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "ActionByDate")
                                {
                                    Response.Write("<td align='left'><b>Action By Date</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "ActionManagerName")
                                {
                                    Response.Write("<td align='left'><b>Action Manager Name</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "NextMettingDate")
                                {
                                    Response.Write("<td align='left'><b>Next Metting Date</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "MeetingPersonName")
                                {
                                    Response.Write("<td align='left'><b>Meeting Person Name</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "MeetingPersonMobile")
                                {
                                    Response.Write("<td align='left'><b>Meeting Person Mobile</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "CreatedByName")
                                {
                                    Response.Write("<td align='left'><b>Created By</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "CreateDate")
                                {
                                    Response.Write("<td align='left'><b>Create Date</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "CustomerType")
                                {
                                    Response.Write("<td align='left'><b>Customer Type</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "OutcomeofMetting")
                                {
                                    Response.Write("<td align='left'><b>Outcome of Metting</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "ModeOfConnect")
                                {
                                    Response.Write("<td align='left'><b>Mode Of Connect</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "ProspectMovement")
                                {
                                    Response.Write("<td align='left'><b>Prospect Movement</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "CityName")
                                {
                                    Response.Write("<td align='left'><b>City Name</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "Row")
                                {
                                    Response.Write("<td align='left'><b>S.No.</b></td>");
                                }
                            }
                            Response.Write("</tr>");
                        }

                        for (int j = 0; j < iColumns.Length; j++)
                        {
                            Response.Write("<td align='left'>" + dt.Rows[i][iColumns[j]].ToString().Replace("'","") + "</td>");
                        }
                        Response.Write("</tr>");
                    }
                    Response.Write("</table>");
                    Response.End();
                }
                else
                {
                    Response.Write("<table border = '1' align = 'center' width = '100%'>");
                    Response.Write("<td align='center'><b>No Record Found</b></td>");
                    Response.Write("</table>");
                    Response.End();
                }
            }
        //}
        //catch (Exception ex)
        //{
        //    ex.Message.ToString();
        //}
    }

    void FillGrid()
    {
        DataSet GetBatchDetail = new DataSet();
        int CustomeriD = 0;
        if (ddlClientType.SelectedItem.Value != "0")
        {
            if (ddlCustName1.SelectedItem.Value != "All")
            {
                CustomeriD = Convert.ToInt32(ddlCustName1.SelectedItem.Value);
            }
        }
        int CreatedBy = 0;
        if (ddlActionMgrName1.SelectedItem.Value != "All")
        {
            CreatedBy = Convert.ToInt32(ddlActionMgrName1.SelectedItem.Value);
        }
        int cityid = 0;
        if (ddlcityName.SelectedItem.Value != "All")
        {
            cityid = Convert.ToInt32(ddlcityName.SelectedItem.Value);
        }
        GetBatchDetail = objCordrive.GetSalesFollowUpReport(Convert.ToDateTime(FromDate.Text), Convert.ToDateTime(ToDate.Text)
        , CreatedBy, ddlClientType1.SelectedItem.Value, ddlmodeconnect1.SelectedItem.Value
                , CustomeriD, cityid);

        gdsalesTracking.DataSource = GetBatchDetail.Tables[0];
        gdsalesTracking.DataBind();
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        FillGrid();
    }
    protected void ddlClientType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ClientType();
    }

    protected void ddlClientType1_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCustName1();
    }

    public void ClientType()
    {
        List<ListItem> items = new List<ListItem>();

        BindCustName();

        if (ddlClientType.SelectedItem.Text == "Prospect")
        {
            txtMeetingDesc.Items.Clear();
            items.Add(new ListItem("Select", "0"));
            items.Add(new ListItem("New Requirement", "New Requirement"));
            items.Add(new ListItem("Relationship Meeting.", "Relationship Meeting."));
            txtMeetingDesc.Items.AddRange(items.ToArray());
            lblprospectmovement.Visible = true;
            ddlProspectMovement.Visible = true;
        }
        else
        {
            txtMeetingDesc.Items.Clear();
            items.Add(new ListItem("Select", "0"));
            items.Add(new ListItem("E1 - Relationship Call", "E1 - Relationship Call"));
            items.Add(new ListItem("E2 - Business Follow Up call", "E2 - Business Follow Up call"));
            items.Add(new ListItem("E3 - Payment follow up call", "E3 - Payment follow up call"));
            items.Add(new ListItem("E4 - Business Review Call", "E4 - Business Review Call"));
            items.Add(new ListItem("E5 - Service Recovery Call", "E5 - Service Recovery Call"));

            /*
            items.Add(new ListItem("Payment follow-up", "Payment follow-up"));
            items.Add(new ListItem("Service Recovery", "Service Recovery"));
            items.Add(new ListItem("Operational Review", "Operational Review"));
            items.Add(new ListItem("New Requirement", "New Requirement"));
            items.Add(new ListItem("Relationship Meeting.", "Relationship Meeting."));
            */

            txtMeetingDesc.Items.AddRange(items.ToArray());

            lblprospectmovement.Visible = false;
            ddlProspectMovement.Visible = false;
        }
    }

    protected void ddlCustName_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtcustname.Text = ddlCustName.SelectedItem.Text;
    }

    protected void gdsalesTracking_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdsalesTracking.PageIndex = e.NewPageIndex;
        FillGrid();
    }
}